cladename	Dmel
CDS	control/dros_files/Dmel_genes.fasta
tree	(((((((((Dsim,Dsec,Dmau),Dmel),(Dere,Dyak)),Deug),((Dbia,Dsuz),Dtak)),Dfic),(Drho,Dele)),(Dkik,Dser)),(Dbip,Dana));
species	8