#!/usr/bin/env python3
"""This script processes several files into a fasta of CDS files"""

## genbank table, RNA, protein files
GBFILE = 'Corsair/control/cricetidae_files/GCF_000500345.1_Pman_1.0_genomic.gff'
RNAFILE = 'Corsair/control/cricetidae_files/Gnomon_mRNA.fsa'
PROTFILE = 'Corsair/control/cricetidae_files/Gnomon_prot.fsa'

## backtrans function
    ## input is a protein seq and mRNA seq, output is a CDS
    ## to find the start, consider back translating all possible
     # combinations of the first 7 aa, then finding where those 21 bp exist in the sequece


def genbank_scan(genbank_table):
    """Read through a genbank table, reutrn a dictionary of transcript ID : protein ID"""
    match_dict = {} ## measures the good key value pairs
    novalue = {} ## measures the ones that didn't work
    with open(genbank_table, 'r') as file1:
        for line in file1.readlines():
            if 'CDS' in line:
                try:
                    match_dict[line.split('\t')[0]] = [line.split('Genbank:')[1].split(';')[0], line.split('Genbank:')[1].split(';')[0]]
                except Exception as e:
                    novalue[line.split('\t')[0]] = True

    return match_dict

def prune_protein_dict_names(dicitonary1):
    """Alter the keys of a dictionary and return the new dictionary"""
    new_dictionary = {}
    for header in dicitonary1:
        if 'NW' in header.split('(')[1].split(')')[0]:
            genbank_number = header.split('(')[1].split(')')[0]
        new_dictionary[genbank_number] = dicitonary1[header]
    return new_dictionary

def fasta_read(file):
    """reads a fasta file, turns it into a dictionary of header : sequence pairs for searching"""
    wholefile = {}
    with open(file,'r') as f:
        for line in f.readlines():
            if ">" in line:
                # keyname = [x.strip() for x in line]
                # keyname = ''.join(keyname)
                keyname = line.split('>')[1].split('\n')[0]
                wholefile[keyname] = []
            elif '>' not in line:
                line = [x.strip() for x in line]
                line = ''.join(line)
                wholefile[keyname].append(line)
    for key in wholefile:
        wholefile[key] = ''.join(wholefile[key])

    return wholefile

def find_mrna_properties(mrna_file):
    """read a file of mRNAs and store all the sequence properties
       requires the Converter class
       returns a dictionary of Converter objects"""

    ## dictionary to hold the Converter objects
    converter_dict = {}

    for header in RNA_FASTA:
        if 'NW' in header.split('(')[1].split(')')[0]:
            genbank_number = header.split('(')[1].split(')')[0]
        long_name = None
        gene_name = None
        converter_dict[genbank_number] = Converter(genbank_number, long_name, gene_name)
        converter_dict[genbank_number].read_mrna_sequence(RNA_FASTA[header])
    return converter_dict

def match_protein_sequence(file1, protein_name):
    pass


## backtrans function
    ## input is a protein seq and mRNA seq, output is a CDS
    ## to find the start, consider back translating all possible
     # combinations of the first 7 aa, then finding where those 21 bp exist in the sequece

class Converter(object):
    """This class is used to conglomerate information about an mRNA and protein
       and then convert it to a CDS"""
    def __init__(self, genbank_id, long_name, gene_name):
        self.genbank_id = genbank_id
        self.long_name = long_name
        self.gene_name = gene_name
        self.mRNA_seq = ''
        self.protein_id = ''

    def read_mrna_sequence(self, mRNA_seq):
        """Accept the sequence of the mRNA"""
        self.mRNA_seq = mRNA_seq

    def something(self, something):
        """Accept the protein name and sequence"""
        pass

    def print_details(self):
        """Prints the relevant detains of the class"""
        print(self.genbank_id, self.gene_name, self.protein_id, self.mRNA_seq)


GENBANK_DICT = genbank_scan(GBFILE)

RNA_FASTA = fasta_read(RNAFILE)
PROTEIN_FASTA = prune_protein_dict_names(fasta_read(PROTFILE))

CONVERTER_DICT = find_mrna_properties(RNAFILE)



# CONVERTER_DICT['NM_001290362.1'].print_details()
# CONVERTER_DICT['XM_016008947.1'].print_details()
# CONVERTER_DICT['XM_016009228.1'].print_details()
Trues = 0
Falses = 0

for thiskey in CONVERTER_DICT:
    if thiskey in PROTEIN_FASTA.keys():
        Trues += 1
    else:
        Falses += 1

print(Trues, Falses)