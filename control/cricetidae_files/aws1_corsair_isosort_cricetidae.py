#!/usr/bin/env python3
import csv
import pickle

species = 'cricetidae_files/Peromyscus_maniculatus'

def trans(sequence):
    # takes a nucleotide string and returns the translation as an amino acid string

    # codon table
    transtable = {
        'agg' : 'R',
        'aga' : 'R',
        'agc' : 'S',
        'agt' : 'S',
        'aag' : 'K',
        'aaa' : 'K',
        'aac' : 'N',
        'aat' : 'N',
        'aca' : 'T',
        'acc' : 'T',
        'acg' : 'T',
        'act' : 'T',
        'atg' : 'M',
        'ata' : 'I',
        'atc' : 'I',
        'att' : 'I',
        'cgg' : 'R',
        'cga' : 'R',
        'cgc' : 'R',
        'cgt' : 'R',
        'cag' : 'Q',
        'caa' : 'Q',
        'cac' : 'H',
        'cat' : 'H',
        'ccg' : 'P',
        'cca' : 'P',
        'ccc' : 'P',
        'cct' : 'P',
        'ctg' : 'L',
        'cta' : 'L',
        'ctc' : 'L',
        'ctt' : 'L',
        'tgg' : 'W',
        'tga' : 'X',
        'tgc' : 'C',
        'tgt' : 'C',
        'tag' : 'X',
        'taa' : 'X',
        'tac' : 'Y',
        'tat' : 'Y',
        'tcg' : 'S',
        'tca' : 'S',
        'tcc' : 'S',
        'tct' : 'S',
        'ttg' : 'L',
        'tta' : 'L',
        'ttc' : 'F',
        'ttt' : 'F',
        'ggg' : 'G',
        'gga' : 'G',
        'ggc' : 'G',
        'ggt' : 'G',
        'gag' : 'E',
        'gaa' : 'E',
        'gac' : 'D',
        'gat' : 'D',
        'gcg' : 'A',
        'gca' : 'A',
        'gcc' : 'A',
        'gct' : 'A',
        'gtg' : 'V',
        'gta' : 'V',
        'gtc' : 'V',
        'gtt' : 'V'}

    # this is a counting variable
    k = 3

    # this the blank string for the amino acid sequence
    aaseq = ''

    # stops when it gets to the end of the sequence=
    while k <= len(sequence):
        # try to add a codon. will reject because it won't find the key if the variable is blank
        try:
            aaseq += transtable[sequence[(k-3):k].lower()]
        except:
            aaseq += '' ## adds a blank space if no translation is possible

        # count up 3
        k = k + 3

    # return value is a string of amino acids
    return aaseq

def fasta_read(file):
    ## reads a file, outputs a dicitonary
    wholefile = {}
    with open(file,'r') as f:
        for line in f.readlines():
            if ">" in line:
                keyname = [x.strip() for x in line]
                keyname = ''.join(keyname)
                keyname = keyname.split('>')[1]
                wholefile[keyname] = []
            elif '>' not in line:
                line = [x.strip() for x in line]
                line = ''.join(line)
                wholefile[keyname].append(line)
    for key in wholefile:
        wholefile[key] = ''.join(wholefile[key])

    return wholefile

# def remove_periods(dict1):
#     ## turns out that none of the isoform or gene names can have periods in them.
#     ## because we have already run some stuff, I'll do this in the blast processing script Tools
#     ## and because so few characters are available, periods are going to be replaced with _a_
#     temp_dic = {}
#     for x in dict1:

## to read through the H.sapiens CDS file and pick out isoforms
# input file
file1 = "Corsair/control/" + species+ ".cds.all.fa"

### read in the fasta file
wholefile = {}
with open(file1,'r') as f:
    for line in f.readlines():
        if ">" in line:
            keyname = [x.strip() for x in line]
            keyname = ''.join(keyname)
            keyname = keyname.split('>')[1]
            wholefile[keyname] = []
        elif '>' not in line:
            line = [x.strip() for x in line]
            line = ''.join(line)
            wholefile[keyname].append(line)
    for key in wholefile:
        wholefile[key] = ''.join(wholefile[key])

## make a directory that only includes the things that identify as protein coding genes
tempdir = {}
for key in wholefile:
    if "protein_coding" in key.split('gene_biotype:',)[1]:
        enst_id = key.split('cds')[0]
        if 'gene_symbol' in key:
            gene_sym = key.split('gene_symbol:')[1].split('description')[0].replace('.','-a-')
        else:
            gene_sym = enst_id.replace('.','-a-')
        tempdir[enst_id] = [gene_sym, wholefile[key], gene_sym]
wholefile = tempdir

######
# assign isoform values (done randomly, so only do it once!)
# then output a table to key the isoform values to ensemble ids
######

### make a dictionary to contain all the gene symbols as dictionaries (automatically be unique since it is a key)
genes = {}
for key in wholefile:
    symbol = wholefile[key][0]
    genes[symbol] = {}

### then add ensemble IDs as values for the gene symbol dictionary, and give the ID a value based on how many enteries there are
for key in wholefile:
    symbol = wholefile[key][0]
    genes[symbol][key] = str(len(genes[wholefile[key][0]])+1)

### use the numeric values to modify the entery in the other dictionary,
for key in wholefile:
    symbol = wholefile[key][0]
    wholefile[key][0] = symbol + '_' + genes[symbol][key]

## write a .csv table of all the ensemble_id and isoform pairs as a record
with open('Corsair/control/' + species + '_id_conversion_table.txt', 'w') as f:
    f.write('Ensemble ID' + '\t' + 'Assigned iso name\n')
    for key in wholefile:
        f.write(key + '\t' + wholefile[key][0] + '\n')


## make a new dictionary with the isoform as the key and seq as value
isoname_dir = {}
for key in wholefile:
    isoname_dir[wholefile[key][0]] = [wholefile[key][0].split('_',)[0],wholefile[key][1]]

## write that dictionary to a file in fasta format
count = 0
with open('Corsair/control/' + species + '_all_isoforms.fasta', 'w') as f:
    for key in isoname_dir:
        f.write('>' + key + '\n' + isoname_dir[key][1] + '\n')
        count += 1
print(str(count) + ' non-unique isoforms')


### figure out what the unqiue isoforms are
genes = {}
for key in isoname_dir:
    symbol = isoname_dir[key][0]
    genes[symbol] = {}

for key in isoname_dir:
    symbol = isoname_dir[key][0]
    genes[symbol][key] = isoname_dir[key][1]

for key in genes:
    genes[key]['isoforms'] = []
    for iso in genes[key]:
        if iso != 'isoforms':
            genes[key]['isoforms'].append(iso)

## set an empty dictionary
unqiue_isoforms = {}
## translate everything
for gx in genes:
    for iso in genes[gx]:
        if iso != 'isoforms':
            genes[gx][iso] = [genes[gx][iso], trans(genes[gx][iso])]

    logg = {}
    for uniso in genes[gx]['isoforms']:
        logg[uniso] = genes[gx][uniso][1]

    # get the name of the longest isoform for each gene, or skip the rest of the function if empty
    if logg: #This only runs if the isodict contained the gene name
        maxseq = sorted(sorted(logg.items(), key=lambda x: x[0]),key=lambda x: len(x[1]),reverse=True)[0][0]
        genes[gx]['longiso'] = maxseq
        genes[gx]['uniqiso']={maxseq:genes[gx][maxseq][1]}
        genes[gx]['uniqisocds']={maxseq:genes[gx][maxseq][0]}
        unqiue_isoforms[maxseq] = genes[gx][maxseq][0]

        ## this loop checks for unique isoforms
        for testiso in genes[gx]['isoforms']:
            dashes = True
            for knownisos in genes[gx]['uniqiso']:
                if genes[gx][testiso][1] in genes[gx]['uniqiso'][knownisos]: ## this is actually a really good line, because it just does a simple check (though not complete) to make sure that one is not just a subset of the other, and without alignment this saves a massive amount of time.
                    dashes = False
            ## save the sequence if not contained in other isoform
            if dashes:
                genes[gx]['uniqiso'][testiso] = genes[gx][testiso][1]
                genes[gx]['uniqisocds'][testiso] = genes[gx][testiso][0]
                unqiue_isoforms[testiso] = genes[gx][testiso][0]


## write a file with the names of the longest isoform and one with the unique isoforms
with open('Corsair/control/' + species + '_longest_isoforms.txt', 'w') as f:
    for gx in genes:
        f.write(genes[gx]['longiso'] + '\n')

with open('Corsair/control/' + species + '_unique_isoforms.txt', 'w') as f:
    for gx in genes:
        for uni in genes[gx]['uniqiso']:
            f.write(uni + '\n')


## write all the unique isoforms to a fasta file
count = 0
with open('Corsair/control/' + species + '_unique_isoforms.fasta', 'w') as f:
    for key in unqiue_isoforms:
        f.write('>' + key + '\n' + isoname_dir[key][1] + '\n')
        count += 1
print(str(count) + ' UNIQUE isoforms')

## store the geneome information as a pickle file
fasta_dic = fasta_read('Corsair/control/' + species + '_all_isoforms.fasta')
with open('Corsair/control/' + species + '_all_isoform_fasta_cds.pkl', 'wb') as output:
    pickle.dump (fasta_dic, output, pickle.HIGHEST_PROTOCOL)
