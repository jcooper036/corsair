#!/usr/bin/env python3
import csv

## build a dictionary with the human transcript ensembl ID as the key
    ## values:
        ## gene ID
        ## sub-dictionary for mouse

## several dictionaries. in all of them, the keys are HUMAN TRANSCRIPT IDs
## the name of the dictionary is the species it compares against
    ## human - contains gene ID
    ## all others - human gene, orthologous gene, orthology type
    ## if there is no data, will be NA

mouse_file = 'Corsair/control/orthology/human_to_mouse.txt'
derms_file = 'Corsair/control/orthology/human_to_deermouse.txt'
doggo_file = 'Corsair/control/orthology/human_to_dog.txt'
cowss_file = 'Corsair/control/orthology/human_to_cow.txt'
smbat_file = 'Corsair/control/orthology/human_to_microbat.txt'

human = {}
mouse = {}
derms = {}
doggo = {}
cowss = {}
smbat = {}

def get_orthology (ortho_file, human_dict, clade_dict):
    '''Takes a csv file of human gene, human transcript, ortho gene, orthology type
        Returns dictionaries where the human trascript is always the key'''
    
    with open(ortho_file, 'r') as f:
        for line in f.readlines():
            line = list(line.strip().split(','))

            ## take care of the human dictionary
            if line[0] in human_dict.keys():
                human_dict[line[0]].append(line[1])
            else:
                human_dict[line[0]] = [line[1]]
            
            ## take care of the mouse dictionary
            if line[2] == '':
                clade_dict[line[1]] = ['none', 'none', 'none']
            else:
                clade_dict[line[1]] = [line[0], line[2], line[3]]

    return human_dict, clade_dict

def one_to_one(clade_dict, oto_set):
    '''Adds the human trasncripts that are only 1:1 to a set'''
    for xnr in clade_dict:
        if clade_dict[xnr][2] == 'ortholog_one2one':
            oto_set.add(xnr)
            # oto_set.add(clade_dict[xnr][0])
    return oto_set

def hit_list(file_name):
    """Input a file with the hits, return a list of gene names"""
    new_list = []
    with open(file_name, 'r') as f:
        for line in f.readlines():
            new_list.append(line.strip())
    return new_list

## human and mouse
human, mouse = get_orthology(mouse_file, human, mouse)
human, derms = get_orthology(derms_file, human, derms)
human, doggo = get_orthology(doggo_file, human, doggo)
human, cowss = get_orthology(cowss_file, human, cowss)
human, smbat = get_orthology(smbat_file, human, smbat)

mouse_set = set([])
derms_set = set([])
doggo_set = set([])
cowss_set = set([])
smbat_set = set([])

mouse_set = one_to_one(mouse, mouse_set)
derms_set = one_to_one(derms, derms_set)
doggo_set = one_to_one(doggo, doggo_set)
cowss_set = one_to_one(cowss, cowss_set)
smbat_set = one_to_one(smbat, smbat_set)

## make the composite table
converstion_table = [['Human', 'Mouse','DeerMouse','Dog','Cow','Bat']]
for xnr in mouse_set:
    human_add = mouse[xnr][0]
    mouse_add = mouse[xnr][1]
    if xnr in derms_set:
        derms_add = derms[xnr][1]
    else:
        derms_add = 'none'
    if xnr in doggo_set:
        doggo_add = doggo[xnr][1]
    else:
        doggo_add = 'none'
    if xnr in cowss_set:
        cowss_add = cowss[xnr][1]
    else:
        cowss_add = 'none'
    if xnr in smbat_set:
        smbat_add = smbat[xnr][1]
    else:
        smbat_add = 'none'
    
    converstion_table.append([human_add, mouse_add, derms_add, doggo_add, cowss_add, smbat_add])

with open('Corsair/control/orthology/one_to_one_list.txt', 'w') as f:
    for xnr in converstion_table:
        f.write(",".join(xnr) + '\n')        
        # f.write(xnr + '\n')

human_hits = hit_list('Corsair/control/orthology/hit_lists/primate_hits.txt')
mouse_hits = hit_list('Corsair/control/orthology/hit_lists/murinae_hits.txt')
derms_hits = hit_list('Corsair/control/orthology/hit_lists/cricetidae_hits.txt')
doggo_hits = hit_list('Corsair/control/orthology/hit_lists/caniformia_hits.txt')
cowss_hits = hit_list('Corsair/control/orthology/hit_lists/bovidae_hits.txt')
smbat_hits = hit_list('Corsair/control/orthology/hit_lists/bat_hits.txt')

## build a table where true = hit and false = not hit
results = {}
for xnr in converstion_table:
    ## name of the gene
    gene_add = xnr[0]
    ## human hit
    if xnr[0] in human_hits:
        human_hit = 'True'
    else:
        human_hit = 'False'
    ## mouse hit
    if xnr[1] in mouse_hits:
        mouse_hit = 'True'
    else:
        mouse_hit = 'False'   
    ## deermouse hit
    if xnr[2] in derms_hits:
        derms_hit = 'True'
    else:
        derms_hit = 'False'
    ## dog hit
    if xnr[3] in doggo_hits:
        doggo_hit = 'True'
    else:
        doggo_hit = 'False'
    ## cow hit
    if xnr[4] in cowss_hits:
        cowss_hit = 'True'
    else:
        cowss_hit = 'False'
    ## bat hit
    if xnr[5] in smbat_hits:
        smbat_hit = 'True'
    else:
        smbat_hit = 'False'
    results[gene_add] = [human_hit, mouse_hit, derms_hit, doggo_hit, cowss_hit, smbat_hit] 

new_save = {}
for xnr in results:
    if 'True' in results[xnr]:
        new_save[xnr] = results[xnr]
results = new_save


with open('Corsair/control/orthology/hit_comparison.txt', 'w') as f:
    f.write('Human Gene,Human,Mouse,DeerMouse,Dog,Cow,Bat\n')
    for xnr in results:
        f.write(xnr + ',' + ",".join(results[xnr]) + '\n')

two_hits = {}
three_hits = {}
four_hits = {}
five_hits = {}
six_hits = {}

for xnr in results:
    flag2 = 0
    for ynr in results[xnr]:
        if ynr == 'True':
            flag2 += 1        
    if flag2 == 2:
        two_hits[xnr] = results[xnr]
    if flag2 == 3:
        three_hits[xnr] = results[xnr]
    if flag2 == 4:
        four_hits[xnr] = results[xnr]
    if flag2 == 5:
        five_hits[xnr] = results[xnr]
    if flag2 == 6:
        six_hits[xnr] = results[xnr]

print(str(len(results.keys())))
print(str(len(two_hits.keys())))
print(str(len(three_hits.keys())))
print(str(len(four_hits.keys())))

print(three_hits.keys())
print(four_hits.keys())
# print(composite)

# print(str(len(mouse_set)))
# print(str(len(derms_set)))
# print(str(len(doggo_set)))
# print(str(len(cowss_set)))
# print(str(len(smbat_set)))

# ## runner is the running list of all transcripts that have 1:1 orthology
# print('\n')
# runner = mouse_set.intersection(derms_set)
# print(str(len(runner)))
# runner = runner.intersection(doggo_set)
# print(str(len(runner)))
# runner = runner.intersection(cowss_set)
# print(str(len(runner)))
# runner = runner.intersection(smbat_set)
# print(str(len(runner)))

# with open('Corsair/control/orthology/one_to_one_list.txt', 'w') as f:
#     for xnr in runner:
#         f.write(xnr + '\n')

# both_genes = sim_cell_genes.intersection(mel_cell_genes) 