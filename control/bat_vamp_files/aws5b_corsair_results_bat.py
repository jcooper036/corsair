#!/usr/bin/env python3

"""Usage: aws_corsair_results.py FILE

Arguments:
    FILE  File that contains isoform names

"""

import docopt
import pickle
import time
import shutil
import os

## Initialize docopt
if __name__ == '__main__':

    try:
        arguments = docopt.docopt(__doc__)
        isoforms_file = str(arguments['FILE'])

    except docopt.DocoptExit as e:
        print(e)

clade = 'Pvam'
ensemble_conversion_file = 'bat_vamp_ensemble_file.pkl'
genes_file = '180328_results_bat_vamp_unique'
result_out_file = '180328_results_bat_vamp_unique_results.txt'
debug = False

## this varaible set the cutoff for signifcant sites
BEB_threshold = 0.99

################################################################
################ definintions
################################################################
def trans(sequence):
    # takes a nucleotide string and returns the translation as an amino acid string

    # codon table
    transtable = {
        'agg' : 'R',
        'aga' : 'R',
        'agc' : 'S',
        'agt' : 'S',
        'aag' : 'K',
        'aaa' : 'K',
        'aac' : 'N',
        'aat' : 'N',
        'aca' : 'T',
        'acc' : 'T',
        'acg' : 'T',
        'act' : 'T',
        'atg' : 'M',
        'ata' : 'I',
        'atc' : 'I',
        'att' : 'I',
        'cgg' : 'R',
        'cga' : 'R',
        'cgc' : 'R',
        'cgt' : 'R',
        'cag' : 'Q',
        'caa' : 'Q',
        'cac' : 'H',
        'cat' : 'H',
        'ccg' : 'P',
        'cca' : 'P',
        'ccc' : 'P',
        'cct' : 'P',
        'ctg' : 'L',
        'cta' : 'L',
        'ctc' : 'L',
        'ctt' : 'L',
        'tgg' : 'W',
        'tga' : 'X',
        'tgc' : 'C',
        'tgt' : 'C',
        'tag' : 'X',
        'taa' : 'X',
        'tac' : 'Y',
        'tat' : 'Y',
        'tcg' : 'S',
        'tca' : 'S',
        'tcc' : 'S',
        'tct' : 'S',
        'ttg' : 'L',
        'tta' : 'L',
        'ttc' : 'F',
        'ttt' : 'F',
        'ggg' : 'G',
        'gga' : 'G',
        'ggc' : 'G',
        'ggt' : 'G',
        'gag' : 'E',
        'gaa' : 'E',
        'gac' : 'D',
        'gat' : 'D',
        'gcg' : 'A',
        'gca' : 'A',
        'gcc' : 'A',
        'gct' : 'A',
        'gtg' : 'V',
        'gta' : 'V',
        'gtc' : 'V',
        'gtt' : 'V'}

    # this is a counting variable
    k = 3

    # this the blank string for the amino acid sequence
    aaseq = ''

    # stops when it gets to the end of the sequence=
    while k <= len(sequence):
        # try to add a codon. will reject because it won't find the key if the variable is blank
        try:
            aaseq += transtable[sequence[(k-3):k].lower()]
        except:
            aaseq += '' ## adds a blank space if no translation is possible

        # count up 3
        k = k + 3

    # return value is a string of amino acids
    return aaseq

def key_max_length(d):
    """ a) create a list of the dict's keys the length of the values (which is a list);
        b) return the key with the max length value
        c) specifically works only with self.BEBsites dicitonary"""
    v = []
    k = []
    for x in d:
        if x is not 'same_sites':
            v.append(len(d[x]))
            k.append(x)
    return k[v.index(max(v))]

def movedirectory(src, dest):
    try:
        shutil.move(src, dest)
    # Directories are the same
    except shutil.Error as e:
        print('Directory not moved. Error: %s' % e)
    # Any error saying that the directory doesn't exist
    except OSError as e:
        print('Directory not moved. Error: %s' % e)

def fasta_read(n):
    print('This definition is not actually present - fasta_read')
    return False

def back_translate(aa_alignment, nuc_input, iso, aln, isodic):
    # takes a dictionary that contains amino acid strings, that are already
    # aligned, where stop codons are marked with X or *, that are all the same
    # length, and reduces them to
    # the minimum amino continuous amino acid sequence across all species

    # make sure input is divisable by 3
    for key in nuc_input:
        if not (len(nuc_input[key]) % 3) == 0:
            print('\nWARNING: The nucleotide input for back translation for ' + key + ' is not divisable by 3. Please check.\n')

    minaa = {}
    for key in aa_alignment:
        cond_seq = ""
        for i in range(0, len(aa_alignment[key])):
            if not any(aa_alignment[nkey][i] == "*" or aa_alignment[nkey][i] == "X" or aa_alignment[nkey][i] == "-" for nkey in aa_alignment):
                cond_seq += aa_alignment[key][i]
        minaa[key] = cond_seq

    minnuc = {}
    for key in minaa:
        k = 0
        cond_nuc = nuc_input[key]
        while k < len(minaa[key]):
            if not minaa[key][k] == trans(cond_nuc[(k*3):((k*3)+3)]): #@ translating codon by codon, which means that if there is an error message in the translation def then it will pop up every time
                cond_nuc = cond_nuc[:(k*3)] + cond_nuc[(k*3)+3:]
            else:
                k += 1
        else:
            cond_nuc = cond_nuc[:(k*3)]

        minnuc[key] = cond_nuc

    if debug:
        print(str(minnuc))

    ## write a file for the output
    with open(isodic[iso].paml_file(aln), 'w') as f:
        f.write('\t' + str(len(minnuc.keys())) + '\t' + str(len(minnuc[clade])) + '\n') ## changed to 'clade' variable
        for key in minnuc:
            f.write(str(key) + '\n')
            f.write(str(minnuc[key]) + '\n')

    ## and also return the dictionary with the back-translation
    return minnuc

def load_database_old_analysis(hit_file, gene_file):
    ## files must have Ensemble ID in column one of a tab seperated file
    ## returns a list of the Ensemble IDs of the hits and a list of the genes
    ## minus the hits

    ## positive hits
    hit_list = []
    with open(hit_file, 'r') as f:
        for line in f.readlines():
            if 'Ensembl.Gene.ID' not in line:
                hit_list.append(line.strip().split('\t')[0])

    ## all genes
    gene_list = []
    with open(gene_file, 'r') as f:
        for line in f.readlines():
            if 'Ensembl.Gene.ID' not in line:
                gene_list.append(line.strip().split('\t')[0])

    ## making those two lists exclusive
    for x in hit_list:
        if x in gene_list:
            gene_list.remove(x)
        pass

    return hit_list, gene_list


################################################################
################ classes
################################################################
################################################################
class isoform(object):
    """Holds information about an isoform"""

    #################
    ## these are used to Initialize and operate on the isoform class
    #################

    def __init__(self, name):
        """initiates the isoform class with several blank properties"""
        self.name = name
        self.alignment = {}
        self.backtrans = {}
        self.BEB_sites = {}
        self.BEB_sites['same_sites'] = []
        self.ref_min_aa = {}
        self.prev_ids = {}

    def rename(self, newname):
        """renames the isoform"""
        self.name = newname

    def link_parent(self, parent_gene):
        ## used to link an isoform with a parent gene
        self.parent_gene = parent_gene
        self.iso_path = genes_file + '/' + self.parent_gene + '/' + self.name + '/'

    def ref_nt(self, ref_nt):
        # nt_seq must be a string. also as is, this can't be over-written
        self.ref_nt = ref_nt

    def translate(self):
        # translates the nt_seq if there is one
        try:
            self.ref_aa = trans(self.ref_nt)
        except:
            print("ERROR: There is no nucleotide sequence for this gene")

    #################
    ## these are some property functions to return common file paths
    ## they help make the handling of everything else much more readable
    #################

    def iso_files(self):
        ## returns the path to the files folder
        return self.iso_path + self.name + '_files/'

    def CDS_file(self):
        ## returns the cds_file path
        return self.iso_path + self.name + '_files/' + self.name + '_CDS.txt'

    def prot_file(self):
        ## returns the protein file path
        return self.iso_path + self.name + '_files/' + self.name + '_prot.txt'

    def alignment_file(self, algnr):
        ## returns the file path with the specified alinger
        try:
            return self.iso_path + self.name + '_files/' + self.name + '_' + algnr + '.fasta'
        except:
            print('Need to specify an aligner')

    def paml_file(self, algnr):
        ## returns the paml alignment file path
        try:
            return self.iso_path + self.name + '_files/' + self.name + '_' + algnr + '.paml'
        except:
            print('Need to specify and aligner')

    def tree_file(self):
        ## returns the tree file path
        return self.iso_path + self.name + '_files/' + self.name + '_tree.txt'

    def paml_output(self, algnr):
        ## returns the file path for the paml output file for a given aligner
        try:
            return (self.iso_path + self.name + '_files/'
                    + self.name + '_' + algnr + '_PAML_out_full.txt')
        except:
            print('Need to specify and aligner')

    def p8ml_output(self, algnr):
        ## returns the file path for the paml output file for a given aligner
        try:
            return (self.iso_path + self.name + '_files/'
                    + self.name + '_' + algnr + '_M8a_PAML_out_full.txt')
        except:
            print('Need to specify and aligner')

    def results_file(self):
        try:
            return self.iso_path + 'results.txt'
        except:
            print('Could not properly specifiy results file')

    #################
    ## these are used in the processing of the data
    #################

    def blast_search(self, blast_dic):
        # reads in a dicitonary of species:sequence pairs
        self.blast_dic = blast_dic

    def blast_trans(self):
        # translates the blast dicitonary
        try:
            self.blast_prot = {}
            for key in self.blast_dic:
                self.blast_prot[key] = trans(self.blast_dic[key])
        except:
            print("ERROR: There is no blast dictionary")

    def load_alignment(self, aligner, file1):
        ## gives .aln property, which is a dictionary containing aligner names
         # that are in turn dictionaries with alignments
        self.alignment[aligner] = {}
        self.alignment[aligner] = fasta_read(file1)

    def load_backtrans(self, aligner, bctrns):
        ## gives .backtrans property, which is a dictionary containing aligner
         # names that are in turn dictionaries with alignments
        ## bctrns is a dictionary that contains the back-translation in nt form
        self.alignment[aligner] = {}
        self.alignment[aligner] = bctrns

    def spec_number_check(self, speccount):
        ##This makes sure that analysis isn't run if PAML file isn't there
        if len(self.blast_dic) >= speccount:
            return True
        else:
            return False

    def loglike_get(self):
        ## scans the results file for a log-likelihood values
        ## stores properties in a dictionary:
        ## llvals: dictionary of aligner_model : log-likelihood
        self.llvals = {}
        for algr in ('clus', 'coff', 'mus'):
            try:
                if os.path.exists(self.paml_output(algr)):
                    with open(self.paml_output(algr), 'r') as f:
                        flag = False
                        for line in f.readlines():

                            if 'Model' in line: ## determines the model
                                flag = True
                                line = [x.strip() for x in line]
                                line = ''.join(line)
                                line = line.split('Model')[1]
                                line = line.split(':')[0]
                                if line: # takes care of some blank lines that get returned
                                    model = algr + '_M' + line

                            if flag and 'lnL' in line: ## gets the log-likelihood value
                                line = [x.strip() for x in line]
                                line = ''.join(line)
                                line = line.split('):')[1]
                                lnl = line.split('+')[0]
                                self.llvals[model] = lnl
                                flag = False
                else:
                    if debug:
                        print('PAML output file for ' + algr + ' not detected.')
            except:
                print('Can not open paml file')

            if 'clus_M7_M8_p' not in self.llvals:
                self.llvals['clus_M7_M8_p'] = 37767



            if os.path.exists(self.p8ml_output(algr)):
                with open(self.p8ml_output(algr), 'r') as f:
                    for line in f.readlines():
                        if 'lnL' in line: ## gets the log-likelihood value
                            model = algr + '_M8a'
                            line = [x.strip() for x in line]
                            line = ''.join(line)
                            line = line.split('):')[1]
                            lnl = line.split('+')[0]
                            self.llvals[model] = lnl
                            flag = False

            ## calculates the 2Delta log-likelihood
            ## calculates a p-value

            M7 = algr + '_M7'
            M8 = algr + '_M8'
            M8a = algr + '_M8a'
            M7_M8_2del = algr + '_M7_M8_2del'
            M7_M8_p = algr + '_M7_M8_p'
            M8_M8a_2del = algr + '_M8_M8a_2del'
            M8_M8a_p = algr + '_M8_M8a_p'

            ## see if there is a M7-M8 comparison
            try:
                self.llvals[M7_M8_2del] = 2 * (abs(float(self.llvals[M7])
                                                   - float(self.llvals[M8])))
                self.llvals[M7_M8_p] = chi2.sf(self.llvals[M7_M8_2del], 2)
            except:
                pass

            ## see if there is a M8-M8a comparison
            try:
                self.llvals[M8_M8a_2del] = 2 * (abs(float(self.llvals[M8])
                                                    - float(self.llvals[M8a])))
                self.llvals[M8_M8a_p] = chi2.sf(self.llvals[M8_M8a_2del], 1)
            except:
                pass

        ## determine the current highest p-value
        d = {}
        for algr in ('clus', 'mus', 'coff'):
            try:
                M7_M8_p = algr + '_M7_M8_p'
                d[algr] = self.llvals[M7_M8_p]
            except:
                pass

        # self.llvals['max_p'] = keywithmaxval(d)
        if d:
            self.llvals['max_p'] = max(d, key=d.get)
        else:
            with open(result_out_file, 'a') as f:
                f.write('\n' + self.name + '\tPAML did not run correctly.')

    def period_back_replace(self):
        ## periods had to be removed from names for AWS, so this replaces the
        ## substitution '-a-' with the period
        if '-a-' in self.name:
            self.name = self.name.replace('-a-', '.')

    def check_positive(self):
        self.pos_result = False
        for aln in ('clus', 'mus', 'coff'):
            if os.path.exists(self.p8ml_output(aln)):
                self.pos_result = True
        ## clear the dictionary here
        if self.pos_result:
            self.BEB_sites = {}
        return self.pos_result

    def site_get(self, algnr):
        # gets the positions of the amino acids under selection in the origonal reference sequence
        # requires: self.llvals['max_p']
        # creates property: self.BEB_sites
        uncor_sites = []  ## this is the list of uncorrected BEB sites
        flag1 = False
        flag2 = False

        ## find the BEB sites from the paml results file
        with open(self.paml_output(algnr), 'r') as f:
            for line in f.readlines():
                if '+-' in line and not 'Pr(w>1)' in line:
                    line = line.split()
                    line[2] = line[2].replace('*', '')
                    if float(line[2]) > BEB_threshold:
                        uncor_sites.append(line[0])
        if debug:
            print('Sites under selection, before correction (pp > ' + str(BEB_threshold) + ') for '
                  + algnr + ': ' + str(uncor_sites))

        ## extracts the D.mel reduced sequence form the PAML input file
        flag1 = False
        count = 0
        red_seq = []
        with open(self.paml_file(algnr), 'r') as f:
            for line in f.readlines():
                if clade in line:
                    flag1 = True
                if flag1 and count < 2:
                    line = [x.strip() for x in line]
                    line = ''.join(line)
                    red_seq.append(line)
                    count += 1

        ## two new properties that contain the reduced reference sequences
        self.ref_min_nt = {algnr : red_seq[1]}
        self.ref_min_aa[algnr] = trans(self.ref_min_nt[algnr])

        ## use UPPER CASE to denote the BEB amino acids
        indicies = [] ## since the index position will be sites - 1
        for i in uncor_sites:
            indicies.append(int(i) - 1)
        self.ref_min_aa[algnr] = ("".join(c.upper() if i in indicies else c for i,
                                          c in enumerate(self.ref_min_aa[algnr].lower())))

        if debug:
            print(self.ref_min_aa[algnr])
            print(self.ref_aa)
            print('\n')

        ## iterate through the length of the main sequence. Check if the two are
        ## equal. If not, add a '-' to the reduced sequence, and check the next
        ## position. The result is that self.ref_min_aa has dashes and BEB sites
        ## in UPPER CASE, and is the same length as teh ref_aa sequence.
        for i in range(0, len(self.ref_aa)):
            try:
                if debug:
                    print(str(i) + ' - ' + self.ref_min_aa[i] + ' , ' + self.ref_aa[i])
                if self.ref_min_aa[algnr][i].lower() != self.ref_aa[i].lower():
                    if i == 0:
                        self.ref_min_aa[algnr] = '-' + self.ref_min_aa[algnr]
                    else:
                        self.ref_min_aa[algnr] = (self.ref_min_aa[algnr][:i]
                                                  + '-' + self.ref_min_aa[algnr][i:])
            except:
                pass

        # get the list of sites that are in UPPER CASE
        self.BEB_sites[algnr] = []
        for i, c in enumerate(self.ref_min_aa[algnr]):
            if c.isupper():
                self.BEB_sites[algnr].append(i+1)

        if debug:
            print('Sites under selection (pp > ' + str(BEB_threshold) + ') for '
                  + algnr + ': ' + str(self.BEB_sites[algnr]))

    def site_analysis(self):
        """reduces to the common sites for all aligners, and generates some other data
           makes a list called self.BEB_sites['same_sites']"""
        self.BEB_sites['same_sites'] = []
        # if len(self.BEB_sites['same_sites']) <= 0:
        for i in self.BEB_sites[key_max_length(self.BEB_sites)]:
            copy = True
            for aln in ('clus', 'mus', 'coff'):
                if i not in self.BEB_sites[aln]:
                    copy = False
            if copy:
                self.BEB_sites['same_sites'].append(i)
    
    def site_id(self):
        ## if the same sites list exists and has length, use the same site number as an index position to grab the aa id
        if len(self.BEB_sites['same_sites']) > 0 and self.BEB_sites['same_sites'] != 'NA':
            temp_list = []
            for x in self.BEB_sites['same_sites']:
                y = self.ref_aa[(x-1)] + str(x)
                temp_list.append(y)
            self.BEB_sites['same_sites_aa'] = temp_list
        else:
            self.BEB_sites['same_sites_aa'] = 'NA'

    def variable_check(self):
        ## scans through different sets of variables and makes sure they are
        ## in a format that can be handled en masse downstream

        ## boolean to check if results outputs are correct
        self.pamlerror = False

        ## in case something never ran
        if debug:
            try:
                print("self.llvals['max_p'] = " + self.llvals['max_p'])
            except:
                print("Cannot find self.llvals['max_p']")
        
        try:
            self.llvals['max_p']
        except:
            self.llvals = {}
            self.llvals['max_p'] = False
            self.pamlerror = True

        ## first check on the codeml outputs and percent alignment
        if self.llvals['max_p']:
            try:
                self.M7_M8 = self.llvals[self.llvals['max_p'] + '_M7_M8_p']
            except:
                self.M7_M8 = 'NA'

            try:
                self.M8a = self.llvals[self.llvals['max_p'] + '_M8_M8a_p']
            except:
                self.M8a = 'NA'

            try:
                self.per_aligned = len(trans(self.alignment['clus'][clade]))/len(self.ref_aa) * 100 #length of the ref_aa of the max pvalue divided by the reference
            except:
                self.per_aligned = 'NA'

            if self.llvals['clus_M7_M8_p'] == 37767:
                self.pamlerror = True

        else:
            self.M7_M8 = 'NA'
            self.M8a = 'NA'
            try:
                self.per_aligned = len(trans(self.alignment['clus'][clade]))/len(self.ref_aa) * 100
            except:
                self.per_aligned = 'NA'
            self.pamlerror = True

        ## make a variable of the species that were used for analysis
        if not self.pamlerror:
            self.species_used = []
            for key in self.alignment[self.llvals['max_p']]:
                self.species_used.append(key)
            self.numspec = len(self.species_used)
        ## if it isn't avaiable, grab it from the results file
        #@ instead, try and get this from class variables
        else:
            if os.path.isfile(self.results_file()):
                with open(self.results_file(), 'r') as f:
                    message = f.read().splitlines()
                    if 'PAML' in message[1]:
                        self.numspec = message[1].split(': ')[1]
                    elif 'Number of species analyzed' in message[2]:
                        self.numspec = message[2].split(':\t')[1].split('\t')[0]
                    else:
                        self.numspec = 'ND'

        ## make sure that these variable exists, return NA's if they don't
        if len(self.BEB_sites['same_sites']) == 0 or self.BEB_sites['same_sites'] == 'NA':
            self.BEB_sites['same_sites_number'] = 0
            self.BEB_sites['same_sites'] = 'NA'
            self.BEB_sites['average_sites'] = 'NA'

        ## set the BEB values to nothing if it is not a positive result
        if not self.check_positive():
            self.BEB_sites = {}
            self.BEB_sites['av_qscore'] = 'NA'
            self.BEB_sites['concordence'] = 'NA'
            self.BEB_sites['same_sites'] = 'NA'
            self.BEB_sites['same_sites_number'] = 0
            self.BEB_sites['average_sites'] = 'NA'
            self.BEB_sites['same_sites_aa'] = 'NA'
            self.BEB_sites['min_qual_score'] = 'NA'

        if debug:
            print('# Variable Check step')
            print('self.M7_M8 = ' + str(self.M7_M8))
            print('self.M8a = ' + str(self.M8a))
            print('self.per_aligned = ' + str(self.per_aligned))
            print("self.llvals['clus_M7_M8_p'] = " + str(self.llvals['clus_M7_M8_p']))

    def site_quality(self):
        ## generate the alignment for sites with a few aa on either side
        try:
            self.BEB_sites['same_sites_number'] = len(self.BEB_sites['same_sites'])
            self.BEB_sites['average_sites'] = float((len(self.BEB_sites['clus'])
                                                     +len(self.BEB_sites['mus'])
                                                     +len(self.BEB_sites['coff']))/3)
        except:
            pass
       
        if self.BEB_sites['same_sites_number'] > 0:
            ## make a dictionary to hold the information about the sites as well
            self.BEB_sites['site_scores'] = {}
            self.BEB_sites['av_qscore'] = []
            for site in self.BEB_sites['same_sites']:
                lower_bound = int(site - 5)
                upper_bound = int(site + 4)

                ## store these in a dictionary too
                self.BEB_sites['site_scores'][site] = {'ref_seq' : self.ref_aa.lower()[lower_bound : upper_bound],
                                                       'test_seq' : self.ref_min_aa[self.llvals['max_p']][lower_bound : upper_bound]}

                ## calculate the site quality score Qscore
                if len(self.BEB_sites['site_scores'][site]['test_seq']) >= 9:
                    self.BEB_sites['site_scores'][site]['qscore'] = 1
                    if self.BEB_sites['site_scores'][site]['test_seq'][1] == '-':
                        self.BEB_sites['site_scores'][site]['qscore'] -= 0.05
                    if self.BEB_sites['site_scores'][site]['test_seq'][2] == '-':
                        self.BEB_sites['site_scores'][site]['qscore'] -= 0.15
                    if self.BEB_sites['site_scores'][site]['test_seq'][3] == '-':
                        self.BEB_sites['site_scores'][site]['qscore'] -= 0.30
                    if self.BEB_sites['site_scores'][site]['test_seq'][5] == '-':
                        self.BEB_sites['site_scores'][site]['qscore'] -= 0.30
                    if self.BEB_sites['site_scores'][site]['test_seq'][6] == '-':
                        self.BEB_sites['site_scores'][site]['qscore'] -= 0.15
                    if self.BEB_sites['site_scores'][site]['test_seq'][7] == '-':
                        self.BEB_sites['site_scores'][site]['qscore'] -= 0.05
                else:
                    self.BEB_sites['site_scores'][site]['qscore'] = 0

                ## add the score to the list to get the average
                self.BEB_sites['av_qscore'].append(self.BEB_sites['site_scores'][site]['qscore'])

            ## calculate concordence, add that to the metrics
            self.BEB_sites['concordence'] = self.BEB_sites['same_sites_number'] / self.BEB_sites['average_sites']

            ## average the scores for the gene
            self.BEB_sites['av_qscore'] = sum(self.BEB_sites['av_qscore']) / len(self.BEB_sites['av_qscore'])

            ## figure out the minimum of those two scores
            self.BEB_sites['min_qual_score'] = min(self.BEB_sites['av_qscore'], self.BEB_sites['concordence'])
        else:
            self.BEB_sites['av_qscore'] = 'NA'
            self.BEB_sites['concordence'] = 'NA'
            self.BEB_sites['min_qual_score'] = '0'

    def tree_length(self):
        """Grabs tree length for M7 and M8 from the PAML files
           Given as average nucelotide sub per codon"""
        
        results_exist = [] # list of the files that actually are there
        self.tree_length = {} # dictionary with the values
        
        ## check what outputs there are
        for alg in ['clus', 'mus', 'coff']:
            if os.path.isfile(self.paml_output(alg)):
                results_exist.append(alg)
        self.tree_length['M7_av'] = 'NA'
        self.tree_length['M8_av'] = 'NA'
        
        M7_values = []
        M8_values = []

        if not self.pamlerror:
            ## get M7 and M8 branch lengths for each algner. M7 is at [0] and M8 at [1]
            for algnr in results_exist:
                ## find the BEB sites from the paml results file
                with open(self.paml_output(algnr), 'r') as f:

                    self.tree_length[algnr] = []
                    for line in f.readlines():
                        if 'tree length =' in line:
                            self.tree_length[algnr].append(line.split()[3])
                M7_values.append(self.tree_length[algnr][0])
                M8_values.append(self.tree_length[algnr][1])
        
            ## make some averages
            if len(M7_values) > 0:
                self.tree_length['M7_av'] = str(sum(map(float, M7_values)) / len(M7_values))
            if len(M8_values) > 0:
                self.tree_length['M8_av'] = str(sum(map(float, M8_values)) / len(M8_values))


    def average_dn_ds(self):
        """Find the average dN/dS pairwise vales from the table in the PAML file"""
        results_exist = []
        self.dNdS = {}
        ## check what outputs there are
        for alg in ['clus', 'mus', 'coff']:
            if os.path.isfile(self.paml_output(alg)):
                results_exist.append(alg)
            self.dNdS['average'] = 'undetermined'
        
        ## for each of those results, grab the dNdS table
        tots = 0
        for algnr in results_exist:
            ## find the BEB sites from the paml results file
            with open(self.paml_output(algnr), 'r') as f:
                capture = False
                self.dNdS[algnr] = []
                for line in f.readlines():
                    if 'Use runmode = -2 for ML pairwise comparison.)' in line:
                        capture = True
      
                    elif 'Model 7: beta (10 categories)' in line:
                        capture = False
                        
                    if capture:
                        line = line.split() # makes this into a list of all items
                        for ele in line:
                            if '(' not in ele and ')' not in ele and not any(c.isalpha() for c in ele) and '-2' not in str(ele) and '=' not in str(ele):
                                ## this next bit is to take care of an error in PAML where 
                                ## dN values = 0 caused the pairwise dN/dS to evaluate as 
                                ## -1
                                if '-1' in ele:
                                    ele = '0'
                                self.dNdS[algnr].append(ele)
            
            if debug:
                print('pairwise dnds vales for ' + algnr + ': ' + str(self.dNdS[algnr]))

            ## get the average dNdS for each aligner that was run
            if len(self.dNdS[algnr]) > 0:
                th = 0
                for x in self.dNdS[algnr]:
                    th += float(x)
                
                self.dNdS[algnr + '_av'] = str(th / len(self.dNdS[algnr]))          
            else:
                self.dNdS[algnr + '_av'] = 'NA'
            
            if debug:
                print('Average dNdS for ' + algnr + ': ' + str(self.dNdS[algnr + '_av']))

            ## add to the total
            if self.dNdS[algnr + '_av'] != 'NA':
                tots += float(self.dNdS[algnr + '_av'])
        
        ## remove anything that equals NA
        temp_list = []
        for algnr in results_exist:
            if self.dNdS[algnr + '_av'] != 'NA':
                temp_list.append(algnr)
        results_exist = temp_list

        ## if none exist, set the output to NA
        if len(results_exist) < 1:
            self.dNdS['average'] = 'NA'

        ## after, average all the aligners together
        if self.dNdS['average'] != 'NA':
            self.dNdS['average'] = tots / len(results_exist)
        if debug:
            print('made it through dNdS')

    def site_at_species(self):
        ## figure out what that site is in every species that ran
        pass

    def ensemble_find(self, ensemble_lookup):
        ## use the ensemble table to grab the gene and isoform ensemble ID
        ## requires a dictionary of isoform name : ensemble ID
        # self.ensemble_transcript_id = ensemble_lookup[self.name.replace('-a-', '.')]['transcript_id']
        # self.ensemble_gene_id = ensemble_lookup[self.name.replace('-a-', '.')]['gene_id']        
        self.ensemble_transcript_id = ensemble_lookup[self.name.replace('.', '-a-')]['transcript_id']
        self.ensemble_gene_id = ensemble_lookup[self.name.replace('.', '-a-')]['gene_id']

    def print_info(self):
        ## check to see if PAML worked and the variables were found
        if not self.pamlerror:

            ## write to the results file in the gene folder
            with open(self.results_file(), 'w') as f:

                ##write results to file
                f.write('Gene name:\t' + self.name + '\n')
                f.write('Aligner with the least significant p-value:\t' + self.llvals['max_p'] + '\n')
                f.write('Number of species analyzed:\t' + str(len(self.species_used)) + '\t(' + ', '.join(self.species_used) + ')\n')
                f.write('Average Pairwise dNdS:\t' + str(self.dNdS['average']))
                f.write('M7-M8 pval:\t' + str(self.M7_M8) + '\n')
                f.write('M8-M8a pval:\t' + str(self.M8a) + '\n')
                f.write('Number of sites common with all aligners:\t' + str(self.BEB_sites['same_sites_number']) + '\n')
                f.write('Site positions:\t' + str(self.BEB_sites['same_sites']).strip('[]') + '\n\n')


                ## show the two sequences on top of each other
                site_write = True
                try:
                    f.write('Clustal aligned sequence:  \t' + self.ref_min_aa['clus'] + '\n')
                except: ## for this one, if it was never made, make it here. This next bit is slightly modified from the site_get function above
                    site_write = False

                try: ## try for Muscle
                    f.write('Muscle aligned sequence:   \t' + self.ref_min_aa['mus'] + '\n')
                except: ## for these two, don't try and make them, just set to false
                    site_write = False

                try: ## try for T-Coffee
                    f.write('T-Coffee aligned sequence: \t' + self.ref_min_aa['coff'] + '\n')
                except:
                    site_write = False

                f.write('Reference sequence:        \t' + self.ref_aa.lower() + '\n')

                ## write the segments about the BEB sites with a small alignment on either side
                if self.BEB_sites['same_sites_number'] > 0:
                    f.write('\nAlignment of sites under selection:\n- Alignments are centered around the site, with 4 amino acids on either side. \n- There may be more than one site per alignment if they are close together. \n\n')
                    for site in self.BEB_sites['site_scores']:
                        f.write('Site position:\t' + str(site) + '\n')
                        f.write('PAML sequence:\t' + self.BEB_sites['site_scores'][site]['ref_seq'] + '\n')
                        f.write('Reference seq:\t' + self.BEB_sites['site_scores'][site]['test_seq'] + '\n\n')

            ## return these as a line so they can be printed to a results file
            return str('\n' + self.name
                       + '\t' + str(self.ensemble_transcript_id)
                       + '\t' + str(self.parent_gene.replace('-a-', '.'))
                       + '\t' + str(self.ensemble_gene_id)        
                       + '\t' + str(self.numspec)
                       + '\t' + str(len(self.ref_aa))
                       + '\t' + str(self.per_aligned)
                       + '\t' + str(self.dNdS['average'])
                       + '\t' + str(self.tree_length['M7_av'])
                       + '\t' + str(self.tree_length['M8_av'])
                       + '\t' + str(self.M7_M8)
                       + '\t' + str(self.M8a)
                       + '\t' + str(self.BEB_sites['average_sites'])
                       + '\t' + str(self.BEB_sites['same_sites_number'])
                       + '\t' + str(self.BEB_sites['min_qual_score'])
                       + '\t' + str(self.BEB_sites['av_qscore'])
                       + '\t' + str(self.BEB_sites['concordence'])
                       + '\t' + str(self.llvals['max_p'])
                       + '\t' + str(self.BEB_sites['same_sites_aa']))

        ## else start checking for the things that IDML either did or did not do
        else:
            ## first, check if a protein file exists
            if os.path.isfile(self.prot_file()):
                ## next check if alignment file
                if os.path.isfile(self.alignment_file('clus')):
                    ## finally check if a PAML file was started
                    if os.path.isfile(self.paml_output('clus')):
                        return str('\n' + self.name
                                   + '\t' + str(self.ensemble_transcript_id)
                                   + '\t' + str(self.parent_gene.replace('-a-', '.'))
                                   + '\t' + str(self.ensemble_gene_id)
                                   + '\t' + str(self.numspec)
                                   + '\tPAML attempted but failed.')
                    else:
                        return str('\n' + self.name
                                   + '\t' + str(self.ensemble_transcript_id)
                                   + '\t' + str(self.parent_gene.replace('-a-', '.'))
                                   + '\t' + str(self.ensemble_gene_id)
                                   + '\t' + str(self.numspec)
                                   + '\tThere is an alignment, but PAML did not start - likely too few species.')
                else:
                    return str('\n' + self.name
                               + '\t' + str(self.ensemble_transcript_id)
                               + '\t' + str(self.parent_gene.replace('-a-', '.'))
                               + '\t' + str(self.ensemble_gene_id)
                               + '\t' + str(self.numspec)
                               + '\tNo alignment file - likely just one species.')
            else:
                return str('\n' + self.name
                           + '\t' + str(self.ensemble_transcript_id)
                           + '\t' + str(self.parent_gene.replace('-a-', '.'))
                           + '\t' + str(self.ensemble_gene_id)
                           + '\t' + str(self.numspec)
                           + '\tNo protein file for this isoform.')

    def sites_by_species(self):
        """Find the identity of the BEB sites in all the different species"""

        ## make sure that there are acutally sites
        if len(self.BEB_sites['same_sites']) > 0 and 'NA' not in self.BEB_sites['same_sites']:
            return self.name

            ## self.alignment[algnr] is the .paml input in dicitonary for by species for all the aligners
            #return self.alignment['clus']

################################################################
################ run commands
################################################################

## get all the isforms into a list
isoform_list = []
with open(isoforms_file, 'r') as f:
    for line in f.readlines():
        isoform_list.append(line.strip())
if debug:
    print(isoform_list)
## for each isoform, make a file for the parent gene and copy that isoform file into it
    ## required for the pathing to be consistant with the rest of corsair
for numb in isoform_list:
    ## make a folder of the parent gene
    par_path = genes_file + '/' + numb.split('_')[0]
    is_path = genes_file + '/' + numb
    if not os.path.exists(par_path):
        os.mkdir(par_path)
    ## move the isoform file into that path
    if os.path.exists(is_path):
        movedirectory(is_path, par_path + '/' + numb)

## load in all the pickle files
isodic = {}
no_data_list = []
gn_count = 0
gn_max = len(isoform_list)

#@ should remove this try loop in the future
for gn in isoform_list:
    gn_count += 1
    #@ for watchting terminal progress
    # if (gn_count % 10) == 0:
    #     print('Opening files: ' + str(gn_count) + ' / ' + str(gn_max))
    gn_name = gn.split('_')[0]
    try:
        ## load all the isoforms into a dictionary if they were there
        with open(genes_file + '/' + gn_name + '/' + gn + '/' + gn + '_save_file.pkl', 'rb') as input:
            isodic[gn] = pickle.load(input)
        isodic[gn].link_parent(gn_name)

    except:
        ## make a list of all the isoforms that don't have data
        no_data_list.append(gn)


## make a dicitonary that has all the ensemble ID values
with open(ensemble_conversion_file, 'rb') as input:
    ensemble_lookup = pickle.load(input)

## make the output files
with open(result_out_file, 'w') as f:
    f.write('Transcript name'
            + '\tEnsemble transcript ID'
            + '\tGene name'
            + '\tEnsemble gene ID'
            + '\tSpecies analyzed'
            + '\tReference Length'
            + '\tPercent aligned'
            + '\tAv. Pairwise dNdS'
            + '\tM7 Tree Dist'
            + '\tM8 Tree Dist'
            + '\tM7-M8 pval'
            + '\tM8-M8a pval'
            + '\tAverage sites per aligner'
            + '\tCommon sites'
            + '\tMin quality score'
            + '\tAv. Site Qscore'
            + '\tConcordance'
            + '\tMax aligner'
            + '\tSite positions')

    ## have all the isoforms go through the print steps
    gn_count = 0
    real_count = 0

    ## process and print information for each isoform
    for iso in isodic:
        #@ for watchting terminal progress
        # if (gn_count % 10) == 0:
        #     print('Printing files: ' + str(gn_count) + ' / ' + str(gn_max))



        ## parses variables into useable
        isodic[iso].variable_check()

        ## if an M8a output exists for an isoform
        if isodic[iso].check_positive():
            ## grabs the BEB sites from the PAML results file
            for aln in ('clus', 'mus', 'coff'):
                isodic[iso].site_get(aln)

            ## reduces to the common sites for all aligners
            isodic[iso].site_analysis()

            ## this asigns the aa to the BEB sites
            isodic[iso].site_id()

            ## make the quality scores for BEB sites
            isodic[iso].site_quality()

        ## get the average dN/dS
        isodic[iso].tree_length()
        
        ## get the average dN/dS
        isodic[iso].average_dn_ds()

        ## match the isform to an ensemble ID
        isodic[iso].ensemble_find(ensemble_lookup)

        ## write to the main results file and to the gene results file (in function)
        f.write(isodic[iso].print_info())

        ## find all the BEB positions for the gene across all the species
        # print(isodic[iso].sites_by_species())

        ## iterate the counting variable
        gn_count += 1

        ## puts a period back in the gene name. Only does this for self.name,
        ## so the isoform class object can still be called from the list
        isodic[iso].period_back_replace()

    # make the genes that did nothing into isoform objects
    for iso in no_data_list:
        isodic[iso] = isoform(iso)

    ## print the list of genes for which there is no data with a message
    print('Some genes had no data; appending to end of results')
    for no_gene in no_data_list:
        isodic[no_gene].ensemble_find(ensemble_lookup)
        f.write('\n' + str(no_gene)
              + '\t' + str(isodic[no_gene].ensemble_transcript_id)
              + '\t' + str(isodic[no_gene].name.replace('-a-', '.').split('_')[0])
              + '\t' + str(isodic[no_gene].ensemble_gene_id)
              + '\t' + str(isodic[iso].prev_ids)
              + '\tND'
              + '\tThere was no data for this isoform')

## save the pickle file for isodic
with open(result_out_file + '.pkl', 'wb') as output:
    pickle.dump(isodic, output, pickle.HIGHEST_PROTOCOL)
