#!/usr/bin/env python3
"""Makes a list of the genes, and returns the unique isoforms"""

CDS_FILE = '/Users/Jacob/Corsair/control/murinae_files/Mus_musculus.GRCm38.cds.all.fa'

## scan through the transcript file
def parse_names(file1):
    """Read all the fasta header lines to get gene and trascript names
       Returns dictionary of class objects
       Requies the gene_info class"""

    isoforms = {}
    with open(file1, 'r') as f:
        for line in f.readlines():
            ## filter for FASTA header
            if '>' in line:                         
                ens_tran_id = line.split('>')[1].split(' cds')[0]
                ens_gene_id = line.split('gene:')[1].split(' gene_bio')[0]
                gene_symbol = line.split('gene_symbol:')[1].split(' description')[0]
                isoforms[ens_tran_id] = Isoform_info(ens_tran_id, ens_gene_id, gene_symbol)
    return isoforms

def link_isoforms (isoformdic):
    """Links all the isofrms together in one dictionary
       Prints the """
    genedic = {}
    for x in isoformdic:
        if isoformdic[x].ids['gene_name'] in genedic.keys():
            genedic[isoformdic[x].ids['gene_name']].append(x)
        else:
            genedic[isoformdic[x].ids['gene_name']] = [x]
    return genedic

def trans(sequence):
    '''takes a nucleotide string and returns the translation as an amino acid string'''

    # codon table
    transtable = {
        'agg' : 'R',
        'aga' : 'R',
        'agc' : 'S',
        'agt' : 'S',
        'aag' : 'K',
        'aaa' : 'K',
        'aac' : 'N',
        'aat' : 'N',
        'aca' : 'T',
        'acc' : 'T',
        'acg' : 'T',
        'act' : 'T',
        'atg' : 'M',
        'ata' : 'I',
        'atc' : 'I',
        'att' : 'I',
        'cgg' : 'R',
        'cga' : 'R',
        'cgc' : 'R',
        'cgt' : 'R',
        'cag' : 'Q',
        'caa' : 'Q',
        'cac' : 'H',
        'cat' : 'H',
        'ccg' : 'P',
        'cca' : 'P',
        'ccc' : 'P',
        'cct' : 'P',
        'ctg' : 'L',
        'cta' : 'L',
        'ctc' : 'L',
        'ctt' : 'L',
        'tgg' : 'W',
        'tga' : 'X',
        'tgc' : 'C',
        'tgt' : 'C',
        'tag' : 'X',
        'taa' : 'X',
        'tac' : 'Y',
        'tat' : 'Y',
        'tcg' : 'S',
        'tca' : 'S',
        'tcc' : 'S',
        'tct' : 'S',
        'ttg' : 'L',
        'tta' : 'L',
        'ttc' : 'F',
        'ttt' : 'F',
        'ggg' : 'G',
        'gga' : 'G',
        'ggc' : 'G',
        'ggt' : 'G',
        'gag' : 'E',
        'gaa' : 'E',
        'gac' : 'D',
        'gat' : 'D',
        'gcg' : 'A',
        'gca' : 'A',
        'gcc' : 'A',
        'gct' : 'A',
        'gtg' : 'V',
        'gta' : 'V',
        'gtc' : 'V',
        'gtt' : 'V'}

    # this is a counting variable
    k = 3

    # this the blank string for the amino acid sequence
    aaseq = ''

    # stops when it gets to the end of the sequence=
    while k <= len(sequence):
        # try to add a codon. will reject because it won't find the key if the variable is blank
        try:
            aaseq += transtable[sequence[(k-3):k].lower()]
        except:
            aaseq += '' ## adds a blank space if no translation is possible

        # count up 3
        k = k + 3

    # return value is a string of amino acids
    return aaseq

def fasta_read(file):
    '''reads a file, outputs a dicitonary'''
    wholefile = {}
    with open(file,'r') as f:
        for line in f.readlines():
            if ">" in line:
                keyname = [x.strip() for x in line]
                keyname = ''.join(keyname)
                keyname = keyname.split('>')[1]
                wholefile[keyname] = []
            elif '>' not in line:
                line = [x.strip() for x in line]
                line = ''.join(line)
                wholefile[keyname].append(line)
    for key in wholefile:
        wholefile[key] = ''.join(wholefile[key])

    return wholefile

class Isoform_info(object):
    """Links the gene name and ensemble IDs"""
    def __init__(self, tid, gn, gid):
        self.name = tid
        self.ids = {'transcrip_id' : tid,
                    'gene_name' : gn,
                    'gene_id' : gid}

ISOFORMS = parse_names(CDS_FILE)
GENES = link_isoforms(ISOFORMS)

print(str(len(list(GENES.keys()))))
print(len(list(ISOFORMS.keys())))


## store the geneome information as a pickle file
fasta_dic = fasta_read('Corsair/control/murinae_files/Mmus_all_isoforms.fasta')
with open('Corsair/control/murinae_files/primate_all_isoform_fasta_cds.pkl', 'wb') as output:
    pickle.dump (fasta_dic, output, pickle.HIGHEST_PROTOCOL)