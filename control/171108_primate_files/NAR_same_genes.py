#!/usr/bin/env python3

NAR_file = '/Users/Jacob/Desktop/gkx704_Supp/Van_der_Lee_et_al__supplementary_tables/Van_der_Lee_et_al__TableS2__biomart_ensembl78_one2one_ortholog_clusters.txt'
transcript_key_file = '/Users/Jacob/Corsair/control/primate_files/human_id_key_count_170821.txt'
base_ensb_file = '/Users/Jacob/Corsair/control/primate_files/Homo_sapiens.GRCh38.cds.all.fa'
NAR_isos = '/Users/Jacob/Corsair/control/primate_files/NAR_isos.txt'
check_results_file = '/Users/Jacob/Desktop/results.txt'
new_results_file = '/Users/Jacob/Desktop/new_results.txt'

## make a list of all the NAR genes that were used
NAR_list = []
with open(NAR_file,'r') as f:
    for line in f.readlines():
        if 'Ensembl.Gene.ID' not in line:
            line = line.split('\t')[0]
            NAR_list.append(line)

## search the base file for the gene ID for that transcript
gene_dic = {}
with open(base_ensb_file,'r') as f:
    for line in f.readlines():
        if '>' in line:
            gene_id = line.split('gene:')[1].split('.')[0]
            trans_id = line.split('>')[1].split(' ')[0]
        gene_dic[gene_id]=trans_id

## make a dictionary with all our ensemble_transcript_ID:gene_name pairs
key_dict = {}
with open(transcript_key_file,'r') as f:
    for line in f.readlines():
        key_dict[line.split('\t')[0]] = line.split('\t')[1].rstrip()

## make a file that contains a list of all the isoforms that the NAR paper tested, using our isoform names
NAR_isos_list = []
NAR_gene_list = []

with open(NAR_isos, 'w') as f:
    for x in NAR_list:
        try:
            f.write(key_dict[gene_dic[x]] + '\n')
            NAR_isos_list.append(key_dict[gene_dic[x]])
            NAR_gene_list.append(key_dict[gene_dic[x]].split('_')[0])
        except:
            pass

    ## open the results file and read in the results on each line
    results_line = {}
    with open(check_results_file, 'r') as b:
        for line in b.readlines():
            if 'Species' not in line:
                line = line.rstrip()
                results_line[line.split('\t')[0]]= line.split('\t')


    for knir in results_line.keys():
        knir_gene = knir.split('_')[0]
        if knir_gene in NAR_gene_list:
            if knir in NAR_isos_list:
                results_line[knir].append('found_in_NAR')
            else:
                results_line[knir].append('other_iso_of_gene_in_NAR')
        else:
            results_line[knir].append('new_gene')


with open(new_results_file, 'w') as f:
    # f.write('Gene name\tSpecies\tReference Length\tPercent aligned\tM7-M8 pval\tM8-M8a pval\tSite average\tNumber of sites\tSite positions\tMax aligner\n')
    f.write('Gene name\tSpecies\tReference Length\tPercent aligned\tM7-M8 pval\tM8-M8a pval\tSite average\tNumber of sites\tAv. Site Qscore\tConcordence\tMax aligner\tSite positions\tNAR_check\n')

    for x in results_line:
        print(str(results_line[x]))
        f.write('\t'.join(results_line[x][0:]) + '\n')
