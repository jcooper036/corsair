#!/usr/bin/env python3

import pickle

conversion_table = 'Corsair/control/bat_files/Myotis_lucifugus_id_conversion_table.txt'
all_genes = '/Users/Jacob/Corsair/control/bat_files/Myotis_lucifugus.cds.all.fa'
output_file = 'bat_ensemble_file.pkl'

## build a dictionary of dictionaries, where a transcript name is a dictionary that contains
## an ensemble transcript ID and an ensemble gene ID
with open(conversion_table, 'r') as fl:
    ensemble_id_dic = {}
    for n in fl.readlines():
        if 'Ensemble ID' not in n:
            ensemble_id_dic[n.strip().split('\t')[1]] = {'transcript_id' : n.strip().split('\t')[0]}

## load all the genes in as a dictionary
gene_dic = {}
with open(all_genes,'r') as f:
    for line in f.readlines():
        if '>' in line:
            gene_id = line.split('gene:')[1].split('.')[0]
            print(gene_id)
            trans_id = line.split('>')[1].split(' ')[0]
            print(trans_id)
        gene_dic[trans_id]=gene_id

## search that dictionary with the trasncript anme
for x in ensemble_id_dic:
    ensemble_id_dic[x]['gene_id'] = gene_dic[ensemble_id_dic[x]['transcript_id']]

## save it as a pickle file
with open(output_file, 'wb') as output:
    pickle.dump (ensemble_id_dic, output, pickle.HIGHEST_PROTOCOL)
