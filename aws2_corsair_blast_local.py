#! /usr/bin/env python3

"""Usage: aws_corsair_blast.py ISOFORM

Arguments:
    ISOFORM     Name of the isoform to be run

"""
#Tools for system
import docopt
import os
import subprocess
import pickle

## Initialize docopt
if __name__ == '__main__':

    try:
        arguments = docopt.docopt(__doc__)
        iso = str(arguments['ISOFORM'])
    except docopt.DocoptExit as e:
        print(e)

## control settings
cds_pickle_file = 'control/primate_all_isoform_fasta_cds.pkl'
genomes = ['Anan','Ptro','Ppan','Ggor','Pabe','Nleu','Mfas','Mmul','Mnem','Panu','Caty','Mleu','Csab','Cang','Nlar','Rbie','Sbol','Ccap','Cjac']
# genomes = ['Ptro','Ppan','Ggor']


################################################################
################ definintions
################################################################

def trans(sequence):
    # takes a nucleotide string and returns the translation as an amino acid string

    # codon table
    transtable = {
        'agg' : 'R',
        'aga' : 'R',
        'agc' : 'S',
        'agt' : 'S',
        'aag' : 'K',
        'aaa' : 'K',
        'aac' : 'N',
        'aat' : 'N',
        'aca' : 'T',
        'acc' : 'T',
        'acg' : 'T',
        'act' : 'T',
        'atg' : 'M',
        'ata' : 'I',
        'atc' : 'I',
        'att' : 'I',
        'cgg' : 'R',
        'cga' : 'R',
        'cgc' : 'R',
        'cgt' : 'R',
        'cag' : 'Q',
        'caa' : 'Q',
        'cac' : 'H',
        'cat' : 'H',
        'ccg' : 'P',
        'cca' : 'P',
        'ccc' : 'P',
        'cct' : 'P',
        'ctg' : 'L',
        'cta' : 'L',
        'ctc' : 'L',
        'ctt' : 'L',
        'tgg' : 'W',
        'tga' : 'X',
        'tgc' : 'C',
        'tgt' : 'C',
        'tag' : 'X',
        'taa' : 'X',
        'tac' : 'Y',
        'tat' : 'Y',
        'tcg' : 'S',
        'tca' : 'S',
        'tcc' : 'S',
        'tct' : 'S',
        'ttg' : 'L',
        'tta' : 'L',
        'ttc' : 'F',
        'ttt' : 'F',
        'ggg' : 'G',
        'gga' : 'G',
        'ggc' : 'G',
        'ggt' : 'G',
        'gag' : 'E',
        'gaa' : 'E',
        'gac' : 'D',
        'gat' : 'D',
        'gcg' : 'A',
        'gca' : 'A',
        'gcc' : 'A',
        'gct' : 'A',
        'gtg' : 'V',
        'gta' : 'V',
        'gtc' : 'V',
        'gtt' : 'V'}

    # this is a counting variable
    k = 3

    # this the blank string for the amino acid sequence
    aaseq = ''

    # stops when it gets to the end of the sequence=
    while k <= len(sequence):
        # try to add a codon. will reject because it won't find the key if the variable is blank
        try:
            aaseq += transtable[sequence[(k-3):k].lower()]
        except:
            aaseq += '' ## adds a blank space if no translation is possible

        # count up 3
        k = k + 3

    # return value is a string of amino acids
    return aaseq

################################################################
################ classes
################################################################

class isoform(object):
    """Holds information about an isoform"""

    def __init__(self, name):
        self.name = name
        self.scaffolds = {}

    def ref_nt(self, ref_nt):
        # nt_seq must be a string. also as is, this can't be over-written
        self.ref_nt = ref_nt

    def translate(self):
        # translates the nt_seq if there is one
        try:
            self.ref_aa = trans(self.ref_nt)
        except:
            print("ERROR: There is no nucleotide sequence for this gene")

    def add_scaffold(self, species, scaffold):
        self.scaffolds[species] = scaffold

    def ref_aa_file(self, file1):
        with open(file1, 'w') as f:
            f.write('>' + self.name + '\n' + self.ref_aa)

    def print_scaffolds(self):
        with open(self.name + '_scaffolds.txt', 'w') as f:
            for x in self.scaffolds:
                f.write(self.name + ':' + x + ',' + self.scaffolds[x] + '\n')

################################################################
################ run commands
################################################################
try:
    ## open the pickle file that contains all the CDS's
    fasta_dic = {}
    with open(cds_pickle_file, 'rb') as input:
        fasta_dic = pickle.load(input)

    ## make the object, get the reference sequence, and translate it
    isodic = {iso:isoform(iso)}
    isodic[iso].ref_nt(fasta_dic[iso])
    isodic[iso].translate()

    ## write a file with the trasnlation
    file_a = iso + '_aa.fasta'
    isodic[iso].ref_aa_file(file_a)

    # get the species:scaffold pairs with tBlastn
    for db in genomes:
        print('Running tblastn for ' + db)
        var=str(subprocess.check_output('tblastn -outfmt "6 sseqid" -query "' + file_a + '" -db genomes/'+ db + '_genome.fasta -max_target_seqs 1| head -n 1',shell=True))
        isodic[iso].add_scaffold(db, (var.replace('b','').replace('\\n','').replace('\'','')))

    ## output the scaffolds to a text file
    isodic[iso].print_scaffolds()

except:
    with open(iso + '_scaffolds.txt', 'w') as f:
        f.write(iso + ':ERROR')

## scp the file back to s3
