#! /usr/bin/env python3
## take a large compounded file where all scaffold information is in the format:
# isform_name:Spec,scaffold
## and make it into a saved dictionary to pipe into the next step


##############################################################
############ made this because two files to combine
##############################################################
import pickle
import os

# primate_genomes = ['Anan', 'Ptro', 'Ppan', 'Ggor', 'Pabe', 'Nleu', 'Mfas', 'Mmul', 'Mnem', 'Panu', 'Caty', 'Mleu', 'Csab', 'Cang', 'Nlar', 'Rbie','Sbol','Ccap','Cjac']
# murinae_genomes = ['Mmus', 'Mpah', 'Mcar', 'Mspr', 'Apse', 'Asyl', 'Rnor']
# cricetidae_genomes = ['Etal','Elut','Magr','Moch','Nlep','Psun','Mgla', 'Maur', 'Cgri']
# caniformia_genomes = ['Elut', 'Mput', 'Aful', 'Lwed', 'Nsch', 'Oros', 'Umar', 'Amel', 'Lpic', 'Clup']
# bovidae genomes = ['Bbis', 'Oari', 'Chir', 'Caeg', 'Bmut', 'Phod', 'Aler', 'Bind', 'Bbub', 'Btau']

genomes = ['Mluc', 'Mbra', 'Mdav', 'Efus', 'Mnat', 'Ppar', 'Drot', 'Rsin', 'Rfer', 'Harm', 'Mlyr', 'Pale', 'Pvam', 'Raeg', 'Ehel']

## set these variables first
input_dir1 = '180319_blast_results_bat' ## where are the files? first file
input_dir2 = '180319_blast_results_bat_Mlyr_only' ## where are the files? first file

output_file = '180319_bat_blast_scaffolds.pkl' ## will be the pickle output file
genomes_list_file = '180319_bat_for_IDML.txt' ## the output of genes to run
min_genome_cutoff = 4 ## what are the exculision criteria?

all_scaffolds = {}
for filename in os.listdir(input_dir1):
    ## checks against hidden files that start with '.'
    if not filename[0] == '.':
        with open(input_dir1 + '/' + filename, 'r') as f:
            for line in f.readlines():
                isoform = line.strip().split(':')[0]
                gene = line.strip().split(':')[0].split('_')[0]
                input1 = line.strip().split(':')[1].split(',')[0]
                input2 = line.strip().split(':')[1].split(',')[1]

                ## hedging against the results being flipped
                if input1 in genomes:
                    species = input1
                    scaffold = input2
                elif input2 in genomes:
                    species = input2
                    scaffold = input1

                if species and scaffold:
                    if gene in all_scaffolds.keys():
                        all_scaffolds[gene][species] = scaffold
                    else:
                        all_scaffolds[gene] = {species : scaffold}

for filename in os.listdir(input_dir2):
    ## checks against hidden files that start with '.'
    if not filename[0] == '.':
        with open(input_dir2 + '/' + filename, 'r') as f:
            for line in f.readlines():
                isoform = line.strip().split(':')[0]
                gene = line.strip().split(':')[0].split('_')[0]
                input1 = line.strip().split(':')[1].split(',')[0]
                input2 = line.strip().split(':')[1].split(',')[1]

                ## hedging against the results being flipped
                if input1 in genomes:
                    species = input1
                    scaffold = input2
                elif input2 in genomes:
                    species = input2
                    scaffold = input1

                if species and scaffold:
                    if gene in all_scaffolds.keys():
                        all_scaffolds[gene][species] = scaffold
                    else:
                        all_scaffolds[gene] = {species : scaffold}

for gene in all_scaffolds:
    print('\n')
    for species in all_scaffolds[gene]:
        print(species, all_scaffolds[gene][species])

## write a pickle file that contains all the species : scaffold pairs for a gene
with open(output_file, 'wb') as output:
    pickle.dump (all_scaffolds, output, pickle.HIGHEST_PROTOCOL)

## make a dictionary of all the isoforms
temp_list = os.listdir(input_dir1)
isoform_dict = {}
for i in temp_list:
    isoform_dict[i.split('_')[0]] = i.split('_scaffold')[0]

## make a dictionary that has all the genes in the genome and how many scaffolds they have
counting_dict = {}
for x in all_scaffolds:
    if len(all_scaffolds[x].keys()) in counting_dict.keys():
        counting_dict[len(all_scaffolds[x].keys())].append(x)
    else:
        counting_dict[len(all_scaffolds[x].keys())] = [x]

    ## to make a list of genes with a certain number of hits
    with open(genomes_list_file, 'a') as output:
        if len(all_scaffolds[x].keys()) >= min_genome_cutoff:
            output.write(isoform_dict[x].replace('.','-a-') + '\n')

## print some stats on how many ran with each number of genomes
for x in counting_dict:
    print (str(x) + ' : ' + str(len(counting_dict[x])))
    # print(len(list(all_scaffolds[x].keys)))

print('\nDone\n')
