Command line: [exonerate --model protein2genome --query genes/CG11271/CG11271_PB/CG11271_PB_files/CG11271_PB_prot.txt --target temp/KB462646.1_CG11271_PB.fasta --ryo forcebeams 
>Dbia
%tcs
 --bestn 1]
Hostname: [pathfinder.local]

C4 Alignment:
------------
         Query: CG11271_PB
        Target: KB462646.1 Drosophila biarmipes unplaced genomic scaffold scf7180000302193, whole genome shotgun sequence
         Model: protein2genome:local
     Raw score: 691
   Query range: 6 -> 139
  Target range: 2257604 -> 2258576

       7 : AspValProSerAlaAlaProValLeuAspGlyAlaMetAspIleAsnThrAlaLe :      25
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           AspValProSerAlaAlaProValLeuAspGlyAlaMetAspIleAsnThrAlaLe
 2257605 : GATGTTCCCTCCGCTGCCCCCGTGCTCGATGGCGCCATGGACATTAACACCGCCCT : 2257659

      26 : uGlnGluValLeuLysLysSerLeuIleAlaAspGlyLeuValHisGlyIleHisG :      44
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           uGlnGluValLeuLysLysSerLeuIleAlaAspGlyLeuValHisGlyIleHisG
 2257660 : GCAGGAGGTCCTGAAGAAGTCCCTGATCGCCGATGGACTCGTCCACGGCATCCACC : 2257716

      45 : lnAlaCysLysAlaLeuAsp{Ly}  >>>> Target Intron 1 >>>>  {s} :      51
           ||||||||||||||||||||{||}            573 bp           {|}
           lnAlaCysLysAlaLeuAsp{Ly}++                         ++{s}
 2257717 : AGGCCTGCAAGGCCCTGGAC{AA}gt.........................ag{G} : 2258312

      52 : ArgGlnAlaValLeuCysIleLeuAlaGluSerPheAspGluProAsnTyrLysLy :      70
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           ArgGlnAlaValLeuCysIleLeuAlaGluSerPheAspGluProAsnTyrLysLy
 2258313 : CGTCAGGCTGTCCTCTGCATCCTGGCCGAGTCCTTCGACGAGCCCAACTACAAGAA : 2258367

      71 : sLeuValThrAlaLeuCysAsnGluHisGlnIleProLeuIleArgValAspSerH :      89
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           sLeuValThrAlaLeuCysAsnGluHisGlnIleProLeuIleArgValAspSerH
 2258368 : GCTGGTCACCGCCCTGTGCAACGAGCACCAGATCCCCCTGATCCGCGTGGACTCGC : 2258424

      90 : isLysLysLeuGlyGluTrpSerGlyLeuCysLysIleAspLysGluGlyLysPro :     107
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           isLysLysLeuGlyGluTrpSerGlyLeuCysLysIleAspLysGluGlyLysPro
 2258425 : ACAAGAAGCTGGGCGAGTGGTCCGGCCTGTGCAAGATCGACAAGGAGGGCAAGCCC : 2258478

     108 : ArgLysValCysGlyCysSerValValValIleLysAspPheGlyGluGluThrPr :     126
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           ArgLysValCysGlyCysSerValValValIleLysAspPheGlyGluGluThrPr
 2258479 : CGCAAGGTGTGCGGCTGCTCCGTGGTCGTGATCAAGGATTTCGGTGAGGAGACACC : 2258535

     127 : oAlaLeuAspValValLysAspHisLeuArgGlnAsnSer :     139
           ||||||||||||||||||||||||||||||||||||||||
           oAlaLeuAspValValLysAspHisLeuArgGlnAsnSer
 2258536 : CGCTTTGGACGTGGTTAAGGACCATCTCAGGCAGAACAGC : 2258576

vulgar: CG11271_PB 6 139 . KB462646.1 2257604 2258576 + 691 M 44 132 S 0 2 5 0 2 I 0 569 3 0 2 S 1 1 M 88 264
forcebeams 
>Dbia
GATGTTCCCTCCGCTGCCCCCGTGCTCGATGGCGCCATGGACATTAACACCGCCCTGCAGGAGGTCCTGA
AGAAGTCCCTGATCGCCGATGGACTCGTCCACGGCATCCACCAGGCCTGCAAGGCCCTGGACAAGCGTCA
GGCTGTCCTCTGCATCCTGGCCGAGTCCTTCGACGAGCCCAACTACAAGAAGCTGGTCACCGCCCTGTGC
AACGAGCACCAGATCCCCCTGATCCGCGTGGACTCGCACAAGAAGCTGGGCGAGTGGTCCGGCCTGTGCA
AGATCGACAAGGAGGGCAAGCCCCGCAAGGTGTGCGGCTGCTCCGTGGTCGTGATCAAGGATTTCGGTGA
GGAGACACCCGCTTTGGACGTGGTTAAGGACCATCTCAGGCAGAACAGC

-- completed exonerate analysis
