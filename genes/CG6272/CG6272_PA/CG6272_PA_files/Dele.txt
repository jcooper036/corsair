Command line: [exonerate --model protein2genome --query genes/CG6272/CG6272_PA/CG6272_PA_files/CG6272_PA_prot.txt --target temp/KB458268.1_CG6272_PA.fasta --ryo forcebeams 
>Dele
%tcs
 --bestn 1]
Hostname: [pathfinder.local]

C4 Alignment:
------------
         Query: CG6272_PA
        Target: KB458268.1 Drosophila elegans unplaced genomic scaffold scf7180000490564, whole genome shotgun sequence:[revcomp]
         Model: protein2genome:local
     Raw score: 459
   Query range: 0 -> 111
  Target range: 1997959 -> 1997492

       1 : MetProAlaLysLysArgThr<-><->AlaAlaSerThrSerLysAsnSerAspSe :      17
           ||||||||||||!:!||||||      |||||||||||||||||||||.!!|||||
           MetProAlaLysArgArgThrAlaAsnAlaAlaSerThrSerLysAsnGlyAspSe
 1997959 : ATGCCAGCCAAAAGAAGAACTGCAAATGCGGCGTCAACCAGCAAAAACGGCGATTC : 1997905

      18 : rProLeuSerProHisThrAspAspProAlaTyrLysGluLysArgLysLysAsnA :      36
           |||||||||||||!...!!|||||||||||||||:::|||||||||||||||||||
           rProLeuSerProArgAlaAspAspProAlaTyrGlnGluLysArgLysLysAsnA
 1997904 : CCCATTAAGTCCGCGGGCAGACGATCCAGCGTAccaagaaaaaagaaagaaaaaca : 1997848

      37 : snGlu  >>>> Target Intron 1 >>>>  AlaValGlnArgThrArgGluL :      45
           |||||            128 bp           ||||||||||||||||||||||
           snGlu++                         ++AlaValGlnArgThrArgGluL
 1997847 : atgaggt.........................agGCTGTACAGCGCACTCGTGAGA : 1997693

      46 : ysThrLysLysSerAlaGluGluArgLysLysArgIleAspAspLeuArgLysGln :      63
           |||||||||||||||||||||||||||||||||||||||||||||||||||||:!!
           ysThrLysLysSerAlaGluGluArgLysLysArgIleAspAspLeuArgLysGlu
 1997692 : AGACCAAGAAATCGGCCGAGGAACGGAAAAAACGCATCGACGATTTACGAAAGGAG : 1997639

      64 : AsnAspAlaLeuLysValGlnIleGluThrSerGluLysHisIleSerThrLeuAr :      82
           |||! !||||||||||||:!!||||||  !.!!|||||||||||||||||||||||
           AsnAlaAlaLeuLysValLysIleGluGlnGlyGluLysHisIleSerThrLeuAr
 1997638 : AACGCCGCTTTAAAAGTGAAGATAGAGCAGGGCGAAAAACATATATCCACTCTGAG : 1997582

      83 : gAspLeuIleIleGlnGlyGluLysThrGluAspGlyHisArgIleIleGlnGluI :     101
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           gAspLeuIleIleGlnGlyGluLysThrGluAspGlyHisArgIleIleGlnGluI
 1997581 : GGATCTGATAATTCAGGGCGAGAAAACTGAGGACGGACACCGCATTATCCAAGAGA : 1997525

     102 : leLeuAlaGluProAspProAspProLysAsp :     111
           ||||||||!  ||||||||||||  !!!.|||
           leLeuAlaGlyProAspProAspAspAsnAsp
 1997524 : TACTCGCTGGCCCGGATCCAGATGACAATGAC : 1997493

vulgar: CG6272_PA 0 111 . KB458268.1 1997959 1997492 - 459 M 7 21 G 0 6 M 30 90 5 0 2 I 0 124 3 0 2 M 74 222
forcebeams 
>Dele
ATGCCAGCCAAAAGAAGAACTGCAAATGCGGCGTCAACCAGCAAAAACGGCGATTCCCCATTAAGTCCGC
GGGCAGACGATCCAGCGTAccaagaaaaaagaaagaaaaacaatgagGCTGTACAGCGCACTCGTGAGAA
GACCAAGAAATCGGCCGAGGAACGGAAAAAACGCATCGACGATTTACGAAAGGAGAACGCCGCTTTAAAA
GTGAAGATAGAGCAGGGCGAAAAACATATATCCACTCTGAGGGATCTGATAATTCAGGGCGAGAAAACTG
AGGACGGACACCGCATTATCCAAGAGATACTCGCTGGCCCGGATCCAGATGACAATGAC

-- completed exonerate analysis
