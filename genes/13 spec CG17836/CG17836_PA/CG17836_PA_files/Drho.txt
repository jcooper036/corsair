Command line: [exonerate --model protein2genome --query genes/CG17836/CG17836_PA/CG17836_PA_files/CG17836_PA_prot.txt --target temp/KB452316.1_CG17836_PA.fasta --ryo forcebeams 
>Drho
%tcs
 --bestn 1]
Hostname: [pathfinder.local]

C4 Alignment:
------------
         Query: CG17836_PA
        Target: KB452316.1 Drosophila rhopaloa unplaced genomic scaffold scf7180000779900, whole genome shotgun sequence
         Model: protein2genome:local
     Raw score: 2223
   Query range: 0 -> 667
  Target range: 388703 -> 395559

      1 : MetIleGlnGluProAlaArgValGlnAlaAlaProThrPheGluAlaThrIleArgG :     20
          |||||||||||||||||||||||||||||||||||||||!:!|||||||||! !!:!|
          MetIleGlnGluProAlaArgValGlnAlaAlaProThrTyrGluAlaThrThrGlnG
 388704 : ATGATCCAGGAGCCAGCACGAGTACAGGCGGCACCCACATACGAGGCGACAACCCAGG : 388761

     21 : luAspAsnAlaLysTyrGlnGluThrAlaAlaGlyGlyThrTyrSerSerProAsnCy :     39
          |||||||||||  !|||! !.!. !!|||||||||!.!||| !!|||!!!||||||||
          luAspAsnAlaAlaTyrProAsnProAlaAlaGlyAlaThrAsnSerTrpProAsnCy
 388762 : AGGACAACGCAGCATACCCGAATCCAGCGGCTGGCGCGACAAACTCGTGGCCCAATTG : 388818

     40 : sSerCysSerGlnArgArgThrArgProThrGluProThrLysAspSerProThrSer :     58
          |!!!||||||||||||||||||! !||||||! !! !! !||||||||||||||||||
          sCysCysSerGlnArgArgThrThrProThrGlyLeuMetLysAspSerProThrSer
 388819 : CTGCTGCTCTCAGAGGAGAACGACGCCAACGGGGCTTATGAAGGATTCGCCTACGTCA : 388875

     59 : ThrIleThrThrIleProIleValIleAspSerLysProAsnProArgIleLeuLeuI :     78
          ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
          ThrIleThrThrIleProIleValIleAspSerLysProAsnProArgIleLeuLeuI
 388876 : ACGATCACCACAATACCGATCGTGATAGACAGCAAGCCAAATCCCAGAATTCTGCTGA : 388935

     79 : leArgValProThrSerAsnArgGlySerAsnSerAsnAsnAsnSerAsnSerThrAr :     97
          ||||||||||||||||||||      |||:::|||:::||||||            ||
          leArgValProThrSerAsn------SerSerSerSerAsnAsn------------Ar
 388936 : TCAGAGTGCCCacaagcaac------agcagcagcagcaacaat------------cg : 388974

     98 : gSerGlyThrGluHisArgAlaProArgThrArgIleIleArgIleProPheLeuPro :    116
          |||||||||||||||||||.!!||||||||||||||||||||||||||||||||||||
          gSerGlyThrGluHisArgThrProArgThrArgIleIleArgIleProPheLeuPro
 388975 : cAGCGGGACAGAGCACAGAACGCCTCGAACTCGCATCATAAGAATACCCTTTTTACCA : 389031

    117 : GlySerValProThrArgProProSerAsnGlnAlaAlaProAlaThrSerSerLeuV :    136
          ||||||||||||||||||||||||:!!|||||||||:!!|||||||||||| !!||||
          GlySerValProThrArgProProAlaAsnGlnAlaSerProAlaThrSerProLeuV
 389032 : GGGTCGGTACCAACGCGTCCGCCAGCCAACCAGGCCTCGCCTGCCACGAGTCCTTTAG : 389091

    137 : alAsnProIleGlySerAlaArgArgCysArgIleSerThrTyrProThrThrGlyTh :    155
          |||||||||||||||||! !||||||! !|||||||||..!|||||||||||||||||
          alAsnProIleGlySerGluArgArgSerArgIleSerValTyrProThrThrGlyTh
 389092 : TAAATCCGATCGGTTCGGAGAGGAGGTCCCGTATCTCAGTGTACCCCACGACTGGGAC : 389148

    156 : rLeuThrSerArgSerGlnProGluThrValAlaGlyLeuProThrThrAlaMetVal :    174
          |! !||||||||||||||||||!!:|||||||||||||||||||||||||||||||||
          rProThrSerArgSerGlnProAspThrValAlaGlyLeuProThrThrAlaMetVal
 389149 : GCCAACATCCCGATCGCAACCAGACACGGTGGCAGGTTTACCGACTACGGCAATGGTG : 389205

    175 : AsnGlnThrValProValLeuSerSerAlaProGlnThrGlnThrLeuAlaAlaMetA :    194
          ||||||||||||||||||! !||||||.!!||||||||||||||||||||||||||||
          AsnGlnThrValProValSerSerSerThrProGlnThrGlnThrLeuAlaAlaMetA
 389206 : AACCAAACGGTCCCGGTTTCGTCAAGCACGCCTCAGACACAGACCCTGGCAGCCATGG : 389265

    195 : laGlyIleGlnLeuThrGlyAlaAspGluProProSerArgArgAspValSerGlyAl :    213
          |||||:!!|||||||||||||||||||||||||||||||||||||||:!!||||||||
          laGlyLeuGlnLeuThrGlyAlaAspGluProProSerArgArgAspMetSerGlyAl
 389266 : CTGGTCTCCAGTTGACAGGTGCGGACGAGCCGCCATCCCGCCGGGATATGAGTGGAGC : 389322

    214 : aMetProThrAlaThrValGluGluIleAspValProValPheIleAsp{G}  >>>> :    230
          |||||||||||||||||||||||||||||||||||||||||||||||||{|}      
          aMetProThrAlaThrValGluGluIleAspValProValPheIleAsp{G}++    
 389323 : AATGCCTACTGCCACAGTTGAAGAGATAGACGTTCCGGTGTTCATTGAT{G}gt.... : 389376

    231 :  Target Intron 1 >>>>  {lu}TyrLeuGlnAspIleGluAlaThrSerSerT :    241
               4649 bp           {||}|||||||||||||||:!!||||||!!!||||
                               ++{lu}TyrLeuGlnAspIleGlnAlaThrThrSerT
 389377 : .....................ag{AA}TACCTGCAAGATATACAAGCTACTACATCGA : 394055

    242 : hrCysAsnSerGluAsnGlyLysGluIlePheThrGluThrAspIleLeuAspPheAs :    260
          |||||!:!|||||||||||||||||||||!!.||||||||||||||||||||||||||
          hrCysSerSerGluAsnGlyLysGluIleLeuThrGluThrAspIleLeuAspPheAs
 394056 : CGTGCAGCAGCGAAAACGGAAAGGAGATTTTAACTGAGACGGATATTCTTGACTTTGA : 394112

    261 : pIleAsnMetPheAlaGluGluAspLeuIleGluLeuGluGlyLysLysThrAspPro :    279
          ||||||||||||||||   ||||||||||||! !||||||||||||!!.|||||| !!
          pIleAsnMetPheAla---GluAspLeuIleValLeuGluGlyLysAsnThrAspSer
 394113 : TATAAACATGTTTGCC---GAGGATCTTATCGTGCTTGAGGGCAAGAATACGGATTCC : 394166

    280 : PheSerProAlaMetAspGlyMetAsnAsnAspAsnLeuAspAspAspPheLeuGlnL :    299
          !:!|||||||||||||||! !|||||||||||||||||||||:!!|||||||||   :
          TyrSerProAlaMetAspGluMetAsnAsnAspAsnLeuAspAsnAspPheLeuPheM
 394167 : TACAGTCCGGCGATGGACGAGATGAACAACGACAACCTAGACAACGACTTCTTATTTA : 394226

    300 : euAsnAspLeuLeuAsnGluGluGluThrPheAspMetSerSerMetSerGlyValGl :    318
          !!:!!! !! !  !   !  .!.|||  !   ..!|||!:!! !!  |||  ! !!!.
          etAspAlaProAlaLeuAlaAsnGluGluGluSerMetAsnMetThrSerIlePheAl
 394227 : TGGATGCCCCGGCGCTGGCTAACGAGGAGGAGTCCATGAACATGACATCGATATTTGC : 394283

    319 : yIleAspIleAsnGlnGluGlnGluLeuAspPheIleAsnIlePheHisAsnSerGln :    337
          !|||:!!|||!.!! !! !|||!!:  !|||.!!||||||  ! ! ! !!:!||||||
          aIleAsnIleThrProValGlnAspAspAspLeuIleAsnAlaValProSerSerGln
 394284 : CATCAATATTACCCCGGTGCAGGACGACGACCTCATTAACGCAGTACCCAGCTCGCAG : 394340

    338 : Pro  >>>> Target Intron 2 >>>>  ThrAlaMetLeuThrSerSerGluAs :    347
          ! !            30 bp            .!!  !:!:   |||!!!|||:!!||
          Gln--                         ++AlaMetLeu---ThrThrSerGlnAs
 394341 : CAGca.........................agGCTATGCTC---ACCACCTCTCAGGA : 394397

    348 : pProTyrLeuAlaThrTyrSerGlnAlaThrSerThrSerSerThrThrLysPheIle :    366
          ||||||||||..!|||!:!|||||||||||||||||||||||||||||||||!:!.!!
          pProTyrLeuCysThrPheSerGlnAlaThrSerThrSerSerThrThrLysTyrPhe
 394398 : CCCCTACCTGTGCACCTTCTCGCaggccacatccacatccagcACCACCAAGTACTTC : 394454

    367 : ThrGluGluProLeuLeuAlaAspMetThrSerPheAspIleSer<-><->TyrThrS :    384
          |||||||||||||||:!!:!!|||  !.!!|||!:!!!:! !|||      |||  !!
          ThrGluGluProLeuIleSerAspAlaAlaSerTyrGluThrSerSerLeuTyrGlyT
 394455 : ACGGAGGAACCCCTGATTTCCGACGCGGCCAGCTACGAAACCAGTAGTCTCTACGGGT : 394514

    385 : erSerGlnGlu<->LeuGlyPheGlyLeuValGluAspVal<-><->SerGlySerPh :    400
           !|||||||||   |||||||||||||||! !|||||||||      |||||||||.!
          yrSerGlnGluThrLeuGlyPheGlyLeuGluGluAspValArgProSerGlySerLe
 394515 : ACAGCCAGGAGACGCTCGGCTTCGGCCTTGAGGAGGACGTCAGGCCCAGCGGCAGCCT : 394571

    401 : eAlaSerSerCysSerAspValAlaAlaAlaIleProAlaProIleGlnHisProIle :    419
          !|||!!!|||..!:!!|||||||||!.!|||! !||||||||||||   |||||||||
          uAlaCysSerAlaAlaAspValAlaValAlaThrProAlaProIleCysHisProIle
 394572 : TGCCTGCAGTGCCGCCGACGTCGCCGTGGCCACCCCAGCACCCATTTGCCATCCCATC : 394628

    420 : IleThrAsnIleLysArgSerAlaProAlaPro<->ValGluSerGlnProThrLysA :    438
          |||:!!  !:!:|||||||||||||||||||||   |||||||||||| !!! !||||
          IleSerAlaLeuLysArgSerAlaProAlaProValValGluSerGlnThrIleLysA
 394629 : ATCTCCGCCCTGAAACGATCTGCTCCGGCCCCCGTGGTCGAGTCGCAGACGATCAAGC : 394688

    439 : rgSerArgLeuMetLeuGlnIleLysThrGluLeuAlaProAlaPheSerProThrTh :    457
          ||.!!||||||  !|||:!!||| ! !:!:!!! !.!!|||!.!! !||||||!:!||
          rgGlyArgLeuAlaLeuLysIleAspSerGlnSerThrProValSerSerProSerTh
 394689 : GCGGCCGACTGGCGCTTAAGATCGACAGCCAGTCAACGCCAGTGTCCAGCCCGAGCAC : 394745

    458 : rProGluIleIleAsnGlnLeuLeuAsnAspAspArg  >>>> Target Intron  :    470
          |||||||||||||.!.!!.|||||||||  !||||||            188 bp   
          rProGluIleIleGluHisLeuLeuAsnPheAspArg++                   
 394746 : GCCCGAAATCATTGAGCATCTCCTGAATTTTGATAGGgt................... : 394786

    471 : 3 >>>>  PheGluAlaProThrThrSerThrSerThrSerThrSerAsnThrSerIl :    486
                  .!!|||||| !!!.!|||||||||!!!!.!!:!||||||! !!.!|||||
                ++LeuGluAlaAlaAsnThrSerThrThrAsnAsnThrSerIleAsnSerIl
 394787 : ......agCTCGAGGCCGCCAACACCAGTACCACCAACAACACCTCCATCAACAGCAT : 395020

    487 : eSerSerSerThrHisAlaAspIleValGluAspLeuArgSerAlaGluGluGluThr :    505
          |   !!!||||||!!.|||!!:||||||||||||||||||||||||||||||||||||
          e---ThrSerThrGlnAlaGluIleValGluAspLeuArgSerAlaGluGluGluThr
 395021 : T---ACCAGCACTCAAGCAGAAATTGTAGAGGATCTACGCAGCGCCGAAGAGGAGACC : 395074

    506 : ThrThrAspPheSerAlaProAsnThrProHisSerAsnTyrSerAlaSerSerSerC :    525
          |||!.!||||||||| !!|||||||||||||||||||||||||||||||||!!!||| 
          ThrAsnAspPheSerProProAsnThrProHisSerAsnTyrSerAlaSerCysSerS
 395075 : ACCAACGACTTCTCGCCGCCGAACACGCCGCACAGCAACTACTCAGCTAGCTGTAGCA : 395134

    526 : ysAlaAlaProThrCysGlnThrGlyTyrGlyGlyPheLeuThrAlaProThrSerPr :    544
          !!|||!.!||||||||||||||||||!:!|||||||||||||||||||||.!!|||||
          erAlaValProThrCysGlnThrGlyPheGlyGlyPheLeuThrAlaProAlaSerPr
 395135 : GTGCAGTGCCCACCTGCCAGACGGGATTCGGTGGCTTCCTCACTGCTCCCGCCTCACC : 395191

    545 : oAlaTyrSerThrAlaSerThrSerValPheSerProSerProAlaSerGlyIleSer :    563
          ||||||||||..!||||||||||||  !|||||||||||||||:!!!:!|||  !!:!
          oAlaTyrSerValAlaSerThrSerGlnPheSerProSerProSerAsnGlySerAsn
 395192 : CGCCTACTCGGTGGCCAGCACCTCCCAGTTCAGTCCCTCTCCATCCAACGGATCCAAT : 395248

    564 : GlyLysArgLysArgGlyArgProAlaLysAspHisAlaAspGlyProAspProValL :    583
          ||||||||||||||||||||||||||||||||||||||||||||||||||||||! !:
          GlyLysArgLysArgGlyArgProAlaLysAspHisAlaAspGlyProAspProGluI
 395249 : GGCAAACGGAAGCGCGGCCGTCCTGCCAAGGATCATGCCGATGGACCCGATCCGGAAA : 395308

    584 : euMetSerSerMetLysSerGluGluGluArgLysAlaTyrGlnAspArgLeuLysAs :    602
          !!|||||| !!|||!!..!!   !!:||||||||||||||||||||||||:!!|||||
          leMetSerArgMetAsnGly---AspGluArgLysAlaTyrGlnAspArgIleLysAs
 395309 : TCATGTCGCGCATGAACGGC---GACGAGAGGAAGGCGTACCAAGACAGGATCAAAAA : 395362

    603 : nAsnGluAlaSerArgValSerArgArgLysThrLysValArgGluGluGluGluLys :    621
          |||||||||||||||||||||||||||||||||||||   ||||||||||||||||||
          nAsnGluAlaSerArgValSerArgArgLysThrLysLysArgGluGluGluGluLys
 395363 : CAACGAGGCCAGCCGAGTCTCGCGCCGGAAAACGAAGAAgcgcgaggaggaggagaag : 395419

    622 : ArgAlaGluAspThrLeuLeuAlaGluAsnLeuArgLeuArgAlaArgAlaAspGluV :    641
            !|||||||||  !|||:!!|||||||||! !!.!|||||||||! !|||!!:!  |
          SerAlaGluAspGluLeuValAlaGluAsnGlnHisLeuArgAlaLeuAlaGluAlaV
 395420 : agCGCCGAGGACGAGTTGGTAGCCGAGAATCAACATCTGCGTGCCCTGGCGGAAGCTG : 395479

    642 : alAlaSerArgGluArgLysPheLysLysTyrLeuMetGluArgGlnArgGlnLysSe :    660
          ||||||||!:!:!!  !|||!!.|||||||||||||||||||||||||||:!!!!.||
          alAlaSerGlnGlnSerLysLeuLysLysTyrLeuMetGluArgGlnArgLysAsnSe
 395480 : TGGCGTCCCAGCAATCCAAGTTAAAGAAGTACCTGATGGAGCGCCAGCGGAAGAACAG : 395536

    661 : rThrTyrValLysGlnGluGln :    667
          ||||||||||||||||||||||
          rThrTyrValLysGlnGluGln
 395537 : CACCTACGTCAAACAGGAGCAG : 395559

vulgar: CG17836_PA 0 667 . KB452316.1 388703 395559 + 2223 M 84 252 G 2 0 M 6 18 G 4 0 M 133 399 S 0 1 5 0 2 I 0 4645 3 0 2 S 1 2 M 35 105 G 1 0 M 72 216 5 0 2 I 0 26 3 0 2 M 3 9 G 1 0 M 39 117 G 0 6 M 6 18 G 0 3 M 9 27 G 0 6 M 34 102 G 0 3 M 39 117 5 0 2 I 0 184 3 0 2 M 17 51 G 1 0 M 102 306 G 1 0 M 77 231
forcebeams 
>Drho
ATGATCCAGGAGCCAGCACGAGTACAGGCGGCACCCACATACGAGGCGACAACCCAGGAGGACAACGCAG
CATACCCGAATCCAGCGGCTGGCGCGACAAACTCGTGGCCCAATTGCTGCTGCTCTCAGAGGAGAACGAC
GCCAACGGGGCTTATGAAGGATTCGCCTACGTCAACGATCACCACAATACCGATCGTGATAGACAGCAAG
CCAAATCCCAGAATTCTGCTGATCAGAGTGCCCacaagcaacagcagcagcagcaacaatcgcAGCGGGA
CAGAGCACAGAACGCCTCGAACTCGCATCATAAGAATACCCTTTTTACCAGGGTCGGTACCAACGCGTCC
GCCAGCCAACCAGGCCTCGCCTGCCACGAGTCCTTTAGTAAATCCGATCGGTTCGGAGAGGAGGTCCCGT
ATCTCAGTGTACCCCACGACTGGGACGCCAACATCCCGATCGCAACCAGACACGGTGGCAGGTTTACCGA
CTACGGCAATGGTGAACCAAACGGTCCCGGTTTCGTCAAGCACGCCTCAGACACAGACCCTGGCAGCCAT
GGCTGGTCTCCAGTTGACAGGTGCGGACGAGCCGCCATCCCGCCGGGATATGAGTGGAGCAATGCCTACT
GCCACAGTTGAAGAGATAGACGTTCCGGTGTTCATTGATGAATACCTGCAAGATATACAAGCTACTACAT
CGACGTGCAGCAGCGAAAACGGAAAGGAGATTTTAACTGAGACGGATATTCTTGACTTTGATATAAACAT
GTTTGCCGAGGATCTTATCGTGCTTGAGGGCAAGAATACGGATTCCTACAGTCCGGCGATGGACGAGATG
AACAACGACAACCTAGACAACGACTTCTTATTTATGGATGCCCCGGCGCTGGCTAACGAGGAGGAGTCCA
TGAACATGACATCGATATTTGCCATCAATATTACCCCGGTGCAGGACGACGACCTCATTAACGCAGTACC
CAGCTCGCAGCAGGCTATGCTCACCACCTCTCAGGACCCCTACCTGTGCACCTTCTCGCaggccacatcc
acatccagcACCACCAAGTACTTCACGGAGGAACCCCTGATTTCCGACGCGGCCAGCTACGAAACCAGTA
GTCTCTACGGGTACAGCCAGGAGACGCTCGGCTTCGGCCTTGAGGAGGACGTCAGGCCCAGCGGCAGCCT
TGCCTGCAGTGCCGCCGACGTCGCCGTGGCCACCCCAGCACCCATTTGCCATCCCATCATCTCCGCCCTG
AAACGATCTGCTCCGGCCCCCGTGGTCGAGTCGCAGACGATCAAGCGCGGCCGACTGGCGCTTAAGATCG
ACAGCCAGTCAACGCCAGTGTCCAGCCCGAGCACGCCCGAAATCATTGAGCATCTCCTGAATTTTGATAG
GCTCGAGGCCGCCAACACCAGTACCACCAACAACACCTCCATCAACAGCATTACCAGCACTCAAGCAGAA
ATTGTAGAGGATCTACGCAGCGCCGAAGAGGAGACCACCAACGACTTCTCGCCGCCGAACACGCCGCACA
GCAACTACTCAGCTAGCTGTAGCAGTGCAGTGCCCACCTGCCAGACGGGATTCGGTGGCTTCCTCACTGC
TCCCGCCTCACCCGCCTACTCGGTGGCCAGCACCTCCCAGTTCAGTCCCTCTCCATCCAACGGATCCAAT
GGCAAACGGAAGCGCGGCCGTCCTGCCAAGGATCATGCCGATGGACCCGATCCGGAAATCATGTCGCGCA
TGAACGGCGACGAGAGGAAGGCGTACCAAGACAGGATCAAAAACAACGAGGCCAGCCGAGTCTCGCGCCG
GAAAACGAAGAAgcgcgaggaggaggagaagagCGCCGAGGACGAGTTGGTAGCCGAGAATCAACATCTG
CGTGCCCTGGCGGAAGCTGTGGCGTCCCAGCAATCCAAGTTAAAGAAGTACCTGATGGAGCGCCAGCGGA
AGAACAGCACCTACGTCAAACAGGAGCAG

-- completed exonerate analysis
