Command line: [exonerate --model protein2genome --query genes/CG17836/CG17836_PA/CG17836_PA_files/CG17836_PA_prot.txt --target temp/scaffold_4770_CG17836_PA.fasta --ryo forcebeams 
>Dere
%tcs
 --bestn 1]
Hostname: [pathfinder.local]

C4 Alignment:
------------
         Query: CG17836_PA
        Target: scaffold_4770 type=golden_path_region; loc=scaffold_4770:1..17746568; ID=scaffold_4770; dbxref=GB:CH954181,REFSEQ:NW_001956552; MD5=46bb7851bfa9110a32f0c89de786e007; length=17746568; release=r1.04; species=Dere;:[revcomp]
         Model: protein2genome:local
     Raw score: 2151
   Query range: 1 -> 666
  Target range: 6885013 -> 6878603

       2 : IleGlnGluProAlaArgValGlnAlaAlaProThrPheGluAlaThrIleArgGl :      20
           ||||||||||||||||||||||||||||||||||||||||||! !|||||||||:!
           IleGlnGluProAlaArgValGlnAlaAlaProThrPheGluGluThrIleArgLy
 6885013 : ATCCAGGAGCCAGCACGAGTACAGGCGGCACCCACATTCGAGGAGACAATCCGGAA : 6884959

      21 : uAspAsnAlaLysTyrGlnGlu<->ThrAlaAlaGlyGlyThrTyrSerSerProA :      38
           !|||||||||!  ||||||!!:     !!.!.!!|||..! !!  !|||!!!  !|
           sAspAsnAlaThrTyrGlnAspPheGlnGlyThrGlySerProValSerTrpIleA
 6884958 : GGACAACGCTACTTACCAGGATTTCCAAGGCACTGGCTCCCCAGTCTCTTGGATCA : 6884902

      39 : snCysSerCysSerGlnArgArgThrArgProThrGluProThrLysAspSerPro :      56
           ||||||||||||||:!!||||||||||||||||||||||||! !|||:!!||||||
           snCysSerCysSerLysArgArgThrArgProThrGluProMetLysAsnSerPro
 6884901 : ACTGCTCCTGCTCCAAAAGGAGAACGAGGCCAACGGAGCCTATGAAGAATTCGCCG : 6884848

      57 : ThrSerThrIleThrThrIleProIleValIleAspSerLysProAsnProArgIl :      75
           |||||||||||||||||||||||||||||||||||||||||||||!.!||||||||
           ThrSerThrIleThrThrIleProIleValIleAspSerLysProThrProArgIl
 6884847 : ACGTCAACGATCACCACAATACCGATCGTGATAGACAGCAAGCCAACTCCCAGAAT : 6884791

      76 : eLeuLeuIleArgValProThrSerAsnArgGlySerAsnSerAsnAsnAsnSerA :      94
           |||||||||||||||||||||||||      ||||||..!||||||||||||    
           eLeuLeuIleArgValProThrSer------GlySerGlySerAsnAsnAsn----
 6884790 : TCTGCTAATCAGAGTGCCCACAAGC------GGAAGCGGCAGCAACAACAAT---- : 6884743

      95 : snSerThrArgSerGlyThrGluHisArgAlaProArgThrArgIleIleArgIle :     112
                   !.!!:!|||||||||! !|||.!!|||!:!||||||! !|||||||||
           --------HisAsnGlyThrGluProArgThrProGlnThrArgAsnIleArgIle
 6884742 : --------CACAACGGGACAGAGCCCAGAACGCCTCAAACTCGCAACATCAGGATT : 6884698

     113 : ProPheLeuProGlySerValProThrArgProProSerAsnGlnAlaAlaProAl :     131
           ! !|||||||||||||||||||||||||||||||||:!!|||||||||||||||:!
           LeuPheLeuProGlySerValProThrArgProProAlaAsnGlnAlaAlaProSe
 6884697 : CTCTTTTTACCAGGGTCGGTACCAACGCGTCCGCCAGCCAACCAGGCCGCGCCTTC : 6884641

     132 : aThrSerSerLeuValAsnProIleGlySerAlaArgArgCysArgIleSerThrT :     150
           !||||||||||||||||||! !! !! !|||||||||!:!|||! !||||||||||
           rThrSerSerLeuValAsnGlnLysAspSerAlaArgLysCysProIleSerThrT
 6884640 : AACGAGTTCTTTAGTAAATCAGAAAGATTCGGCGAGGAAGTGCCCTATCTCAACGT : 6884584

     151 : yrProThrThrGlyThrLeuThrSerArgSerGlnProGluThrValAlaGlyLeu :     168
           |||||||||||||||||! !||||||! !|||||||||||||||||||||||||||
           yrProThrThrGlyThrProThrSerLeuSerGlnProGluThrValAlaGlyLeu
 6884583 : ACCCCACAACTGGGACGCCAACATCCCTATCGCAACCAGAAACGGTGGCAGGTTTA : 6884530

     169 : ProThrThrAlaMetValAsnGlnThrValProValLeuSerSerAlaProGlnTh :     187
           ||||||||||||||||||||||||||||||||||||! !|||||||||||||||||
           ProThrThrAlaMetValAsnGlnThrValProValSerSerSerAlaProGlnTh
 6884529 : CCGACTACGGCAATGGTGAACCAAACGGTCCCGGTTTCGTCGAGCGCGCCTCAGAC : 6884473

     188 : rGlnThrLeuAlaAlaMetAlaGlyIleGlnLeuThrGlyAlaAspGluProProS :     206
           |||||||||||||||||||||||||:!!||||||||||||||||||||||||||||
           rGlnThrLeuAlaAlaMetAlaGlyValGlnLeuThrGlyAlaAspGluProProS
 6884472 : ACAGACCCTGGCAGCCATGGCTGGTGTCCAGCTGACAGGTGCGGACGAGCCGCCAT : 6884416

     207 : erArgArgAspValSerGlyAlaMetProThrAlaThrValGluGluIleAspVal :     224
           ||||| !!|||:!!||||||||||||||||||||||||||||||||||||||||||
           erArgTrpAspMetSerGlyAlaMetProThrAlaThrValGluGluIleAspVal
 6884415 : CCCGCTGGGATATGAGTGGAGCAATGCCTACTGCCACAGTTGAAGAGATAGACGTT : 6884362

     225 : ProValPheIleAsp{G}  >>>> Target Intron 1 >>>>  {lu}TyrLe :     232
           |||||||||||||||{|}           4310 bp           {||}|||||
           ProValPheIleAsp{G}++                         ++{lu}TyrLe
 6884361 : CCGGTGTTCATTGAT{G}gt.........................ag{AA}TACCT : 6880028

     233 : uGlnAspIleGluAlaThrSerSerThrCysAsnSerGluAsnGlyLysGluIleP :     251
           ||||!!:||||||||||||||||||||||||!:!|||:!!||||||! !||||||.
           uGlnGluIleGluAlaThrSerSerThrCysSerSerLysAsnGlyThrGluIleL
 6880027 : GCAAGAAATAGAAGCAACGTCATCAACGTGCAGCAGCAAAAACGGAACGGAGATTC : 6879971

     252 : heThrGluThrAspIleLeuAspPheAspIleAsnMetPheAla<-><-><->Glu :     266
           !!||||||! !|||! !!!!||||||||||||||||||||||||         !!:
           euThrGluMetAspAsnPheAspPheAspIleAsnMetPheAlaProLeuSerAsp
 6879970 : TTACTGAGATGGATAATTTTGACTTTGATATAAACATGTTTGCCCCGCTTTCGGAT : 6879917

     267 : GluAspLeuIleGluLeuGluGlyLysLysThrAspProPheSerProAlaMetAs :     285
               !!! !! !|||! !|||||||||||||||!!:|||!:!||||||||||||||
           ProTyrProThrGluSerGluGlyLysLysThrGluProTyrSerProAlaMetAs
 6879916 : CCTTATCCGACTGAGTCTGAGGGCAAGAAGACGGAACCCTACAGTCCGGCCATGGA : 6879860

     286 : pGlyMetAsnAsnAspAsnLeuAspAspAspPheLeuGlnLeuAsnAspLeuLeuA :     304
           |! !||||||||||||||||||:!!:!!|||.!!|||! !|||:!!||||||! !|
           pGluMetAsnAsnAspAsnLeuAsnAsnAspLeuLeuProLeuAspAspLeuProA
 6879859 : CGAGATGAACAACGACAACCTAAACAACGACCTTCTACCATTGGATGACTTGCCTA : 6879803

     305 : snGluGluGluThrPheAspMetSerSerMetSerGlyValGlyIleAspIleAsn :     322
           |||||||||||||||||||||||!!!||||||! !|||  !|||||||||||||||
           snGluGluGluThrPheAspMetThrSerMetPheGlySerGlyIleAspIleAsn
 6879802 : ACGAGGAGGAGACTTTCGACATGACATCGATGTTCGGCAGCGGCATCGACATCAAC : 6879749

     323 : GlnGluGlnGluLeuAspPheIleAsnIlePheHisAsnSerGlnProThrAlaMe :     341
           ! !||||||!!:! !!!:.!!|||!:!|||.!!! !!:!! !|||! !||||||||
           LeuGluGlnAspGlnGluLeuIleSerIleLeuProSerLeuGlnArgThrAlaMe
 6879748 : CTGGAGCAGGATCAGGAACTCATCAGCATACTCCCCAGCTTGCAGCGGACGGCCAT : 6879692

     342 : tLeuThrSerSerGluAspProTyrLeuAlaThrTyrSerGlnAlaThrSerThrS :     360
           |||||||.!!|||.!.||||||!:!|||:!!  !! ! !!|||:!!  !! !|||!
           tLeuThrGlySerAsnAspProPheLeuSerGlnCysProGlnSerLeuPheThrP
 6879691 : GCTCACCGGTTCAAACGACCCCTTTCTGTCCCAATGCCCGCAGTCCTTATTTACAT : 6879635

     361 : erSerThrThrLysPheIleThrGluGluProLeuLeuAlaAsp<->MetThrSer :     377
            !||||||||||||!:!|||:!!|||||||||:!!:!!:!!|||   :!:  !!:!
           heSerThrThrLysTyrIleSerGluGluProMetIleSerAspAsnLeuGluAsn
 6879634 : TCAGCACCACCAAATACATCTCGGAGGAGCCGATGATCTCCGACAACTTAGAAAAC : 6879581

     378 : PheAspIleSerTyrThrSerSerGlnGluLeuGlyPheGlyLeuValGluAspVa :     396
             !..!|||  !  !  !|||..!:!!..!||||||.!!! !  !! !!   !   
           SerSerIleValGlyTyrSerGlnGluSerLeuGlyLeuGluGluAspValLysPr
 6879580 : AGCAGTATCGTTGGCTACAGCCAGGAGTCGCTCGGCCTTGAGGAGGATGTCAAGCC : 6879524

     397 : lSerGlySerPheAlaSerSer<->CysSerAspValAlaAlaAlaIleProAlaP :     414
           !|||||||||||||||||||||    !!||||||||||||!.!|||||||||||| 
           oSerGlySerPheAlaSerSerGlyGlySerAspValAlaValAlaIleProAlaS
 6879523 : CAGCGGCAGCTTTGCCAGCAGTGGCGGTTCAGACGTCGCCGTGGCCATCCCAGCCT : 6879467

     415 : roIleGlnHisProIleIleThrAsnIleLysArgSerAlaProAlaProVal<-> :     432
           !!|||||||||||||||:!!||||||:!!||||||||||||||||||||||||   
           erIleGlnHisProIleLeuThrAsnLeuLysArgSerAlaProAlaProValLeu
 6879466 : CCATCCAACACCCCATCCTTACCAACCTCAAGCGATCTGCTCCGGCCCCCGTGCTC : 6879413

     433 : GluSerGlnProThrLysArgSerArgLeuMetLeuGlnIleLysThrGluLeuAl :     450
           |||:!!||| !!!.!||||||!!!|||! !  !|||:!!|||!!.||||||:!!.!
           GluAlaGlnSerAsnLysArgArgArgProGluLeuLysIleAsnThrGluValTh
 6879412 : GAGGCGCAGTCCAACAAGCGCAGACGACCTGAGCTCAAAATCAACACTGAGGTAAC : 6879356

     451 : aProAlaPheSerProThrThrProGluIleIleAsnGlnLeuLeuAsnAspAsp  :     469
           !|||||| !!!!!! !||||||||||||:!!|||.!.   |||  !..! !!    
           rProAlaValThrLeuThrThrProGluValIleGluCysLeuAspArgTyrThr+
 6879355 : GCCAGCAGTCACCCTAACCACGCCAGAAGTTATCGAGTGCCTGGATCGTTATACGg : 6879299

     470 :  >>>> Target Intron 2 >>>>  ArgPheGluAlaProThrThrSerThrS :     478
                      105 bp           !:!.!!||||||! !|||.!!|||||||
           +                         -+GlnLeuGluAlaArgThrAlaSerThrS
 6879298 : t.........................tgCAGCTCGAGGCTCGCACCGCGAGTACCA : 6879167

     479 : erThrSerThrSerAsnThrSerIleSerSerSerThrHisAlaAspIleValGlu :     496
           ||||||||!.!!!!::!!.!|||! !! !! !! !  !!.! !!||||||||||||
           erThrSerAsnThrSerAsnSerSerIleIleIlePheArgProAspIleValGlu
 6879166 : GCACCAGCAACACCTCCAACAGCAGCATCATCATCTTCCGTCCAGACATTGTAGAG : 6879113

     497 : AspLeuArgSerAlaGluGluGluThrThrThrAspPheSerAlaProAsnThrPr :     515
           |||! !!.!||||||!!:|||||||||||||||||||||||| !!|||||||||||
           AspArgHisSerAlaAspGluGluThrThrThrAspPheSerProProAsnThrPr
 6879112 : GATCGACACAGTGCCGATGAGGAGACTACCACCGACTTCTCGCCTCCAAACACACC : 6879056

     516 : oHisSerAsnTyrSerAlaSerSerSerCysAlaAlaProThrCysGlnThrGlyT :     534
           |||||||||||||||||||||||||||||||||||||||||||||||||||||||!
           oHisSerAsnTyrSerAlaSerSerSerCysAlaAlaProThrCysGlnThrGlyP
 6879055 : ACACAGTAACTACTCAGCCAGCTCCAGCTGTGCGGCGCCCACCTGCCAGACCGGAT : 6878999

     535 : yrGlyGlyPheLeuThrAlaProThrSerProAlaTyrSerThrAlaSerThrSer :     552
           :!|||||||||||||||||||||  !|||||||||||||||..!||||||||||||
           heGlyGlyPheLeuThrAlaProGlnSerProAlaTyrSerValAlaSerThrSer
 6878998 : TTGGTGGATTCCTCACTGCTCCCCAATCGCCCGCCTATTCAGTAGCCAGTACCTCC : 6878945

     553 : ValPheSerProSerProAlaSerGlyIleSerGlyLysArgLysArgGlyArgPr :     571
           !.!|||||||||||||||:!!.!!||||||||||||||||||||||||||||||||
           AlaPheSerProSerProSerGlyGlyIleSerGlyLysArgLysArgGlyArgPr
 6878944 : GCGTTCAGCCCTTCGCCATCCGGTGGAATCAGCGGCAAGCGGAAGCGCGGTCGCCC : 6878888

     572 : oAlaLysAspHisAlaAspGlyProAspProValLeuMetSerSerMetLysSerG :     590
           |||||||||||||||||||||||||||||||! !|||||||||.!!|||!  ||| 
           oAlaLysAspHisAlaAspGlyProAspProGluLeuMetSerGlyMetThrSer-
 6878887 : TGCCAAGGACCATGCCGACGGTCCCGATCCCGAGCTCATGTCCGGCATGACCAGC- : 6878831

     591 : luGluGluArgLysAlaTyrGlnAspArgLeuLysAsnAsnGluAlaSerArgVal :     608
             ||||||!:!||||||||||||||||||:!!||||||||||||||||||||||||
           --GluGluLysLysAlaTyrGlnAspArgIleLysAsnAsnGluAlaSerArgVal
 6878830 : --GAGGAGAAGAAGGCCTACCAGGACAGAATCAAGAACAATGAGGCAAGCCGCGTA : 6878780

     609 : SerArgArgLysThrLysValArgGluGluGluGluLysArgAlaGluAspThrLe :     627
           ||||||||||||||||||  !||||||||||||||||||!!!!.!||||||  !||
           SerArgArgLysThrLysLysArgGluGluGluGluLysSerValGluAspGluLe
 6878779 : TCGCGCCGGAAGACGAAGAAGCGTGAGGAGGAGGAGAAGAGCGTCGAGGACGAGCT : 6878723

     628 : uLeuAlaGluAsnLeuArgLeuArgAlaArgAlaAspGluValAlaSerArgGluA :     646
           |:!!|||||||||||||||||||||.!!! !||||||||||||||||||!:!||||
           uValAlaGluAsnLeuArgLeuArgThrLeuAlaAspGluValAlaSerLysGluA
 6878722 : GGTGGCAGAGAATCTGCGACTGCGTACGCTGGCCGATGAAGTGGCATCCAAGGAAC : 6878666

     647 : rgLysPheLysLysTyrLeuMetGluArgGlnArgGlnLysSerThrTyrValLys :     664
           |||||||||||!!.!:!||||||  !|||..!|||:!! ! ||||||!:!!.!:!!
           rgLysPheLysAsnPheLeuMetLeuArgMetArgLysAspSerThrPheAlaGlu
 6878665 : GCAAGTTCAAGAATTTCCTGATGCTGCGCATGCGGAAGGATAGCACTTTCGCTGAG : 6878612

     665 : GlnGlu :     666
           ||||||
           GlnGlu
 6878611 : CAGGAG : 6878604

vulgar: CG17836_PA 1 666 . scaffold_4770 6885013 6878603 - 2151 M 26 78 G 0 3 M 56 168 G 2 0 M 7 21 G 4 0 M 133 399 S 0 1 5 0 2 I 0 4306 3 0 2 S 1 2 M 35 105 G 0 9 M 109 327 G 0 3 M 29 87 G 0 3 M 28 84 G 0 3 M 37 111 5 0 2 I 0 101 3 0 2 M 121 363 G 1 0 M 76 228
forcebeams 
>Dere
ATCCAGGAGCCAGCACGAGTACAGGCGGCACCCACATTCGAGGAGACAATCCGGAAGGACAACGCTACTT
ACCAGGATTTCCAAGGCACTGGCTCCCCAGTCTCTTGGATCAACTGCTCCTGCTCCAAAAGGAGAACGAG
GCCAACGGAGCCTATGAAGAATTCGCCGACGTCAACGATCACCACAATACCGATCGTGATAGACAGCAAG
CCAACTCCCAGAATTCTGCTAATCAGAGTGCCCACAAGCGGAAGCGGCAGCAACAACAATCACAACGGGA
CAGAGCCCAGAACGCCTCAAACTCGCAACATCAGGATTCTCTTTTTACCAGGGTCGGTACCAACGCGTCC
GCCAGCCAACCAGGCCGCGCCTTCAACGAGTTCTTTAGTAAATCAGAAAGATTCGGCGAGGAAGTGCCCT
ATCTCAACGTACCCCACAACTGGGACGCCAACATCCCTATCGCAACCAGAAACGGTGGCAGGTTTACCGA
CTACGGCAATGGTGAACCAAACGGTCCCGGTTTCGTCGAGCGCGCCTCAGACACAGACCCTGGCAGCCAT
GGCTGGTGTCCAGCTGACAGGTGCGGACGAGCCGCCATCCCGCTGGGATATGAGTGGAGCAATGCCTACT
GCCACAGTTGAAGAGATAGACGTTCCGGTGTTCATTGATGAATACCTGCAAGAAATAGAAGCAACGTCAT
CAACGTGCAGCAGCAAAAACGGAACGGAGATTCTTACTGAGATGGATAATTTTGACTTTGATATAAACAT
GTTTGCCCCGCTTTCGGATCCTTATCCGACTGAGTCTGAGGGCAAGAAGACGGAACCCTACAGTCCGGCC
ATGGACGAGATGAACAACGACAACCTAAACAACGACCTTCTACCATTGGATGACTTGCCTAACGAGGAGG
AGACTTTCGACATGACATCGATGTTCGGCAGCGGCATCGACATCAACCTGGAGCAGGATCAGGAACTCAT
CAGCATACTCCCCAGCTTGCAGCGGACGGCCATGCTCACCGGTTCAAACGACCCCTTTCTGTCCCAATGC
CCGCAGTCCTTATTTACATTCAGCACCACCAAATACATCTCGGAGGAGCCGATGATCTCCGACAACTTAG
AAAACAGCAGTATCGTTGGCTACAGCCAGGAGTCGCTCGGCCTTGAGGAGGATGTCAAGCCCAGCGGCAG
CTTTGCCAGCAGTGGCGGTTCAGACGTCGCCGTGGCCATCCCAGCCTCCATCCAACACCCCATCCTTACC
AACCTCAAGCGATCTGCTCCGGCCCCCGTGCTCGAGGCGCAGTCCAACAAGCGCAGACGACCTGAGCTCA
AAATCAACACTGAGGTAACGCCAGCAGTCACCCTAACCACGCCAGAAGTTATCGAGTGCCTGGATCGTTA
TACGCAGCTCGAGGCTCGCACCGCGAGTACCAGCACCAGCAACACCTCCAACAGCAGCATCATCATCTTC
CGTCCAGACATTGTAGAGGATCGACACAGTGCCGATGAGGAGACTACCACCGACTTCTCGCCTCCAAACA
CACCACACAGTAACTACTCAGCCAGCTCCAGCTGTGCGGCGCCCACCTGCCAGACCGGATTTGGTGGATT
CCTCACTGCTCCCCAATCGCCCGCCTATTCAGTAGCCAGTACCTCCGCGTTCAGCCCTTCGCCATCCGGT
GGAATCAGCGGCAAGCGGAAGCGCGGTCGCCCTGCCAAGGACCATGCCGACGGTCCCGATCCCGAGCTCA
TGTCCGGCATGACCAGCGAGGAGAAGAAGGCCTACCAGGACAGAATCAAGAACAATGAGGCAAGCCGCGT
ATCGCGCCGGAAGACGAAGAAGCGTGAGGAGGAGGAGAAGAGCGTCGAGGACGAGCTGGTGGCAGAGAAT
CTGCGACTGCGTACGCTGGCCGATGAAGTGGCATCCAAGGAACGCAAGTTCAAGAATTTCCTGATGCTGC
GCATGCGGAAGGATAGCACTTTCGCTGAGCAGGAG

-- completed exonerate analysis
