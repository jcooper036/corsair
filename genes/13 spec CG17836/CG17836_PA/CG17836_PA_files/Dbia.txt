Command line: [exonerate --model protein2genome --query genes/CG17836/CG17836_PA/CG17836_PA_files/CG17836_PA_prot.txt --target temp/KB462814.1_CG17836_PA.fasta --ryo forcebeams 
>Dbia
%tcs
 --bestn 1]
Hostname: [pathfinder.local]

C4 Alignment:
------------
         Query: CG17836_PA
        Target: KB462814.1 Drosophila biarmipes unplaced genomic scaffold scf7180000302402, whole genome shotgun sequence
         Model: protein2genome:local
     Raw score: 2232
   Query range: 0 -> 667
  Target range: 490249 -> 498329

      1 : MetIleGlnGluProAlaArgValGlnAlaAlaProThrPheGluAlaThrIleArgG :     20
          ||||||||||||||||||||||||||||||||||||! !!:!|||||||||! !||||
          MetIleGlnGluProAlaArgValGlnAlaAlaProArgTyrGluAlaThrThrArgG
 490250 : ATGATCCAGGAGCCAGCACGAGTACAGGCGGCACCCAGATACGAGGCGACAACCCGGG : 490307

     21 : luAspAsnAlaLysTyrGlnGluThrAlaAlaGlyGlyThrTyrSerSerProAsnCy :     39
          |||||||||||! !! !|||!!: !!|||||||||!.!||| !!|||!!!||||||||
          luAspAsnAlaThrSerGlnAspProAlaAlaGlyAlaThrAsnSerTrpProAsnCy
 490308 : AGGACAACGCAACGTCCCAGGATCCAGCGGCTGGCGCGACAAACTCGTGGCCCAACTG : 490364

     40 : sSerCysSerGlnArgArgThrArgProThrGluProThrLysAspSerProThrSer :     58
          |||||||||||||||||||||||||||||||||||||! !!:!|||||||||||||||
          sSerCysSerGlnArgArgThrArgProThrGluProMetArgAspSerProThrSer
 490365 : CTCCTGCTCTCAGAGGAGAACGAGGCCAACGGAGCCTATGAGGGATTCGCCTACGTCG : 490421

     59 : ThrIleThrThrIleProIleValIleAspSerLysProAsnProArgIleLeuLeuI :     78
          ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
          ThrIleThrThrIleProIleValIleAspSerLysProAsnProArgIleLeuLeuI
 490422 : ACGATCACCACCATACCGATCGTGATAGACAGCAAGCCAAATCCCAGAATTCTGCTGA : 490481

     79 : leArgValProThrSerAsnArgGlySerAsnSerAsnAsnAsnSerAsnSerThrAr :     97
          |||||||||||||||||         |||:::|||:::|||:::||||||:::...||
          leArgValProThrSer---------SerSerSerSerAsnSerSerAsnAsnAsnAr
 490482 : TCAGAGTGCccacaagc---------agcagcagcagcaacagcagcaacaacaatcg : 490529

     98 : gSerGlyThrGluHisArgAlaProArgThrArgIleIleArgIleProPheLeuPro :    116
          |||||||||||||||||||.!!||||||||||||||||||||||||||||||||||||
          gSerGlyThrGluHisArgThrProArgThrArgIleIleArgIleProPheLeuPro
 490530 : cAGCGGGACAGAGCACAGAACGCCTCGAACTCGCATCATAAGAATACCCTTTTTACCA : 490586

    117 : GlySerValProThrArgProProSerAsnGlnAlaAlaProAlaThrSerSerLeuV :    136
          ||||||||||||||||||||||||:!!|||||||||:!!|||||||||||| !!||||
          GlySerValProThrArgProProAlaAsnGlnAlaSerProAlaThrSerProLeuV
 490587 : GGGTCGGTACCAACGCGTCCGCCAGCCAACCAGGCCTCGCCTGCCACCAGTCCCTTAG : 490646

    137 : alAsnProIleGlySerAlaArgArgCysArgIleSerThrTyrProThrThr<->Gl :    154
          ||||||||! !! !||||||||||||||||||||||||.!!! !|||||||||   ||
          alAsnProThrAspSerAlaArgArgCysArgIleSerAlaCysProThrThrProGl
 490647 : TAAATCCGACAGATTCGGCGAGGAGGTGCCGTATCTCTGCGTGCCCCACGACGCCTGG : 490703

    155 : yThrLeuThrSerArgSerGlnProGluThrValAlaGlyLeuProThrThrAlaMet :    173
          ||||! !! !||||||||||||||||||||||||||||||! !! !||||||||||||
          yThrGlnIleSerArgSerGlnProGluThrValAlaGlySerHisThrThrAlaMet
 490704 : GACACAAATATCCCGATCGCAGCCAGAAACGGTGGCAGGTTCACACACTACGGCAATG : 490760

    174 : Val<->AsnGlnThrValProValLeuSerSerAlaProGlnThrGlnThrLeuAlaA :    192
          |||   ||||||||||||||||||! !|||! !|||||||||||||||||||||||||
          ValAsnAsnGlnThrValProValSerSerIleAlaProGlnThrGlnThrLeuAlaA
 490761 : GTGAACAACCAAACGGTCCCGGTTTCGTCGATCGCGCCTCAGACACAGACCCTGGCAG : 490820

    193 : laMetAlaGlyIleGlnLeuThrGlyAlaAspGluProProSerArgArgAspValSe :    211
          |||||||||||||||||||||||||||||||||||||||||||||||||||||:!!||
          laMetAlaGlyIleGlnLeuThrGlyAlaAspGluProProSerArgArgAspMetSe
 490821 : CCATGGCTGGCATCCAGTTGACAGGTGCGGACGAGCCGCCATCGCGCCGGGATATGAG : 490877

    212 : rGlyAlaMetProThrAlaThrValGluGluIleAspValProValPheIleAsp{G} :    230
          |||||||||||||||||||||||||||||||||||||||||||||||||||||||{|}
          rGlyAlaMetProThrAlaThrValGluGluIleAspValProValPheIleAsp{G}
 490878 : TGGAGCAATGCCTACTGCCACAGTTGAAGAGATAGACGTTCCGGTGTTCATTGAT{G} : 490934

    231 :   >>>> Target Intron 1 >>>>  {lu}TyrLeuGlnAspIleGluAlaThrS :    239
                     5984 bp           {||}|||||||||||||||||||||||||
          ++                         ++{lu}TyrLeuGlnAspIleGluAlaThrS
 490935 : gt.........................ag{AA}TACCTGCAAGATATAGAAGCAACGT : 496945

    240 : erSerThrCysAsnSerGluAsnGlyLysGluIlePheThrGluThrAspIleLeuAs :    258
          |||||||||||!:!||||||||||||||||||||||||||||||||||||||||||||
          erSerThrCysSerSerGluAsnGlyLysGluIlePheThrGluThrAspIleLeuAs
 496946 : CCTCGACGTGCAGCAGCGAAAACGGAAAGGAGATTTTTACTGAGACGGATATTCTTGA : 497002

    259 : pPheAspIleAsnMetPheAlaGluGluAspLeuIleGluLeuGluGlyLysLysThr :    277
          ||||||||||||||||||||||   ||||||  !||||||! !|||||||||! !! !
          pPheAspIleAsnMetPheAla---GluAspAsnIleGluProGluGlyLysThrMet
 497003 : CTTTGATATAAACATGTTTGCC---GAGGATAACATTGAGCCCGAGGGCAAGACGATG : 497056

    278 : AspProPheSerProAlaMetAspGlyMetAsnAsnAspAsnLeuAspAspAspPheL :    297
          !!:! !||||||||||||||||||! !|||||||||||||||||||||:!!||||||!
          GluLeuPheSerProAlaMetAspGluMetAsnAsnAspAsnLeuAspAsnAspPheP
 497057 : GAACTCTTCAGTCCGGCGATGGACGAGATGAACAACGACAACCTAGACAACGACTTCT : 497116

    298 : euGlnLeuAsnAspLeuLeuAsn<->GluGluGluThrPheAspMetSerSerMetSe :    315
          !!|||:!!:!!! !||||||...   ||||||||||||!!.||||||!!!..!|||  
          heGlnMetAspGlyLeuLeuGlyAlaGluGluGluThrLeuAspMetArgAspMet--
 497117 : TTCAAATGGATGGCCTGCTTGGGGCCGAGGAGGAGACCTTGGACATGAGAGACATG-- : 497173

    316 : rGlyValGlyIleAspIleAsnGlnGluGlnGluLeuAspPheIleAsnIlePheHis :    334
              :!!|||||||||||||||:!!  !!:!|||! !|||.!!.!!!:!|||.!!  !
          ----LeuGlyIleAspIleAsnGluProArgGluProAspLeuPheSerIleIleSer
 497174 : ----CTTGGCATCGACATCAACGAGCCGCGGGAACCGGACCTCTTCAGCATTATCTCC : 497224

    335 : AsnSerGlnProThrAlaMetLeuThrSerSerGluAspProTyrLeuAlaThrTyrS :    354
          !:!|||            |||||||||.!!|||:!!||||||||||||  !:!!  !|
          SerSer------------MetLeuThrGlySerGlnAspProTyrLeuPheSerSerS
 497225 : AGTTCG------------ATGCTCACCGGCTCGCAGGACCCCTACCTGTTTTCCAGCT : 497272

    355 : erGlnAlaThrSerThrSerSerThrThrLysPheIleThrGluGluProLeuLeuAl :    373
          |||||.!! !!|||||||||||||||||||||!:!|||||||||! !|||! !:!! !
          erGlnThrProSerThrSerSerThrThrLysTyrIleThrGluValProTyrIlePr
 497273 : CACAGACCCCATCCACATCCAGCACCACCAAATACATCACCGAGGTGCCTTACATCCC : 497329

    374 : aAspMetThrSerPheAspIleSer<-><->TyrThrSerSerGlnGlu<->LeuGly :    389
          !|||:!!.!!! !  !|||! !!:!      |||  !!!!|||||||||   ||||||
          oAspLeuAlaIleAspAspThrAsnSerValTyrGlyCysSerGlnGluSerLeuGly
 497330 : CGACTTGGCCATCGACGACACCAACAGTGTCTATGGCTGCAGCCAGGAGTCGCTCGGC : 497386

    390 : PheGlyLeuValGluAspVal<-><->SerGlySerPheAlaSerSerCysSerAspV :    407
          |||||||||||||||||||||      ||||||||||||||||||||| !!.!!||||
          PheGlyLeuValGluAspValLysProSerGlySerPheAlaSerSerGlyGlyAspV
 497387 : TTCGGCCTTGTGGAGGACGTCAAGCCCAGCGGCAGCTTTGCCAGCAGTGGCGGCGACG : 497446

    408 : alAlaAlaAlaIleProAlaProIleGlnHisProIleIleThrAsnIleLysArgSe :    426
          |||||!.!|||! !|||.!!||||||||||||||||||||||||!..:!!||||||||
          alAlaValAlaThrProThrProIleGlnHisProIleIleThrThrLeuLysArgSe
 497447 : TCGCCGTGGCCACCCCAACCCCCATCCAGCACCCCATCATCACCACGCTCAAGCGATC : 497503

    427 : rAlaProAlaProVal<->GluSerGlnProThrLysArgSerArgLeuMetLeuGln :    444
          |||| !!|||||||||   ||||||||||||  !|||||||||||||||:!:||||||
          rAlaAlaAlaProValAlaGluSerGlnProLeuLysArgSerArgLeuValLeuGln
 497504 : GGCCGCAGCCCCCGTGGCCGAGTCACAGCCCCTCAAGCGCAGCCGCCTGGTACTCCAG : 497560

    445 : IleLysThrGluLeuAlaProAlaPheSerProThrThrProGluIleIleAsnGlnL :    464
          :!!!!.|||  !! !|||! !||||||||||||!:!|||||||||||||||.!..!.|
          ValAsnThrProProAlaGlnAlaPheSerProSerThrProGluIleIleGluAspL
 497561 : GTCAACACCCCGCCGGCGCAAGCGTTCAGCCCGAGCACGCCGGAAATCATCGAAGATC : 497620

    465 : euLeuAsnAspAspArg  >>>> Target Intron 2 >>>>  PheGluAlaPro :    473
          |||||!.!  !!!:|||            86 bp              !!!:||||||
          euLeuThrPheGluArg++                         ++HisAspAlaPro
 497621 : TTTTGACTTTCGAAAGGgt.........................agCACGATGCCCCC : 497733

    474 : ThrThrSerThrSerThrSerThrSerAsnThrSerIleSerSerSer<-><-><->T :    490
          !.!||||||.!!|||.!!|||.!!!!!:!!|||!:!:!:|||||||||         !
          AsnThrSerAlaSerAlaSerAlaThrAspThrAsnValSerSerSerIleValPheI
 497734 : AACACCAGTGCTAGTGCCAGCGCCACCGACACCAATGTGTCCTCCAGCATCGTCTTCA : 497793

    491 : hrHisAlaAspIleValGluAspLeuArgSerAlaGluGluGluThrThrThrAspPh :    509
           !!!.! !   |||||||||||||||||||||!.!|||||||||||||||||||||||
          leGlnGluArgIleValGluAspLeuArgSerValGluGluGluThrThrThrAspPh
 497794 : TCCAAGAGAGAATCGTGGAGGATCTACGCAGTGTCGAAGAGGAGACCACCACCGACTT : 497850

    510 : eSerAlaProAsnThrProHisSerAsnTyrSerAlaSerSerSerCysAlaAlaPro :    528
          |||| !!|||||||||||||||||||||||||||||||||||||||||||||||||||
          eSerProProAsnThrProHisSerAsnTyrSerAlaSerSerSerCysAlaAlaPro
 497851 : CTCGCCGCCGAATACCCCGCACAGCAACTACTCAGCCAGCTCCAGCTGTGCGGCGCCC : 497907

    529 : ThrCysGlnThrGlyTyrGlyGlyPheLeuThrAlaProThrSerProAlaTyrSerT :    548
          |||||||||||||||!:!|||||||||||||||||||||.!!|||||||||||||||.
          ThrCysGlnThrGlyPheGlyGlyPheLeuThrAlaProAlaSerProAlaTyrSerV
 497908 : ACCTGCCAGACCGGCTTCGGTGGCTTCCTCACAGCTCCCGCCTCGCCCGCCTACTCTG : 497967

    549 : hrAlaSerThrSerValPheSerProSerProAla<->SerGlyIleSerGlyLysAr :    566
          .!||||||||||||  !||||||||||||||||||   :!!!.!! !!:!||||||||
          alAlaSerThrSerGlnPheSerProSerProAlaGlyAlaAlaThrAsnGlyLysAr
 497968 : TGGCCAGCACCTCCCAGTTCAGCCCATCGCCAGCCGGCGCAGCCACCAACGGCAAGCG : 498024

    567 : gLysArgGlyArgProAlaLysAspHisAlaAspGlyProAspProValLeuMetSer :    585
          ||||||||||||||||||||||||||||||||||||||||||||||  !|||:!!|||
          gLysArgGlyArgProAlaLysAspHisAlaAspGlyProAspProAsnLeuLeuSer
 498025 : AAAGCGCGGACGTCCTGCCAAGGATCATGCCGACGGACCCGATCCCAATCTCCTGTCG : 498081

    586 : SerMetLysSerGluGluGluArgLysAlaTyrGlnAspArgLeuLysAsnAsnGluA :    605
           !!|||!!..!!   ||||||||||||||||||:!!||||||||||||||||||||||
          ArgMetAsnGly---GluGluArgLysAlaTyrGluAspArgLeuLysAsnAsnGluA
 498082 : CGCATGAACGGC---GAGGAGCGAAAGGCCTACGAGGACAGGCTGAAGAACAACGAGG : 498138

    606 : laSerArgValSerArgArgLysThrLysValArgGluGluGluGluLysArgAlaGl :    624
          |||||||||||||||||||||||||||||   ||||||||||||||||||...|||||
          laSerArgValSerArgArgLysThrLysLysArgGluGluGluGluLysAsnAlaGl
 498139 : CCAGCCGCGTATCGCGCCGGAAGACGAAGaagcgggaggaggaggagaagaacGCCGA : 498195

    625 : uAspThrLeuLeuAlaGluAsnLeuArgLeuArgAlaArgAlaAspGluValAlaSer :    643
          ||||  !|||:!!||||||||||||||||||||||||! !||||||!!:||||||!!!
          uAspGluLeuValAlaGluAsnLeuArgLeuArgAlaLeuAlaAspAspValAlaThr
 498196 : GGACGAGCTGGTGGCCGAGAATCTGCGCCTACGCGCCCTGGCCGACGACGTGGCGACC : 498252

    644 : ArgGluArgLysPheLysLysTyrLeuMetGluArgGlnArgGlnLysSerThrTyr< :    663
          !:!|||||||||!:!|||||||||||||||!!:|||||||||:!!!!.||||||||| 
          GlnGluArgLysTyrLysLysTyrLeuMetAspArgGlnArgLysAsnSerThrTyrA
 498253 : CAGGAGCGCAAGTACAAGAAGTACCTGATGGATCGCCAGAGGAAGAACAGCACCTACA : 498312

    664 : ->ValLysGlnGluGln :    667
            |||||||||||||||
          snValLysGlnGluGln
 498313 : ACGTCAAGCAGGAGCAG : 498329

vulgar: CG17836_PA 0 667 . KB462814.1 490249 498329 + 2232 M 83 249 G 3 0 M 67 201 G 0 3 M 21 63 G 0 3 M 55 165 S 0 1 5 0 2 I 0 5980 3 0 2 S 1 2 M 35 105 G 1 0 M 38 114 G 0 3 M 10 30 G 2 0 M 20 60 G 4 0 M 41 123 G 0 6 M 6 18 G 0 3 M 9 27 G 0 6 M 35 105 G 0 3 M 38 114 5 0 2 I 0 82 3 0 2 M 20 60 G 0 9 M 70 210 G 0 3 M 30 90 G 1 0 M 72 216 G 0 3 M 5 15
forcebeams 
>Dbia
ATGATCCAGGAGCCAGCACGAGTACAGGCGGCACCCAGATACGAGGCGACAACCCGGGAGGACAACGCAA
CGTCCCAGGATCCAGCGGCTGGCGCGACAAACTCGTGGCCCAACTGCTCCTGCTCTCAGAGGAGAACGAG
GCCAACGGAGCCTATGAGGGATTCGCCTACGTCGACGATCACCACCATACCGATCGTGATAGACAGCAAG
CCAAATCCCAGAATTCTGCTGATCAGAGTGCccacaagcagcagcagcagcaacagcagcaacaacaatc
gcAGCGGGACAGAGCACAGAACGCCTCGAACTCGCATCATAAGAATACCCTTTTTACCAGGGTCGGTACC
AACGCGTCCGCCAGCCAACCAGGCCTCGCCTGCCACCAGTCCCTTAGTAAATCCGACAGATTCGGCGAGG
AGGTGCCGTATCTCTGCGTGCCCCACGACGCCTGGGACACAAATATCCCGATCGCAGCCAGAAACGGTGG
CAGGTTCACACACTACGGCAATGGTGAACAACCAAACGGTCCCGGTTTCGTCGATCGCGCCTCAGACACA
GACCCTGGCAGCCATGGCTGGCATCCAGTTGACAGGTGCGGACGAGCCGCCATCGCGCCGGGATATGAGT
GGAGCAATGCCTACTGCCACAGTTGAAGAGATAGACGTTCCGGTGTTCATTGATGAATACCTGCAAGATA
TAGAAGCAACGTCCTCGACGTGCAGCAGCGAAAACGGAAAGGAGATTTTTACTGAGACGGATATTCTTGA
CTTTGATATAAACATGTTTGCCGAGGATAACATTGAGCCCGAGGGCAAGACGATGGAACTCTTCAGTCCG
GCGATGGACGAGATGAACAACGACAACCTAGACAACGACTTCTTTCAAATGGATGGCCTGCTTGGGGCCG
AGGAGGAGACCTTGGACATGAGAGACATGCTTGGCATCGACATCAACGAGCCGCGGGAACCGGACCTCTT
CAGCATTATCTCCAGTTCGATGCTCACCGGCTCGCAGGACCCCTACCTGTTTTCCAGCTCACAGACCCCA
TCCACATCCAGCACCACCAAATACATCACCGAGGTGCCTTACATCCCCGACTTGGCCATCGACGACACCA
ACAGTGTCTATGGCTGCAGCCAGGAGTCGCTCGGCTTCGGCCTTGTGGAGGACGTCAAGCCCAGCGGCAG
CTTTGCCAGCAGTGGCGGCGACGTCGCCGTGGCCACCCCAACCCCCATCCAGCACCCCATCATCACCACG
CTCAAGCGATCGGCCGCAGCCCCCGTGGCCGAGTCACAGCCCCTCAAGCGCAGCCGCCTGGTACTCCAGG
TCAACACCCCGCCGGCGCAAGCGTTCAGCCCGAGCACGCCGGAAATCATCGAAGATCTTTTGACTTTCGA
AAGGCACGATGCCCCCAACACCAGTGCTAGTGCCAGCGCCACCGACACCAATGTGTCCTCCAGCATCGTC
TTCATCCAAGAGAGAATCGTGGAGGATCTACGCAGTGTCGAAGAGGAGACCACCACCGACTTCTCGCCGC
CGAATACCCCGCACAGCAACTACTCAGCCAGCTCCAGCTGTGCGGCGCCCACCTGCCAGACCGGCTTCGG
TGGCTTCCTCACAGCTCCCGCCTCGCCCGCCTACTCTGTGGCCAGCACCTCCCAGTTCAGCCCATCGCCA
GCCGGCGCAGCCACCAACGGCAAGCGAAAGCGCGGACGTCCTGCCAAGGATCATGCCGACGGACCCGATC
CCAATCTCCTGTCGCGCATGAACGGCGAGGAGCGAAAGGCCTACGAGGACAGGCTGAAGAACAACGAGGC
CAGCCGCGTATCGCGCCGGAAGACGAAGaagcgggaggaggaggagaagaacGCCGAGGACGAGCTGGTG
GCCGAGAATCTGCGCCTACGCGCCCTGGCCGACGACGTGGCGACCCAGGAGCGCAAGTACAAGAAGTACC
TGATGGATCGCCAGAGGAAGAACAGCACCTACAACGTCAAGCAGGAGCAG

-- completed exonerate analysis
