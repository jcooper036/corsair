Command line: [exonerate --model protein2genome --query genes/CG17836/CG17836_PA/CG17836_PA_files/CG17836_PA_prot.txt --target temp/CM002913.1_CG17836_PA.fasta --ryo forcebeams 
>Dsim
%tcs
 --bestn 1]
Hostname: [pathfinder.local]

C4 Alignment:
------------
         Query: CG17836_PA
        Target: CM002913.1 Drosophila simulans strain w501 chromosome 3R, whole genome shotgun sequence:[revcomp]
         Model: protein2genome:local
     Raw score: 3185
   Query range: 0 -> 668
  Target range: 6510743 -> 6504679

       1 : MetIleGlnGluProAlaArgValGlnAlaAlaProThrPheGluAlaThrIleAr :      19
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           MetIleGlnGluProAlaArgValGlnAlaAlaProThrPheGluAlaThrIleAr
 6510743 : ATGATCCAGGAGCCAGCACGAGTACAGGCGGCACCCACATTCGAGGCGACAATCCG : 6510689

      20 : gGluAspAsnAlaLysTyrGlnGluThrAlaAlaGlyGlyThrTyrSerSerProA :      38
           |||||||||||||! !|||||||||||||||||||||||||||||||||!!!||||
           gGluAspAsnAlaThrTyrGlnGluThrAlaAlaGlyGlyThrTyrSerTrpProA
 6510688 : CGAGGACAACGCAACATACCAGGAAACAGCGGCTGGCGGGACATACTCGTGGCCCA : 6510632

      39 : snCysSerCysSerGlnArgArgThrArgProThrGluProThrLysAspSerPro :      56
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           snCysSerCysSerGlnArgArgThrArgProThrGluProThrLysAspSerPro
 6510631 : ACTGCTCCTGCTCTCAGAGGAGAACGAGGCCAACGGAGCCTACGAAGGATTCGCCT : 6510578

      57 : ThrSerThrIleThrThrIleProIleValIleAspSerLysProAsnProArgIl :      75
           |||||||||:!!||||||||||||||||||||||||||||||||||||||||||||
           ThrSerThrValThrThrIleProIleValIleAspSerLysProAsnProArgIl
 6510577 : ACGTCAACGGTCACCACAATACCGATCGTGATAGACAGCAAGCCAAATCCCAGAAT : 6510521

      76 : eLeuLeuIleArgValProThrSerAsnArgGlySerAsnSerAsnAsnAsnSerA :      94
           |||||||||||||||||||||||||||||||...|||||||||::::::|||   |
           eLeuLeuIleArgValProThrSerAsnArgSerSerAsnSerSerSerAsn---A
 6510520 : TCTGCTGATCAGAGTGCCCAcaagcaacagaagcagcaacagcagcagcaac---a : 6510467

      95 : snSerThrArgSerGlyThrGluHisArgAlaProArgThrArgIleIleArgIle :     112
           |||||||||||!!!||||||||||||||||||||||||||||||||||||||||||
           snSerThrArgCysGlyThrGluHisArgAlaProArgThrArgIleIleArgIle
 6510466 : acagcactcGCTGCGGGACAGAGCACAGAGCGCCTCGAACTCGCATCATCAGAATA : 6510413

     113 : ProPheLeuProGlySerValProThrArgProProSerAsnGlnAlaAlaProAl :     131
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           ProPheLeuProGlySerValProThrArgProProSerAsnGlnAlaAlaProAl
 6510412 : CCCTTTTTACCAGGGTCGGTACCAACGCGTCCGCCATCCAACCAGGCCGCGCCTGC : 6510356

     132 : aThrSerSerLeuValAsnProIleGlySerAlaArgArgCysArgIleSerThrT :     150
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           aThrSerSerLeuValAsnProIleGlySerAlaArgArgCysArgIleSerThrT
 6510355 : AACGAGTTCTTTAGTAAATCCGATAGGTTCGGCGAGGAGGTGCCGTATCTCAACGT : 6510299

     151 : yrProThrThrGlyThrLeuThrSerArgSerGlnProGluThrValAlaGlyLeu :     168
           |||||||||||||||||! !||||||||||||||||||||||||||||||||||||
           yrProThrThrGlyThrProThrSerArgSerGlnProGluThrValAlaGlyLeu
 6510298 : ACCCCACGACTGGGACACCAACATCCCGATCGCAACCAGAAACGGTGGCAGGTTTA : 6510245

     169 : ProThrThrAlaMetValAsnGlnThrValProValLeuSerSerAlaProGlnTh :     187
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           ProThrThrAlaMetValAsnGlnThrValProValLeuSerSerAlaProGlnTh
 6510244 : CCGACTACGGCAATGGTGAACCAAACGGTCCCGGTTTTATCGAGCGCGCCTCAGAC : 6510188

     188 : rGlnThrLeuAlaAlaMetAlaGlyIleGlnLeuThrGlyAlaAspGluProProS :     206
           ||||||||||||||||||||||||||||||||||.!!||||||||||||||||||!
           rGlnThrLeuAlaAlaMetAlaGlyIleGlnLeuAlaGlyAlaAspGluProProP
 6510187 : ACAGACCCTGGCAGCCATGGCTGGTATCCAGCTGGCAGGTGCGGACGAGCCGCCAT : 6510131

     207 : erArgArgAspValSerGlyAlaMetProThrAlaThrValGluGluIleAspVal :     224
            !! !|||||||||||||||||||||||||||||||||||||||||||||||||||
           heProArgAspValSerGlyAlaMetProThrAlaThrValGluGluIleAspVal
 6510130 : TCCCCCGGGATGTGAGTGGAGCAATGCCTACTGCCACAGTTGAAGAGATAGACGTT : 6510077

     225 : ProValPheIleAsp{G}  >>>> Target Intron 1 >>>>  {lu}TyrLe :     232
           |||||||||||||||{|}           3952 bp           {||}|||||
           ProValPheIleAsp{G}++                         ++{lu}TyrLe
 6510076 : CCGGTGTTCATTGAT{G}gt.........................ag{AA}TACCT : 6506101

     233 : uGlnAspIleGluAlaThrSerSerThrCysAsnSerGluAsnGlyLysGluIleP :     251
           |||||||||||||||||||||||||||||||!:!||||||||||||||||||||||
           uGlnAspIleGluAlaThrSerSerThrCysSerSerGluAsnGlyLysGluIleP
 6506100 : GCAAGACATAGAAGCAACGTCATCGACGTGCAGCAGCGAAAACGGAAAGGAGATTT : 6506044

     252 : heThrGluThrAspIleLeuAspPheAspIleAsnMetPheAlaGluGluAspLeu :     269
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           heThrGluThrAspIleLeuAspPheAspIleAsnMetPheAlaGluGluAspLeu
 6506043 : TTACTGAGACGGATATTCTTGACTTTGATATAAACATGTTTGCCGAGGAGGATCTG : 6505990

     270 : IleGluLeuGluGlyLysLysThrAspProPheSerProAlaMetAspGlyMetAs :     288
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           IleGluLeuGluGlyLysLysThrAspProPheSerProAlaMetAspGlyMetAs
 6505989 : ATTGAGCTTGAGGGCAAGAAGACGGATCCCTTCAGTCCGGCAATGGACGGGATGAA : 6505933

     289 : nAsnAspAsnLeuAspAspAspPheLeuGlnLeuAsnAspLeuLeuAsnGluGluG :     307
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           nAsnAspAsnLeuAspAspAspPheLeuGlnLeuAsnAspLeuLeuAsnGluGluG
 6505932 : CAACGACAACCTAGACGACGACTTTCTACAATTGAATGACCTGCTTAACGAGGAGG : 6505876

     308 : luThrPheAspMetSerSerMetSerGlyValGlyIleAspIleAsnGlnGluGln :     325
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           luThrPheAspMetSerSerMetSerGlyValGlyIleAspIleAsnGlnGluGln
 6505875 : AGACTTTCGACATGTCATCGATGTCCGGCGTAGGCATCGACATCAaccaggagcag : 6505822

     326 : GluLeuAspPheIleAsnIlePheHisAsnSerGlnProThrAlaMetLeuThrSe :     344
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           GluLeuAspPheIleAsnIlePheHisAsnSerGlnProThrAlaMetLeuThrSe
 6505821 : gaactgGACTTCATCAACATATTCCACAACTCACAGCCGACGGCCATGCTCACCAG : 6505765

     345 : rSerGluAspProTyrLeuAlaThrTyrSerGlnAlaThrSerThrSerSerThrT :     363
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           rSerGluAspProTyrLeuAlaThrTyrSerGlnAlaThrSerThrSerSerThrT
 6505764 : CTCAGAGGATCCGTATCTGGCCACCTACTCTCaggccacatccacatccagcaCCA : 6505708

     364 : hrLysPheIleThrGluGluProLeuLeuAlaAspMetThrSerPheAspIleSer :     381
           ||||||||||||||||||||||||||:!!|||||||||||||||||||||||||||
           hrLysPheIleThrGluGluProLeuIleAlaAspMetThrSerPheAspIleSer
 6505707 : CCAAATTCATCACGGAGGAGCCCCTGATCGCCGACATGACCAGCTTCGACATCAGC : 6505654

     382 : TyrThrSerSerGlnGluLeuGlyPheGlyLeuValGluAspValSerGlySerPh :     400
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           TyrThrSerSerGlnGluLeuGlyPheGlyLeuValGluAspValSerGlySerPh
 6505653 : TATACCAGCAGCCAGGAGCTCGGCTTCGGCCTTGTGGAGGACGTCAGCGGCAGTTT : 6505597

     401 : eAlaSerSerCysSerAspValAlaAlaAlaIleProAlaProIleGlnHisProI :     419
           ||||!!!|||||||||||||||||||||||||||||||||||||||!!.|||||||
           eAlaThrSerCysSerAspValAlaAlaAlaIleProAlaProIleHisHisProI
 6505596 : TGCCACCAGTTGCAGCGACGTCGCCGCGGCCATCCCAGCCCCCATCCACCACCCCA : 6505540

     420 : leIleThrAsnIleLysArgSerAlaProAlaPro<->ValGluSerGlnProThr :     436
           |||||||||||:!!|||||||||||| !!||||||   ||||||||||||||||||
           leIleThrAsnLeuLysArgSerAlaAlaAlaProValValGluSerGlnProThr
 6505539 : TCATTACCAACCTCAAGCGTTCTGCTGCAGCCCCCGTGGTCGAGTCGCAGCCCACC : 6505486

     437 : LysArgSerArgLeuMetLeuGlnIleLysThrGluLeuAlaProAlaPheSerPr :     455
           ||||||||||||||||||||||||||||||||||||! !|||||||||||||||||
           LysArgSerArgLeuMetLeuGlnIleLysThrGluSerAlaProAlaPheSerPr
 6505485 : AAGCGCAGCCGACTGATGCTCCAGATCAAAACCGAGTCAGCTCCAGCATTTAGTCC : 6505429

     456 : oThrThrProGluIleIleAsnGlnLeuLeuAsnAspAspArg  >>>> Target :     470
           |||||||||||||||||||||||||||||||||||||||||||            1
           oThrThrProGluIleIleAsnGlnLeuLeuAsnAspAspArg++           
 6505428 : GACCACACCGGAGATTATCAACCAGCTCCTGAATGATGATCGGgt........... : 6505382

     471 :  Intron 2 >>>>  PheGluAlaProThrThrSerThrSerThrSerThrSerA :     483
           08 bp           ||||||||||||||||||||||||||||||||||||! !|
                         ++PheGluAlaProThrThrSerThrSerThrSerThrIleA
 6505381 : ..............agTTCGAGGCTCCCACCACAAGTACCAGTACCAGCACCATCA : 6505237

     484 : snThrSerIleSerSerSerThrHisAlaAspIleValGluAspLeuArgSerAla :     501
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           snThrSerIleSerSerSerThrHisAlaAspIleValGluAspLeuArgSerAla
 6505236 : ACAcctccatcagcagcagcacacatGCAGACATCGTAGAGGATCTACGCAGTGCC : 6505183

     502 : GluGluGluThrThrThrAspPheSerAlaProAsnThrProHisSerAsnTyrSe :     520
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           GluGluGluThrThrThrAspPheSerAlaProAsnThrProHisSerAsnTyrSe
 6505182 : GAAGAGGAGACTACCACCGACTTCTCGGCTCCGAACACACCACACAGCAACTACTC : 6505126

     521 : rAlaSerSerSerCysAlaAlaProThrCysGlnThrGlyTyrGlyGlyPheLeuT :     539
           |||||||||||||! !|||||| !!|||||||||||||||||||||||||||||||
           rAlaSerSerSerTyrAlaAlaSerThrCysGlnThrGlyTyrGlyGlyPheLeuT
 6505125 : AGCCAGCTCCAGCTATGCGGCGTCCACCTGCCAGACCGGATATGGTGGCTTCCTCA : 6505069

     540 : hrAlaProThrSerProAlaTyrSerThrAlaSerThrSerValPheSerProSer :     557
           ||||||||.!!||||||||||||||||||||||||||||||!.!||||||||||||
           hrAlaProAlaSerProAlaTyrSerThrAlaSerThrSerAlaPheSerProSer
 6505068 : CTGCTCCAGCCTCGCCCGCCTATTCAACAGCCAGTACCTCCGCGTTCAGCCCTTCG : 6505015

     558 : ProAlaSerGlyIleSerGlyLysArgLysArgGlyArgProAlaLysAspHisAl :     576
           ||||||.!!|||||||||||||||||||||||||||||||||||||||||||||||
           ProAlaGlyGlyIleSerGlyLysArgLysArgGlyArgProAlaLysAspHisAl
 6505014 : CCAGCCGGCGGAATCAGCGGCAAGCGGAAGCGCGGTCGCCCTGCCAAGGATCATGC : 6504958

     577 : aAspGlyProAspProValLeuMetSerSerMetLysSerGluGluGluArgLysA :     595
           ||||||||||||||||||||||||||||!:!|||||||||||||||||||||||||
           aAspGlyProAspProValLeuMetSerAsnMetLysSerGluGluGluArgLysA
 6504957 : CGACGGTCCCGATCCCGTGCTCATGTCCAACATGAAAAGCGAGGAGGAGAGGAAGG : 6504901

     596 : laTyrGlnAspArgLeuLysAsnAsnGluAlaSerArgValSerArgArgLysThr :     613
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           laTyrGlnAspArgLeuLysAsnAsnGluAlaSerArgValSerArgArgLysThr
 6504900 : CCTACCAGGACAGACTCAAGAACAACGAGGCTAGCCGCGTATCGCGCCGGAAGACG : 6504847

     614 : LysValArgGluGluGluGluLysArgAlaGluAspThrLeuLeuAlaGluAsnLe :     632
           ||||||||||||||||||||||||||||||||||||  !|||||||||||||||||
           LysValArgGluGluGluGluLysArgAlaGluAspGluLeuLeuAlaGluAsnLe
 6504846 : AAGGtgcgcgaggaggaggagaagcgcGCCGAGGACGAGCTGTTGGCCGAGAATCT : 6504790

     633 : uArgLeuArgAlaArgAlaAspGluValAlaSerArgGluArgLysPheLysLysT :     651
           |||||||||||||!:!||||||:!!||||||! !!:!|||||||||||||||||||
           uArgLeuArgAlaGlnAlaAspLysValAlaPheGlnGluArgLysPheLysLysT
 6504789 : GCGACTTCGTGCCCAGGCCGACAAAGTGGCCTTCCAGGAACGGAAGTTCAAGAAGT : 6504733

     652 : yrLeuMetGluArgGlnArgGlnLysSerThrTyrValLysGlnGluGlnAsp :     668
           ||||||||||||||||||||..!!!.|||||||||||||||||||||||||||
           yrLeuMetGluArgGlnArgMetAsnSerThrTyrValLysGlnGluGlnAsp
 6504732 : ACCTGATGGAGCGCCAGCGGATGAACAGCACTTACGtcaagcaggagcaggac : 6504680

vulgar: CG17836_PA 0 668 . CM002913.1 6510743 6504679 - 3185 M 92 276 G 1 0 M 136 408 S 0 1 5 0 2 I 0 3948 3 0 2 S 1 2 M 200 600 G 0 3 M 39 117 5 0 2 I 0 104 3 0 2 M 199 597
forcebeams 
>Dsim
ATGATCCAGGAGCCAGCACGAGTACAGGCGGCACCCACATTCGAGGCGACAATCCGCGAGGACAACGCAA
CATACCAGGAAACAGCGGCTGGCGGGACATACTCGTGGCCCAACTGCTCCTGCTCTCAGAGGAGAACGAG
GCCAACGGAGCCTACGAAGGATTCGCCTACGTCAACGGTCACCACAATACCGATCGTGATAGACAGCAAG
CCAAATCCCAGAATTCTGCTGATCAGAGTGCCCAcaagcaacagaagcagcaacagcagcagcaacaaca
gcactcGCTGCGGGACAGAGCACAGAGCGCCTCGAACTCGCATCATCAGAATACCCTTTTTACCAGGGTC
GGTACCAACGCGTCCGCCATCCAACCAGGCCGCGCCTGCAACGAGTTCTTTAGTAAATCCGATAGGTTCG
GCGAGGAGGTGCCGTATCTCAACGTACCCCACGACTGGGACACCAACATCCCGATCGCAACCAGAAACGG
TGGCAGGTTTACCGACTACGGCAATGGTGAACCAAACGGTCCCGGTTTTATCGAGCGCGCCTCAGACACA
GACCCTGGCAGCCATGGCTGGTATCCAGCTGGCAGGTGCGGACGAGCCGCCATTCCCCCGGGATGTGAGT
GGAGCAATGCCTACTGCCACAGTTGAAGAGATAGACGTTCCGGTGTTCATTGATGAATACCTGCAAGACA
TAGAAGCAACGTCATCGACGTGCAGCAGCGAAAACGGAAAGGAGATTTTTACTGAGACGGATATTCTTGA
CTTTGATATAAACATGTTTGCCGAGGAGGATCTGATTGAGCTTGAGGGCAAGAAGACGGATCCCTTCAGT
CCGGCAATGGACGGGATGAACAACGACAACCTAGACGACGACTTTCTACAATTGAATGACCTGCTTAACG
AGGAGGAGACTTTCGACATGTCATCGATGTCCGGCGTAGGCATCGACATCAaccaggagcaggaactgGA
CTTCATCAACATATTCCACAACTCACAGCCGACGGCCATGCTCACCAGCTCAGAGGATCCGTATCTGGCC
ACCTACTCTCaggccacatccacatccagcaCCACCAAATTCATCACGGAGGAGCCCCTGATCGCCGACA
TGACCAGCTTCGACATCAGCTATACCAGCAGCCAGGAGCTCGGCTTCGGCCTTGTGGAGGACGTCAGCGG
CAGTTTTGCCACCAGTTGCAGCGACGTCGCCGCGGCCATCCCAGCCCCCATCCACCACCCCATCATTACC
AACCTCAAGCGTTCTGCTGCAGCCCCCGTGGTCGAGTCGCAGCCCACCAAGCGCAGCCGACTGATGCTCC
AGATCAAAACCGAGTCAGCTCCAGCATTTAGTCCGACCACACCGGAGATTATCAACCAGCTCCTGAATGA
TGATCGGTTCGAGGCTCCCACCACAAGTACCAGTACCAGCACCATCAACAcctccatcagcagcagcaca
catGCAGACATCGTAGAGGATCTACGCAGTGCCGAAGAGGAGACTACCACCGACTTCTCGGCTCCGAACA
CACCACACAGCAACTACTCAGCCAGCTCCAGCTATGCGGCGTCCACCTGCCAGACCGGATATGGTGGCTT
CCTCACTGCTCCAGCCTCGCCCGCCTATTCAACAGCCAGTACCTCCGCGTTCAGCCCTTCGCCAGCCGGC
GGAATCAGCGGCAAGCGGAAGCGCGGTCGCCCTGCCAAGGATCATGCCGACGGTCCCGATCCCGTGCTCA
TGTCCAACATGAAAAGCGAGGAGGAGAGGAAGGCCTACCAGGACAGACTCAAGAACAACGAGGCTAGCCG
CGTATCGCGCCGGAAGACGAAGGtgcgcgaggaggaggagaagcgcGCCGAGGACGAGCTGTTGGCCGAG
AATCTGCGACTTCGTGCCCAGGCCGACAAAGTGGCCTTCCAGGAACGGAAGTTCAAGAAGTACCTGATGG
AGCGCCAGCGGATGAACAGCACTTACGtcaagcaggagcaggac

-- completed exonerate analysis
