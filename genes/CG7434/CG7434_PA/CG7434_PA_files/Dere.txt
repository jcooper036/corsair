Command line: [exonerate --model protein2genome --query genes/CG7434/CG7434_PA/CG7434_PA_files/CG7434_PA_prot.txt --target temp/scaffold_4845_CG7434_PA.fasta --ryo forcebeams 
>Dere
%tcs
 --bestn 1]
Hostname: [pathfinder.local]

C4 Alignment:
------------
         Query: CG7434_PA
        Target: scaffold_4845 type=golden_path_region; loc=scaffold_4845:1..22589142; ID=scaffold_4845; dbxref=GB:CH954179,REFSEQ:NW_001956550; MD5=8bb0f5f1625e5363f47329f326ee3ce9; length=22589142; release=r1.04; species=Dere;:[revcomp]
         Model: protein2genome:local
     Raw score: 447
   Query range: 16 -> 298
  Target range: 20504181 -> 20503332

       17 : AlaLysProAlaGluLysLysAlaAlaProAlaAlaAlaAlaAlaLysGlyLys :       34
            |||||| !!  !!     :!! !!:!!  !  !|||  !|||  !  !!.!   
            AlaLysSerLysAlaProGluProSerLysLysAlaGlnAlaLysValAlaPro
 20504181 : GCCAAATCGAAGGCCCCCGAGCCGTCGAAGAAAGCCCAAGCTAAGGTGGCCCCT : 20504130

       35 : ValGluLysProLysAlaGluAlaAlaLysProAlaAlaAlaAlaAlaLysAsn :       52
            ||||||    !!!  |||...||||||  !||| !!  !:!!:!!  !|||   
            ValGluValAlaThrAlaSerAlaAlaProProProLeuSerSerLysLysVal
 20504129 : GTAGAAGTCGCCACTGCTTCTGCGGCTCCGCCGCCTTTGTCCTCGAAGAAGGTG : 20504076

       53 : ValLysLysAlaSerGluAlaAlaLysAspValLysAlaAlaAlaAlaAlaAla :       70
            !.!|||    !!:!!! !|||  !|||:!!:!!:!!  !|||  !  !! !:!!
            AlaLysAlaProAlaValAlaLeuLysAsnLeuGluLeuAlaMetLysGluSer
 20504075 : GCCAAAGCTCCGGCCGTGGCTCTGAAGAACCTCGAGTTGGCCATGAAGGAATCG : 20504022

       71 : LysProAlaAlaAlaLysProAlaAlaAlaLysProAlaAlaAlaSerLysAsp :       88
                 !  !|||::!:!!  !|||! !  !|||  !||||||  ! !!||| ! 
            AlaLysLysAlaSerGluMetAlaGluGlnLysPheAlaAlaLysProLysLys
 20504021 : GCTAAGAAGGCCAGCGAAATGGCAGAGCAGAAGTTTGCCGCAAAGCCGAAGAAA : 20503968

       89 : AlaGlyLysLysAlaProAlaAlaAlaAlaProLysLysAspAlaLysAlaAla :      106
            |||  !!!.    !!! !  !  !!.!  !|||  !  !||| !!  !||| !!
            AlaGlnAsnAlaProArgLysAsnValAsnProValProAspProValAlaPro
 20503967 : GCCCAGAATGCTCCGCGCAAGAACGTGAACCCAGTGCCGGACCCAGTAGCTCCT : 20503914

      107 : AlaAlaProAlaProAlaLysAlaAlaProAlaLysLysAlaAlaSerThrPro :      124
            |||!.!  !  !  !  !!:!:!!|||||| !!! !:::!.!  ! !!|||  !
            AlaValValGlnLysLysArgSerAlaProProThrArgGlyLysProThrGly
 20503913 : GCTGTAGTCCAGAAAAAGAGATCGGCGCCTCCTACACGTGGCAAGCCCACGGGT : 20503860

      125 : AlaAlaAlaProProAlaLysLysAlaAlaProAlaLysAlaAlaAlaProAla :      142
            |||||| !!  !  !|||     !!.!  !  !.!!     !! !  ! !! !!
            AlaAlaProLysArgAlaAlaLeuValGlnGluThrProLysGluGlnAlaPro
 20503859 : GCTGCCCCCAAAAGGGCTGCCCTAGTCCAAGAAACGCCCAAGGAACAAGCTCCC : 20503806

      143 : AlaAlaAlaProAlaProAlaAlaAlaAlaProAlaValAlaLysProAlaPro :      160
            .!!||| !!  !|||  !|||  !! !||| !!|||..!  !!..  !! !  !
            ThrAlaProLysAlaGluAlaIleGluAlaThrAlaThrLysSerValAspLys
 20503805 : ACGGCACCTAAAGCCGAAGCAATTGAAGCAACGGCCACTAAAAGTGTCGACAAG : 20503752

      161 : LysProLysAlaLysAlaAlaProAlaProSerLysValValLysLysAsnVal :      178
            !..  !   |||  !!.!  !! !  !|||!.!  !  !  !   |||||||||
            SerIleAlaAlaProValAsnGlnLysProLysProLysArgAlaLysAsnVal
 20503751 : AGCATTGCTGCCCCGGTAAACCAGAAGCCCAAGCCGAAACGCGCCAAGAATGTG : 20503698

      179 : LeuArgGlyLysGlyGlnLysLysLysLysValSerLeuArgPheThrIleAsp :      196
            |||||||||||| !!..!...|||||||||:!!!!!! !||||||..!||||||
            LeuArgGlyLysArgMetSerLysLysLysIleTrpGlnArgPheValIleAsp
 20503697 : CTGCGTGGCAAGAGGATGTCCAAGAAGAAAATCTGGCAGCGCTTTGTGATTGAC : 20503644

      197 : CysThrAsnIleAlaGluAspSerIleMetAspValAlaAspPheGluLysTyr :      214
            |||.!!  !:!:|||||||||! !|||:!!|||:!!||||||||||||||||||
            CysAlaCysValAlaGluAspLeuIleLeuAspIleAlaAspPheGluLysTyr
 20503643 : TGCGCATGCGTGGCCGAGGACTTGATTCTGGATATTGCCGACTTCGAAAAGTAT : 20503590

      215 : IleLysAlaArgLeuLysValAsnGlyLysValAsnAsnLeuGlyAsnAsnVal :      232
            :!::::.!!!.!:!!|||:!!!!...!|||||||||.!.|||  !:!!.!.|||
            LeuArgThrHisIleLysIleLysAsnLysValAsnGlnLeuLysAspGlnVal
 20503589 : TTGCGCACCCACATCAAGATAAAGAACAAGGTCAACCAGCTGAAGGACCAGGTC : 20503536

      233 : ThrPheGluArgSerLys<->LeuLysLeuIleValSerSerAspValHisPhe :      249
            ||||||||||||  !|||   ! !..!|||:!!:!!  !|||!  |||||||||
            ThrPheGluArgValLysAsnSerSerLeuValIleHisSerAlaValHisPhe
 20503535 : ACTTTCGAGCGGGTCAAGAACTCCTCGCTGGTCATCCACTCCGCGGTGCACTTC : 20503482

      250 : SerLysAlaTyrLeuLysTyrLeuThrLysLysTyrLeuLysLysAsnSerLeu :      267
            ||||||  !|||!!!|||||||||.!!|||::!||||||||||||:!!||||||
            SerLysArgTyrPheLysTyrLeuAlaLysArgTyrLeuLysLysHisSerLeu
 20503481 : TCCAAGCGCTACTTCAAGTACCTGGCCAAGCGGTACCTGAAGAAGCACAGCCTG : 20503428

      268 : ArgAspTrpIleArgValValAlaAsnGluLysAspSerTyrGluLeuArgTyr :      285
            |||||||||:!:|||||||||:!!!..!  ||||||!!!!:!!  :!!!!!|||
            ArgAspTrpValArgValValSerThrAlaLysAspThrPheAlaMetSerTyr
 20503427 : CGCGATTGGGTGCGAGTGGTGTCCACGGCCAAGGACACCTTCGCCATGAGCTAC : 20503374

      286 : PheArgIleSerSerAsnAspAspGluAspAspAspAla :      298
            |||!:!|||..!:!!:!!||||||!!:|||! !!!:|||
            PheLysIleGlnAlaAspAspAspAspAspValGluAla
 20503373 : TTCAAGATCCAGGCCGATGACGACGACGACGTCGAGGCC : 20503333

vulgar: CG7434_PA 16 298 . scaffold_4845 20504181 20503332 - 447 M 222 666 G 0 3 M 60 180
forcebeams 
>Dere
GCCAAATCGAAGGCCCCCGAGCCGTCGAAGAAAGCCCAAGCTAAGGTGGCCCCTGTAGAAGTCGCCACTG
CTTCTGCGGCTCCGCCGCCTTTGTCCTCGAAGAAGGTGGCCAAAGCTCCGGCCGTGGCTCTGAAGAACCT
CGAGTTGGCCATGAAGGAATCGGCTAAGAAGGCCAGCGAAATGGCAGAGCAGAAGTTTGCCGCAAAGCCG
AAGAAAGCCCAGAATGCTCCGCGCAAGAACGTGAACCCAGTGCCGGACCCAGTAGCTCCTGCTGTAGTCC
AGAAAAAGAGATCGGCGCCTCCTACACGTGGCAAGCCCACGGGTGCTGCCCCCAAAAGGGCTGCCCTAGT
CCAAGAAACGCCCAAGGAACAAGCTCCCACGGCACCTAAAGCCGAAGCAATTGAAGCAACGGCCACTAAA
AGTGTCGACAAGAGCATTGCTGCCCCGGTAAACCAGAAGCCCAAGCCGAAACGCGCCAAGAATGTGCTGC
GTGGCAAGAGGATGTCCAAGAAGAAAATCTGGCAGCGCTTTGTGATTGACTGCGCATGCGTGGCCGAGGA
CTTGATTCTGGATATTGCCGACTTCGAAAAGTATTTGCGCACCCACATCAAGATAAAGAACAAGGTCAAC
CAGCTGAAGGACCAGGTCACTTTCGAGCGGGTCAAGAACTCCTCGCTGGTCATCCACTCCGCGGTGCACT
TCTCCAAGCGCTACTTCAAGTACCTGGCCAAGCGGTACCTGAAGAAGCACAGCCTGCGCGATTGGGTGCG
AGTGGTGTCCACGGCCAAGGACACCTTCGCCATGAGCTACTTCAAGATCCAGGCCGATGACGACGACGAC
GTCGAGGCC

-- completed exonerate analysis
