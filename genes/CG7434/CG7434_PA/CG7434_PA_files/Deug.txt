Command line: [exonerate --model protein2genome --query genes/CG7434/CG7434_PA/CG7434_PA_files/CG7434_PA_prot.txt --target temp/KB465221.1_CG7434_PA.fasta --ryo forcebeams 
>Deug
%tcs
 --bestn 1]
Hostname: [pathfinder.local]

C4 Alignment:
------------
         Query: CG7434_PA
        Target: KB465221.1 Drosophila eugracilis unplaced genomic scaffold scf7180000409672, whole genome shotgun sequence:[revcomp]
         Model: protein2genome:local
     Raw score: 446
   Query range: 16 -> 297
  Target range: 7187832 -> 7186980

      17 : AlaLysProAlaGluLysLysAlaAlaProAlaAlaAlaAlaAlaLysGlyLysVa :      35
           ||||||   |||.!.  !|||||| !!  ! !!.!!  !  !||||||||||||||
           AlaLysLysAlaAsnProLysAlaProGluProThrLysLysAlaLysGlyLysVa
 7187832 : gccaaaaaggCTAATCCGAAAGCTCCCGAGCCTACGAAGAAAGCCAAAGGTAAGGT : 7187778

      36 : lGluLysProLysAlaGluAlaAlaLysProAlaAlaAlaAlaAla<->LysAsnV :      53
           |! !:!! !!  !!.!   ! !  !...||||||||| !!  !:!!   |||!!.|
           lAlaGluThrAlaValProAspLysSerProAlaAlaProLeuSerAsnLysLysV
 7187777 : GGCAGAAACCGCAGTCCCTGACAAGTCTCCTGCTGCCCCACTGTCTAATAAGAAAG : 7187721

      54 : alLysLysAla<->SerGluAlaAlaLysAspValLysAlaAlaAlaAlaAlaAla :      70
           ||   ||||||   :!!! !|||  !|||:!!  !:!!  !|||  !  !! !:!!
           alAlaLysAlaProAlaValAlaLeuLysAsnHisGluLeuAlaMetLysGluSer
 7187720 : TGGCCAAAGCTCCGGCTGTGGCCCTGAAGAATCATGAGTTGGCCATGAAGGAGTCG : 7187667

      71 : LysProAlaAlaAlaLysProAlaAlaAlaLysProAlaAlaAlaSerLysAspAl :      89
                !  !::!  !:!!  !|||! !  !|||  !|||  !:!!:!!!:! ! ||
           AlaLysLysSerLeuGluAsnAlaAspLysLysIleAlaLeuSerAlaArgLysAl
 7187666 : GCCAAGAAAAGCCTGGAAAATGCTGACAAGAAAATAGCCTTGTCGGCTAGAAAAGC : 7187610

      90 : aGlyLysLysAlaProAlaAlaAlaAlaProLysLysAspAlaLysAlaAlaAlaA :     108
           |  !:!!!..         :::     !|||  !  !:!!  !|||  ! !!||| 
           aIleGluSerIleGluLysSerAsnLeuProValProAsnLysLysArgProAlaP
 7187609 : CATCGAAAGcattgaaaaatcaaacttGCCGGTGCCAAACAAAAAGAGGCCTGCCC : 7187553

     109 : laProAlaProAlaLysAlaAlaProAlaLysLysAlaAlaSerThrProAlaAla :     126
           !! !!||||||   ...|||       !!:!!    !!|||:!!! !  !  ! !!
           roThrAlaProGluAsnAlaLysLysProGluAlaProAlaAlaLysLysLeuPro
 7187552 : CTACGGCACctgaaaatgcaaagaaacCCGAGGCTCCCGCTGCCAAAAAGCTACCT : 7187499

     127 : AlaProProAlaLysLysAlaAlaProAlaLysAlaAlaAlaProAlaAlaAlaAl :     145
            !!  !  ! !!!  ||||||||| !! !!  !||| !!  !|||  !  !! !!.
           ProValLysProThrLysAlaAlaThrProProAlaProLysProLysLysGluVa
 7187498 : CCAGTCAAGCCAACCAAGGCAGCCACTCCTCCAGCCCCAAAGCCTAAGAAGGAGGT : 7187442

     146 : aProAlaProAlaAlaAlaAlaProAlaValAlaLysProAlaProLysProLysA :     164
           !  !.!! !!! !!.!! !||| !! !!! !.!!! ! !!.!!  !    !!   |
           lValThrAlaGluGlyGluAlaAlaProGluThrIleAlaThrLysAlaAlaAlaA
 7187441 : GGTAACAGCCGAAGGAGAAGCAGCTCCGGAAACTATAGCCACCAAGGCTGCAGCTG : 7187385

     165 : laLysAlaAlaProAlaProSerLysValValLysLysAsnValLeuArgGlyLys :     182
           ||  !|||  !|||  !! !!.!     !  !  !|||||||||||||||||||||
           laProAlaLysProLysArgLysProLysArgProLysAsnValLeuArgGlyLys
 7187384 : CTCCGGCCAAACCGAAACGAAAGCCTAAACGCCCGAAGAATGTTCTGCGCGGAAAG : 7187331

     183 : GlyGlnLysLysLysLysValSerLeuArgPheThrIleAspCysThrAsnIleAl :     201
            ! ...   |||||||||::!!!!! !||||||..!||||||||||||  !:!:||
           ArgMetAlaLysLysLysIleTrpGlnArgPheValIleAspCysThrCysLeuAl
 7187330 : CGgatggccaaaaagaagatCTGGCAACGCTTCGTTATCGACTGCACATGCTTGGC : 7187274

     202 : aGluAspSerIleMetAspValAlaAspPheGluLysTyrIleLysAlaArgLeuL :     220
           |||||||! !:!!|||||||||  !|||||||||||||||.!!|||.!!!.!:!!|
           aGluAspMetLeuMetAspValAsnAspPheGluLysTyrPheLysThrHisIleL
 7187273 : CGAGGACATGCTTATGGATGTCAACGATTTCGAGAAGTATTTCAAGACTCACATCA : 7187217

     221 : ysValAsnGlyLysValAsnAsnLeuGlyAsnAsnValThrPheGluArgSerLys :     238
           |||||!!...!|||..!|||.!.|||..!:!!   |||||||||||||||!!!|||
           ysValLysAsnLysThrAsnGlnLeuAsnAspLeuValThrPheGluArgThrLys
 7187216 : AGGTGAAGAACAAGACCAACCAGCTAAACGATCTGGTCACCTTCGAGAGGACCAAG : 7187163

     239 : <->LeuLysLeuIleValSerSerAspValHisPheSerLysAlaTyrLeuLysTy :     256
              ! !...||||||:!!  !|||! !|||||||||||||||  !|||!!!|||||
           AsnTyrSerLeuIleIleHisSerGlyValHisPheSerLysArgTyrPheLysTy
 7187162 : AACTACTCCTTGATCATCCACAGTGGTGTCCACTTCTCGAAGAGATATTTTAAGTA : 7187106

     257 : rLeuThrLysLysTyrLeuLysLysAsnSerLeuArgAspTrpIleArgValValA :     275
           ||||.!!|||:::||||||||||||||||||||||||||||||:!:|||:!!|||:
           rLeuAlaLysArgTyrLeuLysLysAsnSerLeuArgAspTrpValArgLeuValS
 7187105 : CCTCGCCAAGCGCTACCTCAAGAAGAACAGTCTGCGCGATTGGGTGCGGCTAGTGT : 7187049

     276 : laAsnGluLysAspSerTyrGluLeuArgTyrPheArgIleSerSerAsnAspAsp :     293
           !!!.!...||||||!!!!:!   :!!|||||||||!:!|||..!.!!...||||||
           erThrSerLysAspThrPheThrMetArgTyrPheLysIleGlnGlyGlnAspAsp
 7187048 : CCACCAGCAAGGACACGTTCACCATGCGCTACTTCAAGATCCAAGGGCaggacgat : 7186995

     294 : GluAspAspAsp :     297
           :::|||||||||
           AspAspAspAsp
 7186994 : gacgacgatgat : 7186981

vulgar: CG7434_PA 16 297 . KB465221.1 7187832 7186980 - 446 M 34 102 G 0 3 M 6 18 G 0 3 M 182 546 G 0 3 M 59 177
forcebeams 
>Deug
gccaaaaaggCTAATCCGAAAGCTCCCGAGCCTACGAAGAAAGCCAAAGGTAAGGTGGCAGAAACCGCAG
TCCCTGACAAGTCTCCTGCTGCCCCACTGTCTAATAAGAAAGTGGCCAAAGCTCCGGCTGTGGCCCTGAA
GAATCATGAGTTGGCCATGAAGGAGTCGGCCAAGAAAAGCCTGGAAAATGCTGACAAGAAAATAGCCTTG
TCGGCTAGAAAAGCCATCGAAAGcattgaaaaatcaaacttGCCGGTGCCAAACAAAAAGAGGCCTGCCC
CTACGGCACctgaaaatgcaaagaaacCCGAGGCTCCCGCTGCCAAAAAGCTACCTCCAGTCAAGCCAAC
CAAGGCAGCCACTCCTCCAGCCCCAAAGCCTAAGAAGGAGGTGGTAACAGCCGAAGGAGAAGCAGCTCCG
GAAACTATAGCCACCAAGGCTGCAGCTGCTCCGGCCAAACCGAAACGAAAGCCTAAACGCCCGAAGAATG
TTCTGCGCGGAAAGCGgatggccaaaaagaagatCTGGCAACGCTTCGTTATCGACTGCACATGCTTGGC
CGAGGACATGCTTATGGATGTCAACGATTTCGAGAAGTATTTCAAGACTCACATCAAGGTGAAGAACAAG
ACCAACCAGCTAAACGATCTGGTCACCTTCGAGAGGACCAAGAACTACTCCTTGATCATCCACAGTGGTG
TCCACTTCTCGAAGAGATATTTTAAGTACCTCGCCAAGCGCTACCTCAAGAAGAACAGTCTGCGCGATTG
GGTGCGGCTAGTGTCCACCAGCAAGGACACGTTCACCATGCGCTACTTCAAGATCCAAGGGCaggacgat
gacgacgatgat

-- completed exonerate analysis
