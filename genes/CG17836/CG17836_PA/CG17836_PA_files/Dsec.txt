Command line: [exonerate --model protein2genome --query genes/CG17836/CG17836_PA/CG17836_PA_files/CG17836_PA_prot.txt --target temp/CH480820.1_CG17836_PA.fasta --ryo forcebeams 
>Dsec
%tcs
 --bestn 1]
Hostname: [pathfinder.local]

C4 Alignment:
------------
         Query: CG17836_PA
        Target: CH480820.1 Drosophila sechellia strain Rob3c scaffold_5 genomic scaffold, whole genome shotgun sequence:[revcomp]
         Model: protein2genome:local
     Raw score: 3147
   Query range: 0 -> 668
  Target range: 5150524 -> 5144312

       1 : MetIleGlnGluProAlaArgValGlnAlaAlaProThrPheGluAlaThrIleAr :      19
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           MetIleGlnGluProAlaArgValGlnAlaAlaProThrPheGluAlaThrIleAr
 5150524 : ATGATCCAGGAGCCAGCACGAGTACAGGCGGCACCCACATTCGAGGCGACAATCCG : 5150470

      20 : gGluAspAsnAlaLysTyrGlnGluThrAlaAlaGlyGlyThrTyrSerSerProA :      38
           |||||||||||||! !|||||||||||||||||||||||||||||||||!!!||||
           gGluAspAsnAlaThrTyrGlnGluThrAlaAlaGlyGlyThrTyrSerTrpProA
 5150469 : GGAGGACAACGCAACATACCAGGAAACAGCGGCTGGCGGGACATACTCGTGGCCCA : 5150413

      39 : snCysSerCysSerGlnArgArgThrArgProThrGluProThrLysAspSerPro :      56
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           snCysSerCysSerGlnArgArgThrArgProThrGluProThrLysAspSerPro
 5150412 : ACTGCTCCTGCTCTCAGAGGAGAACGAGGCCAACGGAGCCTACGAAGGATTCGCCT : 5150359

      57 : ThrSerThrIleThrThrIleProIleValIleAspSerLysProAsnProArgIl :      75
           |||||||||:!!||||||||||||||||||||||||||||||||||||||||||||
           ThrSerThrValThrThrIleProIleValIleAspSerLysProAsnProArgIl
 5150358 : ACGTCAACGGTCACCACAATACCGATCGTGATAGACAGCAAGCCAAATCCCAGAAT : 5150302

      76 : eLeuLeuIleArgValProThrSerAsnArgGlySerAsnSerAsnAsnAsnSerA :      94
           |||||||||||||||||||||||||||||||...||||||||||||:::|||   |
           eLeuLeuIleArgValProThrSerAsnArgSerSerAsnSerAsnSerAsn---A
 5150301 : TCTGCTGATCAGAGTGCCCACAAgcaacagaagcagcaacagcaacagcaac---a : 5150248

      95 : snSerThrArgSerGlyThrGluHisArgAlaProArgThrArgIleIleArgIle :     112
           |||||||||||!!!||||||||||||||||||||||||||||||||||||||||||
           snSerThrArgCysGlyThrGluHisArgAlaProArgThrArgIleIleArgIle
 5150247 : acagcactCGCTGCGGGACAGAGCACAGAGCGCCTCGAACACGCATCATCAGAATA : 5150194

     113 : ProPheLeuProGlySerValProThrArgProProSerAsnGlnAlaAlaProAl :     131
           ||||||||||||||||||||||||||||||||||||||||||||||||!.!|||||
           ProPheLeuProGlySerValProThrArgProProSerAsnGlnAlaValProAl
 5150193 : CCCTTTTTACCAGGGTCGGTACCAACGCGTCCGCCATCCAACCAGGCCGTGCCTGC : 5150137

     132 : aThrSerSerLeuValAsnProIleGlySerAlaArgArgCysArgIleSerThrT :     150
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           aThrSerSerLeuValAsnProIleGlySerAlaArgArgCysArgIleSerThrT
 5150136 : AACGAGTTCTTTAGTAAATCCGATAGGTTCGGCGAGGAGGTGCCGTATCTCAACGT : 5150080

     151 : yrProThrThrGlyThrLeuThrSerArgSerGlnProGluThrValAlaGlyLeu :     168
           |||||||||||||||||! !||||||||||||||||||||||||||||||||||||
           yrProThrThrGlyThrProThrSerArgSerGlnProGluThrValAlaGlyLeu
 5150079 : ACCCCACGACTGGGACACCAACATCCCGATCGCAACCAGAAACGGTGGCAGGTTTA : 5150026

     169 : ProThrThrAlaMetValAsnGlnThrValProValLeuSerSerAlaProGlnTh :     187
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           ProThrThrAlaMetValAsnGlnThrValProValLeuSerSerAlaProGlnTh
 5150025 : CCGACTACGGCAATGGTGAACCAAACGGTCCCGGTTTTATCGAGCGCGCCTCAGAC : 5149969

     188 : rGlnThrLeuAlaAlaMetAlaGlyIleGlnLeuThrGlyAlaAspGluProProS :     206
           ||||||||||||||||||||||||||||||||||.!!||||||||||||||||||!
           rGlnThrLeuAlaAlaMetAlaGlyIleGlnLeuAlaGlyAlaAspGluProProP
 5149968 : ACAGACCCTGGCAGCCATGGCTGGTATCCAGCTGGCAGGTGCGGACGAGCCGCCAT : 5149912

     207 : erArgArgAspValSerGlyAlaMetProThrAlaThrValGluGluIleAspVal :     224
            !!!!|||||||||||||||||||||||||||||||||||||||||||||||||||
           heSerArgAspValSerGlyAlaMetProThrAlaThrValGluGluIleAspVal
 5149911 : TCAGCCGGGATGTGAGTGGAGCAATGCCTACTGCCACAGTTGAAGAGATAGACGTT : 5149858

     225 : ProValPheIleAsp{G}  >>>> Target Intron 1 >>>>  {lu}TyrLe :     232
           |||||||||||||||{|}           4143 bp           {||}|||||
           ProValPheIleAsp{G}++                         ++{lu}TyrLe
 5149857 : CCGGTGTTCATTGAT{G}gt.........................ag{AA}TACCT : 5145691

     233 : uGlnAspIleGluAlaThrSerSerThrCysAsnSerGluAsnGlyLysGluIleP :     251
           |||||||||||||||||||||||||||||||!:!||||||||||||||||||||||
           uGlnAspIleGluAlaThrSerSerThrCysSerSerGluAsnGlyLysGluIleP
 5145690 : GCAAGACATAGAAGCAACGTCATCGACGTGCAGCAGCGAAAACGGAAAGGAGATTT : 5145634

     252 : heThrGluThrAspIleLeuAspPheAspIleAsnMetPheAlaGluGluAspLeu :     269
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           heThrGluThrAspIleLeuAspPheAspIleAsnMetPheAlaGluGluAspLeu
 5145633 : TTACTGAGACGGATATTCTTGACTTTGATATAAACATGTTTGCCGAGGAGGATCTG : 5145580

     270 : IleGluLeuGluGlyLysLysThrAspProPheSerProAlaMetAspGlyMetAs :     288
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||:!
           IleGluLeuGluGlyLysLysThrAspProPheSerProAlaMetAspGlyMetAs
 5145579 : ATTGAGCTTGAGGGCAAAAAGACGGATCCCTTCAGTCCGGCAATGGATGGGATGGA : 5145523

     289 : nAsnAspAsnLeuAspAspAspPheLeuGlnLeuAsnAspLeuLeuAsnGluGluG :     307
           !|||||||||||||||||||||||||||||||||||||||||||||||||||||||
           pAsnAspAsnLeuAspAspAspPheLeuGlnLeuAsnAspLeuLeuAsnGluGluG
 5145522 : CAACGACAACCTAGACGACGACTTTCTACAATTGAATGACCTGCTTAACGAGGAGG : 5145466

     308 : luThrPheAspMetSerSerMetSerGlyValGlyIleAspIleAsnGlnGluGln :     325
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           luThrPheAspMetSerSerMetSerGlyValGlyIleAspIleAsnGlnGluGln
 5145465 : AGACTTTCGACATGTCATCGATGTCCGGCGTAGGCATCGACATCAACCAGGAGCAG : 5145412

     326 : GluLeuAspPheIleAsnIlePheHisAsnSerGlnProThrAlaMetLeuThrSe :     344
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           GluLeuAspPheIleAsnIlePheHisAsnSerGlnProThrAlaMetLeuThrSe
 5145411 : GAACTGGACTTCATCAACATATTCCACAACTCACAGCCGACGGCCATGCTCACCAG : 5145355

     345 : rSerGluAspProTyrLeuAlaThrTyrSerGlnAlaThrSerThrSerSerThrT :     363
           |||||||||||||||||||!.!||||||||||||||||||||||||||||||||||
           rSerGluAspProTyrLeuGlyThrTyrSerGlnAlaThrSerThrSerSerThrT
 5145354 : CTCAGAGGATCCGTATCTGGGCACCTACTCTCaggccacatccacatccagcACCA : 5145298

     364 : hrLysPheIleThrGluGluProLeuLeuAlaAspMetThrSerPheAspIleSer :     381
           ||||||||||||||||||||||||||:!!|||||||||||||||||||||||||||
           hrLysPheIleThrGluGluProLeuIleAlaAspMetThrSerPheAspIleSer
 5145297 : CCAAATTCATCACGGAGGAGCCCCTGATCGCCGACATGACCAGCTTCGACATCAGC : 5145244

     382 : TyrThrSerSerGlnGluLeuGlyPheGlyLeuValGluAspValSerGlySerPh :     400
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           TyrThrSerSerGlnGluLeuGlyPheGlyLeuValGluAspValSerGlySerPh
 5145243 : TATACCAGCAGCCAGGAGCTCGGCTTCGGCCTTGTGGAGGACGTCAGCGGCAGTTT : 5145187

     401 : eAlaSerSerCysSerAspValAlaAlaAlaIleProAlaProIleGlnHisProI :     419
           ||||!!!|||||||||||||||||||||||||||||||||||||||!!.|||||||
           eAlaThrSerCysSerAspValAlaAlaAlaIleProAlaProIleHisHisProI
 5145186 : TGCCACCAGTTGCAGCGACGTCGCCGCGGCCATCCCAGCCCCCATCCACCACCCCA : 5145130

     420 : leIleThrAsnIleLysArgSerAlaProAlaPro<->ValGluSerGlnProThr :     436
           |||||||||||:!!|||||||||||||||||||||   |||:!!||||||||||||
           leIleThrAsnLeuLysArgSerAlaProAlaProValValGlnSerGlnProThr
 5145129 : TCATTACCAACCTCAAGCGTTCTGCTCCAGCCCCCGTGGTCCAGTCGCAGCCCACC : 5145076

     437 : LysArgSerArgLeuMetLeuGlnIleLysThrGluLeuAlaProAlaPheSerPr :     455
           |||! !||||||||||||||||||||||||||||||! !|||||||||||||||||
           LysLeuSerArgLeuMetLeuGlnIleLysThrGluProAlaProAlaPheSerPr
 5145075 : AAGCTCAGCCGACTGATGCTCCAGATCAAAACCGAGCCAGCTCCAGCATTCAGTCC : 5145019

     456 : oThrThrProGluIleIleAsnGlnLeuLeuAsnAspAspArg  >>>> Target :     470
           |||||||||||||||||||||||||||||||||||||||||||            6
           oThrThrProGluIleIleAsnGlnLeuLeuAsnAspAspArg++           
 5145018 : GACCACACCGGAGATTATCAACCAGCTCCTGAATGATGATCGGgt........... : 5144972

     471 :  Intron 2 >>>>  PheGluAlaProThrThrSerThrSerThrSerThrSerA :     483
           5 bp            ||||||||||||||||||!!!|||||||||||||||! !|
                         ++PheGluAlaProThrThrThrThrSerThrSerThrIleA
 5144971 : ..............agTTCGAGGCTCCCACCACAACTACCAGTACCAGCACCATCA : 5144870

     484 : snThrSerIleSerSerSerThrHisAlaAspIleValGluAspLeuArgSerAla :     501
           |||||||||||||||||||||||||||||! !||||||||||||||||||||||||
           snThrSerIleSerSerSerThrHisAlaGlyIleValGluAspLeuArgSerAla
 5144869 : ACACctccatcagcagcagcacacaTGCAGGCATCGTAGAGGATCTACGCAGTGCC : 5144816

     502 : GluGluGluThrThrThrAspPheSerAlaProAsnThrProHisSerAsnTyrSe :     520
           ||||||||||||||||||||||||||||||||||||||||||||||||!:!|||||
           GluGluGluThrThrThrAspPheSerAlaProAsnThrProHisSerSerTyrSe
 5144815 : GAAGAGGAGACTACCACCGACTTCTCGGCTCCGAACACACCACACAGCAGCTACTC : 5144759

     521 : rAlaSerSerSerCysAlaAlaProThrCysGlnThrGlyTyrGlyGlyPheLeuT :     539
           |!.!|||||||||! !|||||| !!|||||||||||||||||||||||||||||||
           rValSerSerSerTyrAlaAlaSerThrCysGlnThrGlyTyrGlyGlyPheLeuT
 5144758 : AGTCAGCTCCAGCTATGCGGCGTCCACCTGCCAGACCGGATATGGTGGCTTCCTCA : 5144702

     540 : hrAlaProThrSerProAlaTyrSerThrAlaSerThrSerValPheSerProSer :     557
           ||||||||.!!||||||||||||||||||||||||||||||!.!||||||||||||
           hrAlaProAlaSerProAlaTyrSerThrAlaSerThrSerAlaPheSerProSer
 5144701 : CTGCTCCAGCCTCGCCCGCCTATTCAACAGCCAGTACCTCCGCGTTCAGCCCTTCG : 5144648

     558 : ProAlaSerGlyIleSerGlyLysArgLysArgGlyArgProAlaLysAspHisAl :     576
           ||||||.!!|||||||||!.!|||||||||||||||||||||||||||||||||:!
           ProAlaGlyGlyIleSerAlaLysArgLysArgGlyArgProAlaLysAspHisSe
 5144647 : CCAGCCGGCGGAATCAGCGCCAAGCGGAAGCGCGGTCGCCCTGCCAAGGATCATTC : 5144591

     577 : aAspGlyProAspProValLeuMetSerSerMetLysSerGluGluGluArgLysA :     595
           !|||||||||||||||||||||||||||!:!|||||||||||||||||||||||||
           rAspGlyProAspProValLeuMetSerAsnMetLysSerGluGluGluArgLysA
 5144590 : CGACGGTCCCGATCCCGTGCTCATGTCCAACATGAAAAGCGAGGAGGAGAGGAAGG : 5144534

     596 : laTyrGlnAspArgLeuLysAsnAsnGluAlaSerArgValSerArgArgLysThr :     613
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           laTyrGlnAspArgLeuLysAsnAsnGluAlaSerArgValSerArgArgLysThr
 5144533 : CCTACCAGGACAGACTCAAGAACAACGAGGCTAGCCGCGTATCGCGCCGGAAGACG : 5144480

     614 : LysValArgGluGluGluGluLysArgAlaGluAspThrLeuLeuAlaGluAsnLe :     632
           ||||||||||||||||||||||||||||||||||||  !|||||||||||||||||
           LysValArgGluGluGluGluLysArgAlaGluAspGluLeuLeuAlaGluAsnLe
 5144479 : AAGGTgcgcgaggaggaggagaagcgCGCCGAGGACGAGCTGTTGGCCGAGAATCT : 5144423

     633 : uArgLeuArgAlaArgAlaAspGluValAlaSerArgGluArgLysPheLysLysT :     651
           |||||||||||||!:!||||||:!!||||||! !!:!|||||||||||||||||||
           uArgLeuArgAlaGlnAlaAspLysValAlaPheGlnGluArgLysPheLysLysT
 5144422 : GCGACTGCGTGCCCAGGCCGACAAAGTGGCCTTCCAGGAACGGAAGTTCAAGAAGT : 5144366

     652 : yrLeuMetGluArgGlnArgGlnLysSerThrTyrValLysGlnGluGlnAsp :     668
           ||||||||||||||||||||..!!!.|||||||||||||||||||||||||||
           yrLeuMetGluArgGlnArgMetAsnSerThrTyrValLysGlnGluGlnAsp
 5144365 : ACCTGATGGAGCGCCAGCGGATGAACAGCACTTACGTCAAGCAGGAGCAGGAC : 5144313

vulgar: CG17836_PA 0 668 . CH480820.1 5150524 5144312 - 3147 M 92 276 G 1 0 M 136 408 S 0 1 5 0 2 I 0 4139 3 0 2 S 1 2 M 200 600 G 0 3 M 39 117 5 0 2 I 0 61 3 0 2 M 199 597
forcebeams 
>Dsec
ATGATCCAGGAGCCAGCACGAGTACAGGCGGCACCCACATTCGAGGCGACAATCCGGGAGGACAACGCAA
CATACCAGGAAACAGCGGCTGGCGGGACATACTCGTGGCCCAACTGCTCCTGCTCTCAGAGGAGAACGAG
GCCAACGGAGCCTACGAAGGATTCGCCTACGTCAACGGTCACCACAATACCGATCGTGATAGACAGCAAG
CCAAATCCCAGAATTCTGCTGATCAGAGTGCCCACAAgcaacagaagcagcaacagcaacagcaacaaca
gcactCGCTGCGGGACAGAGCACAGAGCGCCTCGAACACGCATCATCAGAATACCCTTTTTACCAGGGTC
GGTACCAACGCGTCCGCCATCCAACCAGGCCGTGCCTGCAACGAGTTCTTTAGTAAATCCGATAGGTTCG
GCGAGGAGGTGCCGTATCTCAACGTACCCCACGACTGGGACACCAACATCCCGATCGCAACCAGAAACGG
TGGCAGGTTTACCGACTACGGCAATGGTGAACCAAACGGTCCCGGTTTTATCGAGCGCGCCTCAGACACA
GACCCTGGCAGCCATGGCTGGTATCCAGCTGGCAGGTGCGGACGAGCCGCCATTCAGCCGGGATGTGAGT
GGAGCAATGCCTACTGCCACAGTTGAAGAGATAGACGTTCCGGTGTTCATTGATGAATACCTGCAAGACA
TAGAAGCAACGTCATCGACGTGCAGCAGCGAAAACGGAAAGGAGATTTTTACTGAGACGGATATTCTTGA
CTTTGATATAAACATGTTTGCCGAGGAGGATCTGATTGAGCTTGAGGGCAAAAAGACGGATCCCTTCAGT
CCGGCAATGGATGGGATGGACAACGACAACCTAGACGACGACTTTCTACAATTGAATGACCTGCTTAACG
AGGAGGAGACTTTCGACATGTCATCGATGTCCGGCGTAGGCATCGACATCAACCAGGAGCAGGAACTGGA
CTTCATCAACATATTCCACAACTCACAGCCGACGGCCATGCTCACCAGCTCAGAGGATCCGTATCTGGGC
ACCTACTCTCaggccacatccacatccagcACCACCAAATTCATCACGGAGGAGCCCCTGATCGCCGACA
TGACCAGCTTCGACATCAGCTATACCAGCAGCCAGGAGCTCGGCTTCGGCCTTGTGGAGGACGTCAGCGG
CAGTTTTGCCACCAGTTGCAGCGACGTCGCCGCGGCCATCCCAGCCCCCATCCACCACCCCATCATTACC
AACCTCAAGCGTTCTGCTCCAGCCCCCGTGGTCCAGTCGCAGCCCACCAAGCTCAGCCGACTGATGCTCC
AGATCAAAACCGAGCCAGCTCCAGCATTCAGTCCGACCACACCGGAGATTATCAACCAGCTCCTGAATGA
TGATCGGTTCGAGGCTCCCACCACAACTACCAGTACCAGCACCATCAACACctccatcagcagcagcaca
caTGCAGGCATCGTAGAGGATCTACGCAGTGCCGAAGAGGAGACTACCACCGACTTCTCGGCTCCGAACA
CACCACACAGCAGCTACTCAGTCAGCTCCAGCTATGCGGCGTCCACCTGCCAGACCGGATATGGTGGCTT
CCTCACTGCTCCAGCCTCGCCCGCCTATTCAACAGCCAGTACCTCCGCGTTCAGCCCTTCGCCAGCCGGC
GGAATCAGCGCCAAGCGGAAGCGCGGTCGCCCTGCCAAGGATCATTCCGACGGTCCCGATCCCGTGCTCA
TGTCCAACATGAAAAGCGAGGAGGAGAGGAAGGCCTACCAGGACAGACTCAAGAACAACGAGGCTAGCCG
CGTATCGCGCCGGAAGACGAAGGTgcgcgaggaggaggagaagcgCGCCGAGGACGAGCTGTTGGCCGAG
AATCTGCGACTGCGTGCCCAGGCCGACAAAGTGGCCTTCCAGGAACGGAAGTTCAAGAAGTACCTGATGG
AGCGCCAGCGGATGAACAGCACTTACGTCAAGCAGGAGCAGGAC

-- completed exonerate analysis
