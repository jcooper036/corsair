Command line: [exonerate --model protein2genome --query genes/CG3997/CG3997_PA/CG3997_PA_files/CG3997_PA_prot.txt --target temp/scaffold_4845_CG3997_PA.fasta --ryo forcebeams 
>Dere
%tcs
 --bestn 1]
Hostname: [pathfinder.local]

C4 Alignment:
------------
         Query: CG3997_PA
        Target: scaffold_4845 type=golden_path_region; loc=scaffold_4845:1..22589142; ID=scaffold_4845; dbxref=GB:CH954179,REFSEQ:NW_001956550; MD5=8bb0f5f1625e5363f47329f326ee3ce9; length=22589142; release=r1.04; species=Dere;:[revcomp]
         Model: protein2genome:local
     Raw score: 255
   Query range: 0 -> 51
  Target range: 21325111 -> 21324736

        1 : MetAlaAlaHisLysSerPheArgIleLysGlnLysLeuAlaLysLysLeuLys :       18
            ..!|||||||||||||||||||||||||||||||||||||||||||||||||||
            GlnAlaAlaHisLysSerPheArgIleLysGlnLysLeuAlaLysLysLeuLys
 21325111 : CAGGCTGCACACAAGTCGTTCAGAATAAAGCAGAAGCTGGCTAAGAAGCTGAAG : 21325060

       19 : GlnAsnArgSerValProGlnTrpValArgLeuArgThrGlyAsnThrIle{Ar :       36
            |||||||||||||||||||||||||||||||||||||||||||||||||||{||
            GlnAsnArgSerValProGlnTrpValArgLeuArgThrGlyAsnThrIle{Ar
 21325059 : CAGAACAGATCCGTTCCCCAATGGGTTCGCCTACGTACTGGCAACACTATT{CG : 21325006

       37 : }  >>>> Target Intron 1 >>>>  {g}TyrAsnAlaLysArgArgHis :       43
            }            222 bp           {|}|||||||||||||||||||||
            }++                         ++{g}TyrAsnAlaLysArgArgHis
 21325005 : }gt.........................ag{T}TACAACGCTAAGCGCCGTCAC : 21324763

       44 : TrpArgArgThrLysLeuLysLeu :       51
            ||||||||||||||||||||||||
            TrpArgArgThrLysLeuLysLeu
 21324762 : TGGAGGCGTACCAAGTTGAAGCTG : 21324737

vulgar: CG3997_PA 0 51 . scaffold_4845 21325111 21324736 - 255 M 35 105 S 0 2 5 0 2 I 0 218 3 0 2 S 1 1 M 15 45
forcebeams 
>Dere
CAGGCTGCACACAAGTCGTTCAGAATAAAGCAGAAGCTGGCTAAGAAGCTGAAGCAGAACAGATCCGTTC
CCCAATGGGTTCGCCTACGTACTGGCAACACTATTCGTTACAACGCTAAGCGCCGTCACTGGAGGCGTAC
CAAGTTGAAGCTG

-- completed exonerate analysis
