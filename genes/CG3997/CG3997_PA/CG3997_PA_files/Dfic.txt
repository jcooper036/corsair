Command line: [exonerate --model protein2genome --query genes/CG3997/CG3997_PA/CG3997_PA_files/CG3997_PA_prot.txt --target temp/KB457339.1_CG3997_PA.fasta --ryo forcebeams 
>Dfic
%tcs
 --bestn 1]
Hostname: [pathfinder.local]

C4 Alignment:
------------
         Query: CG3997_PA
        Target: KB457339.1 Drosophila ficusphila unplaced genomic scaffold scf7180000453858, whole genome shotgun sequence:[revcomp]
         Model: protein2genome:local
     Raw score: 259
   Query range: 1 -> 51
  Target range: 1688868 -> 1688428

       2 : AlaAlaHisLysSerPheArgIleLysGlnLysLeuAlaLysLysLeuLysGlnAs :      20
           ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
           AlaAlaHisLysSerPheArgIleLysGlnLysLeuAlaLysLysLeuLysGlnAs
 1688868 : GCTGCACATAAGTCGTTCAGAATAAAGCAAAAGCTGGCTAAGAAGCTGAAGCAGAA : 1688814

      21 : nArgSerValProGlnTrpValArgLeuArgThrGlyAsnThrIle{Ar}  >>>> :      36
           ||||||||||||||||||||||||||||||||||||||||||||||{||}      
           nArgSerValProGlnTrpValArgLeuArgThrGlyAsnThrIle{Ar}++    
 1688813 : CAGGTCCGTTCCCCAATGGGTTCGTCTACGTACTGGCAACACAATT{CG}gt.... : 1688762

      37 :  Target Intron 1 >>>>  {g}TyrAsnAlaLysArgArgHisTrpArgArg :      46
                 290 bp           {|}||||||||||||||||||||||||||||||
                                ++{g}TyrAsnAlaLysArgArgHisTrpArgArg
 1688761 : .....................ag{C}TACAACGCTAAGCGCCGTCACTGGAGGCGT : 1688446

      47 : ThrLysLeuLysLeu :      51
           |||||||||||||||
           ThrLysLeuLysLeu
 1688445 : ACCAAGTTGAAGTTG : 1688429

vulgar: CG3997_PA 1 51 . KB457339.1 1688868 1688428 - 259 M 34 102 S 0 2 5 0 2 I 0 286 3 0 2 S 1 1 M 15 45
forcebeams 
>Dfic
GCTGCACATAAGTCGTTCAGAATAAAGCAAAAGCTGGCTAAGAAGCTGAAGCAGAACAGGTCCGTTCCCC
AATGGGTTCGTCTACGTACTGGCAACACAATTCGCTACAACGCTAAGCGCCGTCACTGGAGGCGTACCAA
GTTGAAGTTG

-- completed exonerate analysis
