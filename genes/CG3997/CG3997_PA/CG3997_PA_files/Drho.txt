Command line: [exonerate --model protein2genome --query genes/CG3997/CG3997_PA/CG3997_PA_files/CG3997_PA_prot.txt --target temp/KB450517.1_CG3997_PA.fasta --ryo forcebeams 
>Drho
%tcs
 --bestn 1]
Hostname: [pathfinder.local]

C4 Alignment:
------------
         Query: CG3997_PA
        Target: KB450517.1 Drosophila rhopaloa unplaced genomic scaffold scf7180000774050, whole genome shotgun sequence
         Model: protein2genome:local
     Raw score: 178
   Query range: 1 -> 36
  Target range: 26387 -> 26492

     2 : AlaAlaHisLysSerPheArgIleLysGlnLysLeuAlaLysLysLeuLysGlnAsnArg :    21
         ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
         AlaAlaHisLysSerPheArgIleLysGlnLysLeuAlaLysLysLeuLysGlnAsnArg
 26388 : GCGGCACACAAGTCGTTCAGAATAAAGCAGAAGCTGGCTAAAAAGCTGAAGCAGAACAGA : 26445

    22 : SerValProGlnTrpValArgLeuArgThrGlyAsnThrIleArg :    36
         |||||||||||||||||||||||||||||||||||||||||||||
         SerValProGlnTrpValArgLeuArgThrGlyAsnThrIleArg
 26446 : TCGGTTCCCCAATGGGTTCGCCTACGTACTGGCAACACAATCCGG : 26492

vulgar: CG3997_PA 1 36 . KB450517.1 26387 26492 + 178 M 35 105
forcebeams 
>Drho
GCGGCACACAAGTCGTTCAGAATAAAGCAGAAGCTGGCTAAAAAGCTGAAGCAGAACAGATCGGTTCCCC
AATGGGTTCGCCTACGTACTGGCAACACAATCCGG

-- completed exonerate analysis
