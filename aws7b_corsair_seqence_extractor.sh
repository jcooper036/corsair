#/bin/bash

## input should be a file name that has isoforms in it

## run the script to make an output of the amino acid sequences
python3 Corsair/aws7_corsair_seqence_extractor.py $1

## make the alignment
megacc -a muscle_align_protein.mao -d output.fasta -o align.meg

## make the NNJ Tree
megacc -a infer_NJ_protein.mao -d align.meg -o jupyter/out.nwk

rm -rf output.fasta
rm -rf align.meg