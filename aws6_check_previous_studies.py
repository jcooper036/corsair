#!/usr/bin/env python3
"""Usage: aws_corsair_results.py FILE

Arguments:
    FILE  Data output from Corsair

"""

import pandas
import docopt
import numpy as np

## Initialize docopt
if __name__ == '__main__':
    try:
        arguments = docopt.docopt(__doc__)
        CORSAIR_FILE = str(arguments['FILE'])

    except docopt.DocoptExit as e:
        print(e)



################################################################
################ definitions
################################################################

def read_corsair_data(file1):
    """parses the input file into a pandas object"""
    results = {}
    gene_df = pandas.read_table(file1)
    for i, x in gene_df.iterrows():
        gene_name = x['Gene name']
        results[gene_name] = ResultBlock(gene_name,
                                         x['Van der Lee 2017, positive hits'],
                                         x['Van der Lee 2017, gene list'],
                                         x['George 2007, positive hits'],
                                         x['George 2007, gene list'],
                                         x['Species analyzed'],
                                         x['Reference Length'],
                                         x['Percent aligned'],
                                         x['M7-M8 pval'],
                                         x['M8-M8a pval'],
                                         x['Average sites per aligner'],
                                         x['Common sites'],
                                         x['Av. Site Qscore'],
                                         x['Concordance'],
                                         x['Max aligner'],
                                         x['Site positions'])

    return results
################################################################
################ classes
################################################################

class ResultBlock(object):
    """Stores the results from a Corsair file for analysis"""

    #################
    ## these are used to Initialize and operate on the isoform class
    #################

    def __init__(self, name, lee_hit, lee_list, george_hit, george_list,
                 species, ref_length, per_aligned, m7m8, m8a,
                 average_sites, same_sites, av_site_qual,
                 concordance, max_aligner, site_positions):
        """initiates the result block. will contain NAs if they are not present"""
        self.name = name
        self.lee_hit = lee_hit
        self.lee_list = lee_list
        self.george_hit = george_hit
        self.george_list = george_list
        self.species = species if not np.isnan(species) else False
        self.ref_length = ref_length
        self.per_aligned = per_aligned if not np.isnan(per_aligned) else False
        self.m7m8 = m7m8 if not np.isnan(m7m8) else False
        self.m8a = m8a if not np.isnan(m8a) else False
        self.average_sites = average_sites if not np.isnan(average_sites) else False
        self.same_sites = same_sites if not np.isnan(same_sites) else False
        self.av_site_qual = av_site_qual if not np.isnan(av_site_qual) else False
        self.concordance = concordance if not np.isnan(concordance) else False
        self.max_aligner = max_aligner
        self.site_positions = site_positions
        ## set these to false, just in case
        self.was_run = False
        self.was_hit = False
        self.lee = 'DNR'
        self.george = 'DNR'
        self.cooper = 'DNR'

    def determine_hits(self):
        """evaluates our criteria to see if a gene was run and if it is a hit"""
        ## see if it was a hit
        if self.m8a < float(0.05):
            if self.concordance >= 0.90:
                if self.av_site_qual >= 0.90:
                    if self.same_sites >= 3:
                        self.was_hit = True

        ## see if it was run
        if self.m7m8 and not self.was_hit:
            self.was_run = True

    def determine_status(self):
        """figures out what the status for each of the genes is in each study"""
        
        ## van der Lee
        if str(self.lee_hit) == 'True':
            self.lee = 'hit'
        elif str(self.lee_list) == 'True':
            self.lee = 'neg'
        
        ## George
        if str(self.george_hit) == 'True':
            self.george = 'hit'
        elif str(self.george_list) == 'True':
            self.george = 'neg'

        ## us
        if self.was_hit:
            self.cooper = 'hit'
        elif self.was_run:
            self.cooper = 'neg'
            
    def print_results(self):
        """returns the phrases to be printed"""
        ## see if it was a hit
        with open('jupyter/compare_martix.txt', 'a') as f:
            return('\n' + self.name
                #    + '\t' + str(self.lee_hit)
                #    + '\t' + str(self.lee_list)
                #    + '\t' + str(self.george_hit)
                #    + '\t' + str(self.george_list)
                #    + '\t' + str(self.was_hit)
                #    + '\t' + str(self.was_run)
                   + '\t' + str(self.lee)
                   + '\t' + str(self.george)
                   + '\t' + str(self.cooper))
                    
        if self.m8a < float(0.05):
            if self.concordance >= 0.90:
                if self.av_site_qual >= 0.90:
                    self.was_hit = True

        ## see if it was run
        if self.m7m8 and not self.was_hit:
            self.was_run = True

################################################################
################ run commands
################################################################

RESULTS = read_corsair_data(CORSAIR_FILE)

## write the header to the results file
with open('jupyter/compare_martix.txt', 'w') as f:
    f.write('Gene name'
            # + '\t' + 'van der Lee hits'
            # + '\t' + 'van der Lee run'
            # + '\t' + 'George hits'
            # + '\t' + 'George run'
            # + '\t' + 'Cooper hits'
            # + '\t' + 'Cooper run'
            + '\t' + 'lee'
            + '\t' + 'george'
            + '\t' + 'cooper')

    for GENE in RESULTS:
        ## evaluate each gene
        RESULTS[GENE].determine_hits()
        ## evaluate each gene
        RESULTS[GENE].determine_status()
        ## print results for each gene
        f.write(RESULTS[GENE].print_results())
